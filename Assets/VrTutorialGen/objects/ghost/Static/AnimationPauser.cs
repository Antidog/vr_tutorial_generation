﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AnimationPauser : MonoBehaviour
{
  public static Vector3 currentHintPositon = new Vector3();
  public static Dictionary<KeyValuePair<int, string>, GameObject> reusableTextClones = new Dictionary<KeyValuePair<int, string>, GameObject>();
  public float timeTillPause = 0f;
  public float pauseDuration = 1.5f;
  public int number = 0;
  public Vector3 textPosition;
  public Animator obj1CloneAnimator;
  public string headline = "Place the Object";
  public string subline = "";
  public bool hasText = false;
  public GameObject floatingTextPrefab;
  public GameObject wallTextObject;
  public KeyValuePair<int, string> event_objectname_text_identifier;
  // Start is called before the first frame update
  public void startAnimationPauser()
  {
    StartCoroutine(Pause());
  }
  IEnumerator Pause()
  {
    GameObject textClone = hasText ? getTextClone() : null;
    while (timeTillPause >= 0)
    {
      timeTillPause -= Time.deltaTime;
      yield return new WaitForSeconds(Time.deltaTime);
    }
    if (hasText)
    {
      textClone.transform.position = obj1CloneAnimator.transform.position;
      textClone.GetComponentInChildren<TextTutorialHandler>().enableTextTutorial(headline, subline, number.ToString(format: "00"));
      wallTextObject.GetComponentInChildren<TextTutorialHandler>().enableTextTutorial(headline, subline, number.ToString(format: "00"));
      currentHintPositon = textClone.transform.position;
    }
    float oldSpeed = obj1CloneAnimator.speed;
    obj1CloneAnimator.speed = 0; 
    finalPosition = gameObject.transform;
    yield return new WaitForSeconds(pauseDuration);
    obj1CloneAnimator.speed = oldSpeed;
    if (hasText)
    {
      textClone.GetComponentInChildren<TextTutorialHandler>().diableTextTutorial();
      // wallTextObject.GetComponentInChildren<textTutorialHandler>().diableTextTutorial();
    }
  }
  public Transform finalPosition = null;
  public GameObject getTextClone()
  {
    GameObject textClone;
    /*if (reusableTextClones.ContainsKey(event_objectname_text_identifier))
    {
      textClone = reusableTextClones[event_objectname_text_identifier];
      textClone.is
      textClone.transform.SetParent(this.transform);
      textClone.SetActive(true);
    }
    else*/
    {
      textClone = GameObject.Instantiate(floatingTextPrefab);
      reusableTextClones[event_objectname_text_identifier] = textClone;
      textClone.transform.SetParent(this.transform);
      textClone.SetActive(true);
    }
    return textClone;
  }
}