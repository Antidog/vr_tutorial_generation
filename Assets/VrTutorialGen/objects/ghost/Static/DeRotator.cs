﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeRotator : MonoBehaviour
{
  GameObject player;
  Camera playerCam;

  void Start()
  {
    player = TutorialEventManager.Instance.Player;
    playerCam = player.GetComponentInChildren<Camera>();
    transform.localPosition = new Vector3(0, 0, 0);
  }

  void Update()
  {
    if (playerCam)
      transform.rotation = Quaternion.LookRotation(transform.position - (playerCam.gameObject.transform.position));
    else
      transform.rotation = Quaternion.LookRotation(transform.position- player.transform.position);
  }
  
}
