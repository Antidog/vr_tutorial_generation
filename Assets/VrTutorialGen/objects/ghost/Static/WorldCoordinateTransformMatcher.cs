﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCoordinateTransformMatcher : MonoBehaviour
{
  public Vector3 pos;
  public Quaternion rot;
  public Vector3 scale;
  OperationMode operationMode;
  public enum OperationMode
  {
    RECORDING,
    ENFORCING,
    NOTHING
  };
  // Start is called before the first frame update
  void Start()
  {
    switch (FindObjectOfType<GhostTutorialScript>().mode)
    {
      case GhostTutorialScript.GHOST_SCRIPT_MODE.Nothing:
        operationMode = OperationMode.NOTHING;
        break;
      case GhostTutorialScript.GHOST_SCRIPT_MODE.PlayGhostAnimation:
        operationMode = OperationMode.ENFORCING;
        break;
      case GhostTutorialScript.GHOST_SCRIPT_MODE.RecordGhostAnimation:
        operationMode = OperationMode.RECORDING;
        break;
      case GhostTutorialScript.GHOST_SCRIPT_MODE.RecordGhostAnimationStage2:
        operationMode = OperationMode.RECORDING;
        break;
    }
  }

  // Update is called once per frame
  void Update()
  {
    if(operationMode == OperationMode.RECORDING)
    {
      pos = gameObject.transform.position;
      rot = gameObject.transform.rotation;
      scale = gameObject.transform.localToWorldMatrix.GetScale();
    }
  }
  private void FixedUpdate()
  {
    if (operationMode == OperationMode.ENFORCING)
    {
      gameObject.transform.position = pos;
      gameObject.transform.rotation = rot;
      gameObject.transform.localScale = scale * 1.01f; //fixes z wars with materials and is barely noticeable
    }
  }
}
