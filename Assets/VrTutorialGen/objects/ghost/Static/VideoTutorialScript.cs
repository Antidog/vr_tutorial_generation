﻿using Assets.VrTutorialGen.events;
using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.Video;

public class VideoTutorialScript : MonoBehaviour
{
  public TextMeshPro text;
  public VideoPlayer videoPlayer;
  public int idFromTutorial;
  public List<VrTutorialGen.Event> events;
  public string videoFileName;
  // Start is called before the first frame update
  void Awake()
  {
    LoadGhostDataFromDB();
    videoPlayer.url = "file://" + videoFileName + ".mp4";
    videoPlayer.Prepare();
    videoPlayer.Play();
    videoPlayer.skipOnDrop = false;
    //videoPlayer.time = getStartTime();
    StartCoroutine("PlayNextEvent");
  }
  public float duration = 1f;
  float replayDuration = 1f;
  public float startDelay = 0f;
  float getStartTime()
  {
    var ev = events[CurrentEventId];
    float factor = (float)(videoPlayer.length)/ (replayDuration);
    float offset = (float)(videoPlayer.length) - replayDuration;
    float startTime = Mathf.Max(0, (ev.VideoTime+ startDelay) - duration / 2 );
    return startTime;
  }
  // Update is called once per frame
  void Update()
  {
  }
  public int repetitions = 5;
  int currentRepetitions = 0;
  float endTime = 0.5f;
  int CurrentEventId = 0;


  IEnumerator PlayNextEvent()
  {
    videoPlayer.Pause();
    videoPlayer.Prepare();
    yield return new WaitUntil(() => videoPlayer.isPrepared == true);

    var ev = events[CurrentEventId];
    float startTime = getStartTime();
    videoPlayer.time = startTime;
    endTime = startTime + duration; //Mathf.Min(startTime + 2f, (float)videoPlayer.length-0.005f);
    text.text = ev.GetAutomatedTutorialText();
    currentRepetitions++;
    if (currentRepetitions >= repetitions)
    {
      CurrentEventId++;
      currentRepetitions = 0;
      if (CurrentEventId >= events.Count)
      {
        CurrentEventId = 0;
      }
    }
    videoPlayer.Play();
    yield return new WaitForSeconds(duration);
    StartCoroutine("PlayNextEvent");
  }

  private void LoadGhostDataFromDB()
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "SELECT id, events, gameId, videoName, duration FROM tutorialevents where id=@id order by id desc limit 1";
    SqliteParameter idParam = new SqliteParameter("@id", System.Data.DbType.String);
    idParam.Value = idFromTutorial;
    cmd.Parameters.Add(idParam);
    IDataReader reader = cmd.ExecuteReader();
    while (reader.Read())
    {
      int id = reader.GetInt32(0);
      byte[] bin = (byte[])reader.GetValue(1);
      MemoryStream s = new MemoryStream(bin);
      BinaryFormatter binary = new BinaryFormatter();
      binary.Binder = new SerializationBinderHelper();
      events = (List<VrTutorialGen.Event>)binary.Deserialize(s);
      if (reader.IsDBNull(reader.GetOrdinal("duration")))
        replayDuration = 1;
      else
        replayDuration = reader.GetFloat(reader.GetOrdinal("duration"));
      if (reader.IsDBNull(reader.GetOrdinal("videoName")))
        videoFileName = "Error";
      else
        videoFileName = Application.streamingAssetsPath + "/VrTutorialGen/" + reader.GetString(reader.GetOrdinal("videoName"));
    }
    reader.Close();
    reader = null;
    cmd.Dispose();
    cmd = null;
    con.Close();
    con = null;
  }
}
