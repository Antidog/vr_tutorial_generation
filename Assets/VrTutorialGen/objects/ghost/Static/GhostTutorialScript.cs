﻿using Assets.VrTutorialGen;
using Assets.VrTutorialGen.events;
using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class GhostTutorialScript : MonoBehaviour
{
  public RuntimeAnimatorController dummyToClone;
  public GameObject wallTextObject;
  public List<VrTutorialGen.Event> events;
  public Dictionary<float, string> attachedObjects;
  public int idFromTutorial;
  public List<Hand> allHands;
  public Dictionary<Hand, List<string>> monitoredHands = new Dictionary<Hand, List<string>>();
  public GameObject floatingTextPrefab;
  float replayDuration = 1f;
  public float alphaForClones = 0.4f;
  public float eventDisplayDuration = 5f;
  public int repeateNTimes = 5;
  public float animationPauseDuration = 3f;
  public bool deduplicateCollisonsAndTriggers = true;
  private static readonly int HasHighlight = Shader.PropertyToID("_HasHighlight");
  private TutorialHintDecalScript hintDecal;
  public enum GHOST_SCRIPT_MODE
  {
    PlayGhostAnimation,
    RecordGhostAnimation,
    RecordGhostAnimationStage2,
    Nothing
  }
  public GHOST_SCRIPT_MODE mode = GHOST_SCRIPT_MODE.Nothing;
  bool initialized = false;
  bool ghostFoundInitialized = false;
  bool playerFoundInitialized = false;
  bool attachedObjectsUpdated = false;
  public float eventsOffset = 1;
  void DoInit()
  {
    PlayerPerformedAction = false;
    if (mode != GHOST_SCRIPT_MODE.Nothing)
      LoadGhostDataFromDB();
    gameObjectCache = new Dictionary<string, GameObject>();
    //videoPlayer.time = getStartTime();
    if (allHands.Count > 0 && !allHands[0])
    {
      foreach (var hand in FindObjectsOfType<Hand>())
        allHands.Add(hand);
    }
    foreach (var hand in allHands)
      gameObjectCache.TryAdd(hand.gameObject.name, hand.gameObject);
    if (hintDecal == null)
      this.hintDecal = gameObject.GetComponentInChildren<TutorialHintDecalScript>();
    //TDOO: play normal animation
  }
  public void RecordHands(bool record)
  {
    if (handsRecorded == false)
    {
      handsRecorded = true;
      foreach (Hand item in allHands)
      {
        if (!monitoredHands.ContainsKey(item))
        {
          //TODO: maybe wrong with timing
          monitoredHands.Add(item, new List<string>());
          if(record)
            RecordAnimationForObject(item.gameObject);
        }
      }
    }
  }
  public bool LastPlayedEventHasEnded = true;
  public bool PlayerPerformedAction = false;
  private bool handsRecorded = false;
  private List<String> collisionsPlayed = new List<string>();
  public float pauseTime = 1;
  int currentRepetitions = 0;
  private float currPause = 0;
  // Update is called once per frame
  void Update()
  {
    if (mode == GHOST_SCRIPT_MODE.Nothing)
      return;
    if(!initialized)
    {
      DoInit();
      initialized = true;
      return;
    }
    if(!ghostFoundInitialized && (mode == GHOST_SCRIPT_MODE.RecordGhostAnimation || mode == GHOST_SCRIPT_MODE.RecordGhostAnimationStage2))
    {
      if (TutorialEventManager.Instance && TutorialEventManager.Instance.Ghost && TutorialEventManager.Instance.Ghost.enabled && TutorialEventManager.Instance.Ghost.gameObject.activeInHierarchy)
      { 
        ghostFoundInitialized = true;
        return;
      }
      else
        return;
    }else if(!playerFoundInitialized && mode == GHOST_SCRIPT_MODE.PlayGhostAnimation)
    {
      if (TutorialEventManager.Instance && TutorialEventManager.Instance.Player && TutorialEventManager.Instance.Player.activeInHierarchy)
      {
        playerFoundInitialized = true;
        return;
      }
      else
        return;
    }
    if (allHands.Count > 0 && !allHands[0])
    {
      allHands.Clear();
      foreach (var hand in FindObjectsOfType<Hand>())
        allHands.Add(hand);
    }
    if (isNewIteration()) //Second+ Run Through
    {
      allHands.Clear();
      foreach (var hand in FindObjectsOfType<Hand>())
        allHands.Add(hand);
      if (mode == GHOST_SCRIPT_MODE.RecordGhostAnimation)
        mode = GHOST_SCRIPT_MODE.Nothing;
      //  mode = GHOST_SCRIPT_MODE.RecordGhostAnimationStage2;
      else if(mode == GHOST_SCRIPT_MODE.RecordGhostAnimationStage2)
        mode = GHOST_SCRIPT_MODE.Nothing;
    }
    List<VrTutorialGen.Event> actionRecordsPerformed = new List<VrTutorialGen.Event>();
    if (mode == GHOST_SCRIPT_MODE.RecordGhostAnimation)
    {
      RecordHands(false);
      foreach (var hand in monitoredHands)
      {
        foreach (var obj in hand.Key.AttachedObjects)
        {
          if (!hand.Value.Contains(obj.attachedObject.name))
          {
            hand.Value.Add(obj.attachedObject.name);
            // attachedObjects.Add(TutorialEventManager.Instance.iterationDependendTime(), obj.attachedObject.name);
            attachedObjects.Add(Time.time, obj.attachedObject.name);
          }
        }
      }
      if(TutorialEventManager.firstIterationIsOver && !attachedObjectsUpdated)
      {
        UpdateAttachedObjectDataInDB();
        attachedObjectsUpdated = true;
      }
    }
    else if (mode == GHOST_SCRIPT_MODE.RecordGhostAnimationStage2)
    {
      RecordHands(true);
      foreach (var ghostEvent in events)
      {
        if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(ghostEvent.PerformedAt - eventDisplayDuration / 2f))
          continue;
        RecordGhostAnimationsForSinlgeEvent(ghostEvent);
        actionRecordsPerformed.Add(ghostEvent);
      }
      Dictionary<float, string> toRemove = new Dictionary<float, string>();
      foreach (var item in attachedObjects)
      {
        if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(item.Key - eventDisplayDuration/2f))
          continue;
        if (TutorialEventManager.firstIterationIsOver)
          break;
        try
        {
          GameObject obj1 = getCachedGameObjectByName(item.Value);
          RecordAnimationForObject(obj1);
          toRemove.Add(item.Key, item.Value);
        }
        catch (Exception) { }
      }
      foreach (var item in toRemove)
        attachedObjects.Remove(item.Key);

    }
    else if (mode == GHOST_SCRIPT_MODE.PlayGhostAnimation)
    {
      if (deduplicateCollisonsAndTriggers)
      { 
        events = RemoveDuplicateCollisionAndTriggerEvents(events);
        deduplicateCollisonsAndTriggers = false;
      }
      if (eventIdToDisplay < events.Count)
      {
        //if (!TutorialEventManager.Instance.iterationDependendTimeIsInFuture(eventEndTime))
        if (LastPlayedEventHasEnded)
        {
          if (!PlayerPerformedAction)
          {
            var ghostEvent = events[eventIdToDisplay];

            if (currPause > 0)
            {
              currPause -= Time.deltaTime;
              //todo fancy animation to catch attention?
            }
            else
            {
              LastPlayedEventHasEnded = false;
              Debug.Log("#" + (eventIdToDisplay+1) + ": " + ghostEvent.GetAutomatedTutorialText() + " " + ghostEvent.GetAutomatedText() + ghostEvent.EventType_);
              switch (ghostEvent.EventType_)
              {
                case (VrTutorialGen.Event.EventType.COLLISION):
                  PlayCollisionAnimation(ghostEvent);
                  break;
                case (VrTutorialGen.Event.EventType.TRIGGER):
                  PlayTriggerAnimation(ghostEvent);
                  break;
                case (VrTutorialGen.Event.EventType.BOOLEAN_ACTION):
                  PlayBooleanAnimation(ghostEvent);
                  break;
                default:
                  Debug.Log(ghostEvent.EventType_ + "no good visualization possible");
                  LastPlayedEventHasEnded = true;
                  PlayerPerformedAction = true;
                  //PlayBooleanAnimation(ghostEvent);
                  break;
              }
              currentRepetitions++;
              displayButtonTutorial(ghostEvent);
              currPause = pauseTime;
            }
          }
          else
          {
            hintDecal.DoReset();
            PlayerPerformedAction = false;
            currentRepetitions = 0;
            eventIdToDisplay++;
          }
        }
      }
      else
      {
        eventIdToDisplay = 0;
      }
    }
    else if(mode == GHOST_SCRIPT_MODE.Nothing)
    {
      Valve.VR.OpenVR.Input.ShowActionOrigins(0, 0);
    }

    foreach (var actionRecord in actionRecordsPerformed)
    {
      events.Remove(actionRecord);
    }

    actionRecordsPerformed.Clear();

  }
  private List<VrTutorialGen.Event> RemoveDuplicateCollisionAndTriggerEvents(List<VrTutorialGen.Event> myevents)
  {
    List<VrTutorialGen.Event> singles = new List<VrTutorialGen.Event>();
    foreach (var ev in myevents)
    {
      if (ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
      {
        var converted = (VrTutorialGen.CollisionRecord)ev;
        bool alreadyContainsIt = false;
        foreach (var single in singles)
        {
          if (single.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
          {
            var converted2 = (VrTutorialGen.CollisionRecord)single;
            bool identical = converted2.Self == converted.Self && converted.Other_name == converted2.Other_name;
            if (identical)
            {
              alreadyContainsIt = true;
            }
          }
        }
        if (!alreadyContainsIt)
          singles.Add(converted);
      }
      else if (ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
      {
        var converted = (VrTutorialGen.TriggerRecord)ev;
        bool alreadyContainsIt = false;
        foreach (var single in singles)
        {
          if (single.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
          {
            var converted2 = (VrTutorialGen.TriggerRecord)single;
            bool identical = converted2.Self == converted.Self && converted.Other_name == converted2.Other_name;
            if (identical)
            {
              alreadyContainsIt = true;
            }
          }
        }
        if (!alreadyContainsIt)
          singles.Add(converted);
      }
      else
        singles.Add(ev);
    }
    return singles;
  }

  private void OnDestroy()
  {
    try
    {
      Valve.VR.OpenVR.Input.ShowActionOrigins(0, 0);
    }
    catch (Exception) { };
  }
  public int eventIdToDisplay = 0;

  Dictionary<string, GameObject> gameObjectCache;
  private GameObject getCachedGameObjectByName(string name)
  {
    GameObject obj = null;
    string alias = VrTutorialGen.Event.GetAlias(name);
    if (gameObjectCache.ContainsKey(name))
    {
      obj = gameObjectCache[name];
    }
    else if (gameObjectCache.ContainsKey(alias))
    {
      obj = gameObjectCache[alias];
    }
    if (obj)
      return obj;
    obj = GameObject.Find(name);
    gameObjectCache[name] = obj;
    if (obj == null)
      Debug.LogError("[GameObjectCache]  Object " + name + " not found in scene, this usually happens if steamvr has no controllers attached and therefore the hands don't exist - check if the given object is somewhere active and enabled in the Scene Hierarchy");
    return obj;
  }

  float lastIterationTime = 0;
  private bool isNewIteration()
  {
    float thisIteratonTime = TutorialEventManager.Instance.iterationDependendTime();
    var result = lastIterationTime > thisIteratonTime ;
    lastIterationTime = thisIteratonTime;
    return result;
  }

  private void UpdateAttachedObjectDataInDB()
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "Update tutorialevents set attachedObjects=@attachedObjects, duration=@duration where id=@id ;";
    SqliteParameter idParam = new SqliteParameter("@id", System.Data.DbType.String);
    idParam.Value = idFromTutorial;
    cmd.Parameters.Add(idParam);

    SqliteParameter attachedObjectsParam = new SqliteParameter("@attachedObjects", System.Data.DbType.Binary);
    SqliteParameter durationParam = new SqliteParameter("@duration", System.Data.DbType.Single);
    if (attachedObjects != null)
    {
      MemoryStream s = new MemoryStream();
      BinaryFormatter binary = new BinaryFormatter();
      binary.Serialize(s, attachedObjects);

      attachedObjectsParam.Value = s.ToArray();
    }
    else
    {
      attachedObjectsParam.Value = null;
    }
    durationParam.Value = TutorialEventManager.firstIterationDuration;
    cmd.Parameters.Add(durationParam);
    cmd.Parameters.Add(attachedObjectsParam);
    cmd.ExecuteNonQuery();

    cmd.Dispose();
    cmd = null;
    con.Close();
    con = null;
    Debug.Log("tutorialevents saved attached objects to db - you can exit this step now");
  }

  private void LoadGhostDataFromDB()
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "SELECT id, events, attachedObjects, gameId, videoName, duration FROM tutorialevents where id=@id order by id asc;";
    SqliteParameter idParam = new SqliteParameter("@id", System.Data.DbType.String);
    idParam.Value = idFromTutorial;
    cmd.Parameters.Add(idParam);
    IDataReader reader = cmd.ExecuteReader();
    while (reader.Read())
    {
      try
      {
        int id = reader.GetInt32(0);
        byte[] bin = (byte[])reader.GetValue(1);
        MemoryStream s = new MemoryStream(bin);
        if (!reader.IsDBNull(2))
        {
          BinaryFormatter binary1 = new BinaryFormatter();
          binary1.Binder = new SerializationBinderHelper();
          byte[] bin2 = (byte[])reader.GetValue(2);
          MemoryStream s2 = new MemoryStream(bin2);
          attachedObjects = (Dictionary<float, string>)binary1.Deserialize(s2);
        }
        else
          attachedObjects = new Dictionary<float, string>();
        BinaryFormatter binary = new BinaryFormatter();
        binary.Binder = new SerializationBinderHelper();
        events = (List<VrTutorialGen.Event>)binary.Deserialize(s);
        if (reader.IsDBNull(reader.GetOrdinal("duration")))
          replayDuration = 1;
        else
          replayDuration = reader.GetFloat(reader.GetOrdinal("duration"));

      }
      catch (System.Exception ex)
      {
        Debug.LogWarning("Error when reading old ghost data:" + ex);
      }
    }
    reader.Close();
    reader = null;
    cmd.Dispose();
    cmd = null;
    con.Close();
    con = null;

  }

  void makeTransperant(ref Material material)
  { //https://forum.unity.com/threads/change-rendering-mode-via-script.476437/
    /*material.SetOverrideTag("RenderType", "Transparent");
    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
    material.SetInt("_ZWrite", 0);
    material.DisableKeyword("_ALPHATEST_ON");
    material.EnableKeyword("_ALPHABLEND_ON");
    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
    material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;*/
  }
  void RecordAnimationForObject(GameObject obj)
  {
    AnimationRecorder anim1 = obj.GetComponent<AnimationRecorder>();
    if (anim1 == null)
    {
      obj.AddComponent<WorldCoordinateTransformMatcher>();
      obj.AddComponent<AnimationRecorder>();
      obj.GetComponent<AnimationRecorder>().StartAnimationRecordingForXSeconds(obj.name, eventDisplayDuration);
    }
    else
      anim1.ExtendDurationByXSeconds(eventDisplayDuration);
  }
  void RecordGhostAnimationsForSinlgeEvent(VrTutorialGen.Event ghostEvent)
  {
    string selfname;
    string othername;
    GameObject obj1;
    GameObject obj2;
    switch (ghostEvent.EventType_)
    {
      case VrTutorialGen.Event.EventType.BOOLEAN_ACTION:
        var booleanRecord = (VrTutorialGen.BooleanActionRecord)ghostEvent;
        break;
      case VrTutorialGen.Event.EventType.COLLISION:
        var collisionRecord = (VrTutorialGen.CollisionRecord)ghostEvent;
        selfname = ((VrTutorialGen.CollisionRecord)ghostEvent).Self_name;
        othername = ((VrTutorialGen.CollisionRecord)ghostEvent).Other_name;
        obj1 = getCachedGameObjectByName(selfname);
        RecordAnimationForObject(obj1);
        obj2 = getCachedGameObjectByName(othername);
        RecordAnimationForObject(obj2);
        break;
      case VrTutorialGen.Event.EventType.TRIGGER:
        var triggerRecord = (VrTutorialGen.TriggerRecord)ghostEvent;
        selfname = ((VrTutorialGen.TriggerRecord)ghostEvent).Self_name;
        othername = ((VrTutorialGen.TriggerRecord)ghostEvent).Other_name;
        obj1 = getCachedGameObjectByName(selfname);
        RecordAnimationForObject(obj1);
        obj2 = getCachedGameObjectByName(othername);
        RecordAnimationForObject(obj2);
        break;
    }
  }
  void ReplaceMaterialAlpha(GameObject gameObjectWithMeshRenderer, float alpha, bool doMakeTransparent = true)
  {
    MeshRenderer renderer = gameObjectWithMeshRenderer.GetComponent<MeshRenderer>();
    if (!renderer)
      renderer = gameObjectWithMeshRenderer.GetComponentInChildren<MeshRenderer>();
    if (!renderer)
      return;

    var material = new Material(renderer.material);
    material.SetFloat(HasHighlight, doMakeTransparent ? 1 : 0);
    renderer.material = material;
    if(!material.HasFloat(HasHighlight))
    {
      try
      {
        Debug.LogError("Fallback material used for transperancy, maybe not URP _hashighlight available");
      Material b = renderer.material;
      try
      {
        if( b.HasProperty("color"))
          b.color = new Color(b.color.r, b.color.g, b.color.b, b.color.a * alpha);
      }
      catch (System.Exception) { }
      if (doMakeTransparent)
        makeTransperant(ref b);
      var meshRender = gameObjectWithMeshRenderer.GetComponent<MeshRenderer>();
      if(meshRender)
        meshRender.material = b;
      }
      catch (System.Exception) { }
    }
  }
  void removeScriptsAndRigidBodyFromClone(GameObject clone)
  {
    foreach (var col in clone.GetComponents<Collider>())
    {
      col.enabled = false;
    }
    foreach (var col in clone.GetComponentsInChildren<Collider>())
    {
      col.enabled = false;
    }
    foreach (var sj in clone.GetComponents<SpringJoint>())
    {
      Destroy(sj);
    }
    foreach (var sj in clone.GetComponentsInChildren<SpringJoint>())
    {
      Destroy(sj);
    }
    foreach (var thr in clone.GetComponentsInChildren<Throwable>())
    {
      Destroy(thr);
    }
    foreach (var thr in clone.GetComponents<Throwable>())
    {
      Destroy(thr);
    }
    
    foreach (var hd in clone.GetComponents<HandCollider>())
    {
      Destroy(hd);
    }
    foreach (var hd in clone.GetComponentsInChildren<HandCollider>())
    {
      Destroy(hd);
    }

    foreach (var hch in clone.GetComponentsInChildren<HandCollisionHandler>())
    {
      hch.enabled = false;
    }
    foreach (var hch in clone.GetComponents<HandCollisionHandler>())
    {
      hch.enabled = false;
    }
    foreach (var save in clone.GetComponentsInChildren<AGTIV.Scripts.Saveable.Saveable>())
    {
      save.enabled = false;
    }
    foreach (var save in clone.GetComponents<AGTIV.Scripts.Saveable.Saveable>())
    {
      save.enabled = false;
    }

    try
    {
      foreach (var rb in clone.GetComponents<Rigidbody>())
      {
        rb.isKinematic = true;
        Destroy(rb);
      }
      foreach (var rb in clone.GetComponentsInChildren<Rigidbody>())
      {
        rb.isKinematic = true;
        Destroy(rb);
      }
    }
    catch (Exception) { }
  }
  
  void PlayAnimationOf2ObjectNames(string ownName, string otherName, out float timeTillPause, float eventPerformedAt, VrTutorialGen.Event.EventType type, string headline = "", string subline = "")
  { 
    GameObject obj1 = getCachedGameObjectByName(ownName);
    if (currentRepetitions == 0)
    {
      if (type == VrTutorialGen.Event.EventType.COLLISION)
      {
        if (!obj1.GetComponent<CollisionDetectionForEvent>())
        {
          CollisionDetectionForEvent collisionDetectionForEvent = obj1.AddComponent<CollisionDetectionForEvent>();
          collisionDetectionForEvent.objectNameToCheck = otherName;
          collisionDetectionForEvent.parent = this;
        }
      }
      else if (type == VrTutorialGen.Event.EventType.TRIGGER)
      {
        if (!obj1.GetComponent<TriggerDetectionForEvent>())
        {
          TriggerDetectionForEvent triggerDetectionForEvent = obj1.AddComponent<TriggerDetectionForEvent>();
          triggerDetectionForEvent.objectNameToCheck = otherName;
          triggerDetectionForEvent.parent = this;
        }
      }
    }
    GameObject obj1Clone = GameObject.Instantiate(obj1);
    obj1Clone.AddComponent<WorldCoordinateTransformMatcher>();
    ReplaceMaterialAlpha(obj1Clone, alphaForClones);

    GameObject obj2 = getCachedGameObjectByName(otherName);
    GameObject obj2Clone = GameObject.Instantiate(obj2);
    ReplaceMaterialAlpha(obj2Clone, alphaForClones);
    obj2Clone.AddComponent<WorldCoordinateTransformMatcher>();

    SetAnimationClipForObjectClone(obj1Clone, obj1, eventPerformedAt, out timeTillPause, true, headline, subline);
    SetAnimationClipForObjectClone(obj2Clone, obj2, eventPerformedAt, out timeTillPause, false, "", "");
    removeScriptsAndRigidBodyFromClone(obj1Clone);
    removeScriptsAndRigidBodyFromClone(obj2Clone);
  }
  void PlayCollisionAnimation(VrTutorialGen.Event ghostEvent)
  {
    string ownName = ((VrTutorialGen.CollisionRecord)ghostEvent).Self_name;
    string otherName = ((VrTutorialGen.CollisionRecord)ghostEvent).Other_name;
    float timeTillPause;
    PlayAnimationOf2ObjectNames(ownName, otherName, out timeTillPause, ghostEvent.PerformedAt + eventsOffset, ghostEvent.EventType_, ghostEvent.GetAutomatedTutorialText());
  }
  void PlayTriggerAnimation(VrTutorialGen.Event ghostEvent)
  {
    string ownName = ((VrTutorialGen.TriggerRecord)ghostEvent).Self_name;
    string otherName = ((VrTutorialGen.TriggerRecord)ghostEvent).Other_name;
    float timeTillPause;
    PlayAnimationOf2ObjectNames(ownName, otherName, out timeTillPause, ghostEvent.PerformedAt + eventsOffset, ghostEvent.EventType_, ghostEvent.GetAutomatedTutorialText()); 
  }
  void PlayBooleanAnimation(VrTutorialGen.Event ghostEvent)
  {
    VrTutorialGen.BooleanActionRecord action = (VrTutorialGen.BooleanActionRecord)ghostEvent;
    bool found = false;
    foreach (Hand item in allHands)
    {
      if (action.Input_Source == item.handType)
      {
        GameObject handGameObject = item.gameObject;
        GameObject handClone = GameObject.Instantiate(handGameObject);
        handClone.GetComponent<Valve.VR.SteamVR_Behaviour_Pose>().enabled = false;
        handClone.GetComponent<Hand>().enabled = false;
        handClone.AddComponent<WorldCoordinateTransformMatcher>();
        removeScriptsAndRigidBodyFromClone(handClone);
        ReplaceMaterialAlpha(handClone, alphaForClones);
        float timeTillPause;
        SetAnimationClipForObjectClone(handClone, handGameObject, action.PerformedAt + eventsOffset, out timeTillPause, true, action.GetHeadlineText(), action.GetSublineText());
        foreach (var col in handClone.GetComponents<Collider>())
        {
          col.enabled = false;
        }
        foreach (var rb in handClone.GetComponents<Rigidbody>())
        {
          rb.isKinematic = true;
        }

        StartCoroutine(HandleAttachedObjects(action,timeTillPause, item));
        found = true;
        break;
      }
    }
    if(!found)
    {
      Debug.LogWarning("Event "+ghostEvent.GetAutomatedText() +" #" +eventIdToDisplay+ " is used by input: '" + action.Input_Source + "' which is not present in AllHands[]. The event will be ignored if this event seems important add a handscript with that input source to the AllHands list of the TutorialGenerationEventManager/GhostTutorialScript");
      LastPlayedEventHasEnded = true;
      PlayerPerformedAction = true;
    }
  }
  private void HandleBooleanActionPlayerPerformedChecks(VrTutorialGen.BooleanActionRecord action, GameObject obj1, AnimationPauser pauser)
  {
    foreach (var hand in FindObjectsOfType<Hand>())
    {
      if (currentRepetitions == 0)
      {
        if (IsGrabStartingEvent(action))
        {
          PickUpDetecionForEvent pickUpDetecion = hand.gameObject.AddComponent<PickUpDetecionForEvent>();
          pickUpDetecion.parent = this;
          pickUpDetecion.gameObjectThatShouldBeAttached = obj1;
          pickUpDetecion.hand = hand;
        }
        else if (IsGrabEndingEvent(action))
        {
          DropOffDetectionForEvent dropOffDetectionForEvent = hand.gameObject.AddComponent<DropOffDetectionForEvent>();
          dropOffDetectionForEvent.parent = this;
          dropOffDetectionForEvent.gameObjectThatShouldBeDetached = obj1;
          dropOffDetectionForEvent.pauser = pauser;
          hintDecal.init(obj1);
          hintDecal.pauser = pauser;
        }
        else
        {
          Debug.LogError("[GhostTutorial] Player has Performed action not implemented for action " + action.GetName() + " currently only grab actions allow this check - it will be assumed that the player performed it. Tutorial text: " + action.GetAutomatedTutorialText());
          PlayerPerformedAction = true;
        }
      }
    }
  }
  private bool IsGrabStartingEvent(VrTutorialGen.BooleanActionRecord action)
  {
    if (action.ActionStarts == true && action.GetName().ToLower().Contains("grab"))
      return true;
    return false;
  }
  private bool IsGrabEndingEvent(VrTutorialGen.BooleanActionRecord action)
  {
    if (action.ActionStarts == false && action.GetName().ToLower().Contains("grab"))
      return true;
    return false;
  }
  Dictionary<string, AnimationClip> animationClipCache = new Dictionary<string, AnimationClip>();
  AnimationClip getAnimationClip(GameObject o)
  {
    if (animationClipCache.ContainsKey(o.name))
      return animationClipCache[o.name];
    AnimationClip clip = Resources.Load<AnimationClip>(o.name);
    if (clip == null)
      Debug.LogError("Couldn't load resource: " + o.name + " make sure to generate them first by Running ghostanimator once in RecordGhostAnimation Mode;");
    else
      animationClipCache.Add(o.name, (AnimationClip)clip);
    return (AnimationClip)clip;
  }
  //returns animation Duration
  void SetAnimationClipForObjectClone(GameObject clone, GameObject original, float eventPerformedAt, out float timeTillPause,  bool isMainObject, string headline = "", string subline="")
  {
    timeTillPause = 0;
    Animator obj1CloneAnimator = clone.GetComponent<Animator>();
    if (!obj1CloneAnimator)
      obj1CloneAnimator = clone.AddComponent<Animator>();

    AnimationClip clip = getAnimationClip(original);
    if (clip != null)
    {
      obj1CloneAnimator.runtimeAnimatorController = dummyToClone;
      AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController();
      animatorOverrideController.runtimeAnimatorController = obj1CloneAnimator.runtimeAnimatorController;
      animatorOverrideController["OldGhostAnimation 1"] = clip;
      obj1CloneAnimator.runtimeAnimatorController = animatorOverrideController;
      float animationDuration = clip.length;
      float timeDiffOriginalEventRecordedDuration = replayDuration - animationDuration;
      float secondsIntoAnimationWhenEventOccurs = eventPerformedAt - timeDiffOriginalEventRecordedDuration;
      float percentOfAnimationToStartFrom = (secondsIntoAnimationWhenEventOccurs - eventDisplayDuration/2) / animationDuration;
      if(secondsIntoAnimationWhenEventOccurs - eventDisplayDuration / 2 < 0)
      {
        timeTillPause = (secondsIntoAnimationWhenEventOccurs - eventDisplayDuration)*-1;
        percentOfAnimationToStartFrom = MathF.Max(0, percentOfAnimationToStartFrom);
      }
      else timeTillPause = eventDisplayDuration / 2f;
      obj1CloneAnimator.Play(0, -1, percentOfAnimationToStartFrom);
      addPauser(clone, timeTillPause, obj1CloneAnimator, headline, subline, isMainObject);
      obj1CloneAnimator.enabled = true;
    }
  }
  public void ResetTutorial()
  {
    currentRepetitions = 0;
    eventIdToDisplay = -1;
    currPause = 0;
    PlayerPerformedAction = true;
  }
  void SetAnimationClipForChildObjectClone(GameObject clone, GameObject original, float parentAnimationStartTime, float ownTime, float timeTillPause, bool isMainObject, string headlinetext = "", string sublinetext = "")
  {

    Animator obj1CloneAnimator = clone.GetComponent<Animator>();
    if (!obj1CloneAnimator)
      obj1CloneAnimator = clone.AddComponent<Animator>();
    AnimationClip clip = getAnimationClip(original);
    float animationDuration = clip.length;
    float timeDiffOriginalEventRecordedDuration = replayDuration - animationDuration;
    float secondsIntoAnimationWhenEventOccurs = parentAnimationStartTime - timeDiffOriginalEventRecordedDuration;
    float percentOfAnimationToStartFrom = (secondsIntoAnimationWhenEventOccurs - eventDisplayDuration / 2) / animationDuration;
    if (secondsIntoAnimationWhenEventOccurs - eventDisplayDuration / 2 < 0)
    {
      timeTillPause = secondsIntoAnimationWhenEventOccurs;
      percentOfAnimationToStartFrom = MathF.Max(0, percentOfAnimationToStartFrom);
    }
    else timeTillPause = eventDisplayDuration / 2f;
    if (clip != null)
    {
      obj1CloneAnimator.runtimeAnimatorController = dummyToClone;
      AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController();
      animatorOverrideController.runtimeAnimatorController = obj1CloneAnimator.runtimeAnimatorController;
      animatorOverrideController["OldGhostAnimation 1"] = clip;
      obj1CloneAnimator.runtimeAnimatorController = animatorOverrideController;
      obj1CloneAnimator.Play(0, -1, percentOfAnimationToStartFrom);
      addPauser(clone, timeTillPause, obj1CloneAnimator, headlinetext, sublinetext, isMainObject);
    }
  }
  private void addPauser(GameObject clone, float timeTillPause, Animator obj1CloneAnimator, string headlinetext,  string sublinetext, bool isMainObject)
  {

    AnimationPauser pauser = clone.AddComponent<AnimationPauser>();
    pauser.timeTillPause = timeTillPause;
    pauser.obj1CloneAnimator = obj1CloneAnimator;
    pauser.subline = sublinetext;
    pauser.headline = headlinetext;
    pauser.hasText = isMainObject;
    pauser.floatingTextPrefab = floatingTextPrefab;
    pauser.pauseDuration = animationPauseDuration;
    pauser.number = this.eventIdToDisplay+1;
    pauser.event_objectname_text_identifier = new KeyValuePair<int, string>(eventIdToDisplay, clone.name);
    pauser.wallTextObject = wallTextObject;
    obj1CloneAnimator.enabled = true;
    pauser.startAnimationPauser();


    var cloneFadeoutComponent = clone.GetComponent<CloneFadeout>();
    if (!cloneFadeoutComponent)
      cloneFadeoutComponent = clone.AddComponent<CloneFadeout>();
    cloneFadeoutComponent.thisIsMainObject = isMainObject;
    cloneFadeoutComponent.remainingDuration += eventDisplayDuration+ pauser.pauseDuration;
  }
  IEnumerator HandleAttachedObjects(VrTutorialGen.BooleanActionRecord booleanActionRecord, float timeTillPause, Hand hand)
  {
    float parentAnimationStartTime = booleanActionRecord.PerformedAt + eventsOffset;
    float realStartTime = Time.time;
    var localAttachedObjectsClone = new Dictionary<float, string>(attachedObjects);
    while (Time.time < realStartTime + eventDisplayDuration)
    {
      float timepassed = Time.time - realStartTime;
      Dictionary<float, string> removeFromAttachedObjects = new Dictionary<float, string>(); //TODO: maybe need a list of strings, 
      foreach (var item in localAttachedObjectsClone)
      {
        float itemStartTime = item.Key - eventDisplayDuration / 2f;
        if (parentAnimationStartTime + timepassed <= itemStartTime)
          continue;
        if ( booleanActionRecord.attachedObject != item.Value)
          continue;
        if (removeFromAttachedObjects.ContainsValue(item.Value))
          continue;
        GameObject obj1 = getCachedGameObjectByName(item.Value);
        GameObject obj1Clone = GameObject.Instantiate(obj1);
        obj1Clone.AddComponent<WorldCoordinateTransformMatcher>();
        ReplaceMaterialAlpha(obj1Clone, alphaForClones);
        SetAnimationClipForChildObjectClone(obj1Clone, obj1, parentAnimationStartTime, itemStartTime, timeTillPause, false);
        if(obj1.name == booleanActionRecord.attachedObject)
        {
          AnimationPauser pauser = obj1Clone.GetComponent<AnimationPauser>();
          HandleBooleanActionPlayerPerformedChecks(booleanActionRecord, obj1, pauser);
        }
        foreach (var col in obj1Clone.GetComponents<Collider>())
        {
          col.enabled = false;
        }
        foreach (var rb in obj1Clone.GetComponents<Rigidbody>())
        {
          rb.isKinematic = true;
        }
        foreach (var item2 in attachedObjects)
        {
          if (item2.Value == item.Value)
            removeFromAttachedObjects.Add(item2.Key, item2.Value);
        }
      }
      foreach (var item in removeFromAttachedObjects)
        localAttachedObjectsClone.Remove(item.Key);

      yield return new WaitForSeconds(0.001f);
    }

  }
  Valve.VR.SteamVR_Action lastAction = null;
  

  public void displayButtonTutorial(VrTutorialGen.Event tutEvent)
  {
    if (lastAction != null)
    {
      try
      {
        lastAction.HideOrigins();
      }
      catch (NullReferenceException) { Debug.LogWarning("action.HideOrigins() not possible without connected device or " + lastAction.fullPath + " not set on current device."); } //Happens if run without headset connected
    }
    string actionName = "";

    if (tutEvent.EventType_ == VrTutorialGen.Event.EventType.BOOLEAN_ACTION)
    {
      var booleanEvent = (VrTutorialGen.BooleanActionRecord)tutEvent;
      actionName = booleanEvent.GetName();
    }
    else if (tutEvent.EventType_ == VrTutorialGen.Event.EventType.VECTOR1_ACTION)
    {
      var vecEvent = (VrTutorialGen.Vector1ActionRecord)tutEvent;
      actionName = vecEvent.GetName();
    }
    else if (tutEvent.EventType_ == VrTutorialGen.Event.EventType.VECTOR2_ACTION)
    {
      var vecEvent = (VrTutorialGen.Vector2ActionRecord)tutEvent;
      actionName = vecEvent.GetName();
    }
    else if (tutEvent.EventType_ == VrTutorialGen.Event.EventType.VECTOR3_ACTION)
    {
      var vecEvent = (VrTutorialGen.Vector3ActionRecord)tutEvent;
      actionName = vecEvent.GetName();
    }
    else if (tutEvent.EventType_ == VrTutorialGen.Event.EventType.POSE_ACTION)
    {
      var vecEvent = (VrTutorialGen.PoseActionRecord)tutEvent;
      actionName = vecEvent.GetName();
    }
    if (actionName != "")
    {
      Valve.VR.SteamVR_Action action = (Valve.VR.SteamVR_Action)Valve.VR.SteamVR_Action.FindExistingActionForPartialPath(actionName);


      try { action.ShowOrigins(); }
      catch (NullReferenceException) { Debug.LogWarning("action.ShowOrigins() not possible without connected device or "+actionName+" not set on current device."); } //Happens if run without headset connected
      lastAction = action;
    }
    /*if(tutEvent.EventType_ == VrTutorialGen.Event.EventType.BOOLEAN_ACTION)
    {
      var booleanEvent = (VrTutorialGen.BooleanActionRecord)tutEvent;
      //((Valve.VR.ISteamVR_Action_In)booleanEvent.Action_Boolean)

      //foreach (var set in Valve.VR.SteamVR_ActionSet_Manager.rawActiveActionSetArray)
      {
        //ulong handle = set.ulActionSet;
        //var actionset = Valve.VR.SteamVR_ActionSet_Manager.GetSetFromHandle(handle);
        Valve.VR.SteamVR_Action action = (Valve.VR.SteamVR_Action)Valve.VR.SteamVR_Action.FindExistingActionForPartialPath(booleanEvent.GetName());
        action.ShowOrigins();
        lastAction = action;
//        Valve.VR.OpenVR.Input.ShowActionOrigins(handle, action.handle);
//        Valve.VR.OpenVR.Input.show
        // actionset.ShowBindingHints(action);
      }

    }
    */
    // Valve.VR.SteamVR_PartialInputBindings;
    // foreach (var a in Valve.VR.SteamVR_Input.actionsBoolean) {
    // Debug.Log(a.fullPath, a.GetActiveDevice, a.GetSourceMap);



    //}
    /*   var currentActionsFile =  Valve.VR.SteamVR_Input.actionFile;
      string currentBindingDirectory = Valve.VR.SteamVR_Input.GetActionsFileFolder();
      foreach (var a in currentActionsFile.actions)*/

  }
}
