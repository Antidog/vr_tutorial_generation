﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CloneFadeout : MonoBehaviour
{
  public float remainingDuration = 0;
  public bool thisIsMainObject = false;
  private static readonly int HasHighlight = Shader.PropertyToID("_HasHighlight");

  void Start()
  {
    StartCoroutine(FadeoutEffect());
  }

  void Update()
  {
    if(TutorialConfigurator.Instance.ghostTutorialScript.PlayerPerformedAction)
    {
      if (thisIsMainObject)
        FindObjectOfType<GhostTutorialScript>().LastPlayedEventHasEnded = true;
      StopCoroutine("FadeoutEffect");
      Destroy(gameObject);
    }
  }
  IEnumerator FadeoutEffect()
  {
    TextMeshPro tmPro = gameObject.GetComponentInChildren<TextMeshPro>();
    while (remainingDuration > 0.5f)
    {
      ReplaceMaterialAlpha(gameObject, 0.4f, true);
      while (remainingDuration > 0.5f) //gets increased externally
      {
        float wait = remainingDuration - 0.5f;
        remainingDuration = 0.5f;
        yield return new WaitForSeconds(wait);
      }

      while (remainingDuration >= 0.00f && remainingDuration <= 0.5f)
      {
        if (tmPro)
        {
          Color newColor = tmPro.color;
          newColor.a -= 1;
          tmPro.color = newColor;
        }
        ReplaceMaterialAlpha(gameObject, remainingDuration, true);
        remainingDuration -= 0.01f;
        yield return new WaitForSeconds(0.01f);
      }
    }
    if (thisIsMainObject)
      FindObjectOfType<GhostTutorialScript>().LastPlayedEventHasEnded = true;
    Destroy(gameObject);
  }

  void ReplaceMaterialAlpha(GameObject gameObjectWithMeshRenderer, float alpha, bool doMakeTransparent = true)
  {
    MeshRenderer renderer = gameObjectWithMeshRenderer.GetComponent<MeshRenderer>();
    if (!renderer)
      renderer = gameObjectWithMeshRenderer.GetComponentInChildren<MeshRenderer>();
    if (!renderer)
      return;

    var material = new Material(renderer.material);
    material.SetFloat(HasHighlight, doMakeTransparent ? 1 : 0);
    renderer.material = material;

  }
  //https://forum.unity.com/threads/change-rendering-mode-via-script.476437/ other way to make things transperant
}
