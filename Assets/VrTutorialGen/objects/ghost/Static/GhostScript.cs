﻿using Assets.VrTutorialGen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class GhostScript : MonoBehaviour
{
  public Animator ghostAnimator;
  List<VrTutorialGen.Event> ghostEvents = null;
  public List<VrTutorialGen.CollisionRecord> collisionRecords = new List<VrTutorialGen.CollisionRecord>();
  public List<VrTutorialGen.TriggerRecord> triggerRecords = new List<VrTutorialGen.TriggerRecord>();
  public List<VrTutorialGen.Vector1ActionRecord> ghostVector1ActionRecords = new List<VrTutorialGen.Vector1ActionRecord>();
  public List<VrTutorialGen.Vector2ActionRecord> ghostVector2ActionRecords = new List<VrTutorialGen.Vector2ActionRecord>();
  public List<VrTutorialGen.Vector3ActionRecord> ghostVector3ActionRecords = new List<VrTutorialGen.Vector3ActionRecord>();
  public List<VrTutorialGen.BooleanActionRecord> ghostBooleanActionRecords = new List<VrTutorialGen.BooleanActionRecord>();
  public List<VrTutorialGen.PoseActionRecord> ghostPoseActionRecords = new List<VrTutorialGen.PoseActionRecord>();
  public List<VrTutorialGen.AnimationSyncRecord> ghostAnimationSyncRecords = new List<VrTutorialGen.AnimationSyncRecord>();
  public List<VrTutorialGen.GameEndRecord> ghostGameActionRecords = new List<VrTutorialGen.GameEndRecord>();
  static List<KeyValuePair<string, SteamVR_Input_Sources>> poseActionPathsModified = new List<KeyValuePair<string, SteamVR_Input_Sources>>();
  static List<VrTutorialGen.CollisionRecord> collisionOccurencesBlocked = new List<VrTutorialGen.CollisionRecord>();
  static List<VrTutorialGen.TriggerRecord> triggerOccurencesBlocked = new List<VrTutorialGen.TriggerRecord>();
  private float StartTime;
  System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
  private void Awake()
  {
    sw.Start();
    ResetModifiedGhostData();
  }
  void Start()
  {
    if (ghostAnimator == null)
      ghostAnimator = gameObject.GetComponent<Animator>();
    SteamVR_Input.isGhostInput = true;
    foreach (var tracker in gameObject.GetComponentsInChildren<UnityEngine.SpatialTracking.TrackedPoseDriver>())
    {
      tracker.enabled = false;
    }
    {
      var tracker = gameObject.GetComponent<UnityEngine.SpatialTracking.TrackedPoseDriver>();
      if(tracker)
        tracker.enabled = false;
    }

  }
  void Update()
  {
    if (SteamVR_Input.isGhostInput && SteamVR.active) //only present if vr headset is connected
    {
      var behaviour = GameObject.FindObjectOfType<SteamVR_Behaviour>();
      if (behaviour)
        behaviour.enabled = false;
      var renderer = GameObject.FindObjectOfType<SteamVR_Render>();
      if (renderer)
        renderer.enabled = false;
    }
    PerformCurrentActions();
  }

  float end = 0;
  float first = float.MaxValue;
  void performBooleanActions()
  {
    List<VrTutorialGen.BooleanActionRecord> booleanActionRecordsPerformed = new List<VrTutorialGen.BooleanActionRecord>();
    foreach (var booleanActionRecord in ghostBooleanActionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(booleanActionRecord.PerformedAt))
        break;
     // Debug.Log("trying to perform action " + booleanActionRecord.GetName() + " " + booleanActionRecord.ActionStarts + " @" + (booleanActionRecord.PerformedAt));

      var actionBoolean = SteamVR_Input.GetBooleanActionFromPath(booleanActionRecord.GetName());
      SteamVR_Input_Sources source = booleanActionRecord.Input_Source;
      if (booleanActionRecord.ActionStarts)
        SteamVR_Input.GetBooleanActionFromPath(booleanActionRecord.GetName()).InvokeStateDown(source);
      else
        SteamVR_Input.GetBooleanActionFromPath(booleanActionRecord.GetName()).InvokeStateUp(source);
      booleanActionRecordsPerformed.Add(booleanActionRecord);

      if (ghostEvents.Contains((VrTutorialGen.Event)booleanActionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)booleanActionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in booleanActionRecordsPerformed)
    {
      ghostBooleanActionRecords.Remove(actionRecord);
    }
  }
  void performAnimationSyncActions()
  {
    List<VrTutorialGen.AnimationSyncRecord> animationSyncActionsPerformed = new List<VrTutorialGen.AnimationSyncRecord>();
    foreach (var ghostAnimationRecord in ghostAnimationSyncRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(ghostAnimationRecord.PerformedAt))
        break;
      //Debug.Log("trying to perform animation sync @" + (ghostAnimationRecord.PerformedAt));
      ghostAnimator.enabled = true;
      ghostAnimator.Play("Ghost", -1, ghostAnimationRecord.AnimationTime / clip.length);
      try
      {

      }
      catch (Exception ex)
      {
        Debug.LogError(ex);
      }
      animationSyncActionsPerformed.Add(ghostAnimationRecord);
    }
    foreach (var actionRecord in animationSyncActionsPerformed)
    {
      ghostAnimationSyncRecords.Remove(actionRecord);
    }
  }

  void performPoseActions()
  {
    List<VrTutorialGen.PoseActionRecord> poseActionRecordsPerformed = new List<VrTutorialGen.PoseActionRecord>();
    foreach (var poseActionRecord in ghostPoseActionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(poseActionRecord.PerformedAt - 0.05f)) //-Time.fixedUnscaledDeltaTime Time.deltaTime to ensure this is happening about 1 frame before any realease action that already might access this data
        break;
      //Debug.Log("[POSE] trying to perform action " + poseActionRecord.GetName() + " " + poseActionRecord.ActionStarts + " @" + (poseActionRecord.PerformedAt) + "     Velocity: (" + poseActionRecord.GhostPoseActionData.pose.vVelocity.v0.ToString() + "," + poseActionRecord.GhostPoseActionData.pose.vVelocity.v1.ToString() + ","+ poseActionRecord.GhostPoseActionData.pose.vVelocity.v2.ToString() + ")" );
      var actionPose = SteamVR_Input.GetPoseActionFromPath(poseActionRecord.GetName());
      SteamVR_Input_Sources source = poseActionRecord.Input_Source;
      string name = poseActionRecord.GetName();
      SteamVR_Input.GetPoseActionFromPath(name).setGhostPoseActionData(source, VrTutorialGen.SerializeablePoseStructs.PseudoConvertInputPoseActionData(poseActionRecord.GhostPoseActionData));
      KeyValuePair<string, SteamVR_Input_Sources> posePair = new KeyValuePair<string, SteamVR_Input_Sources>(name, source);
      if (!poseActionPathsModified.Contains(posePair))
        poseActionPathsModified.Add(posePair);
      poseActionRecordsPerformed.Add(poseActionRecord);


      if (ghostEvents.Contains((VrTutorialGen.Event)poseActionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)poseActionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in poseActionRecordsPerformed)
    {
      ghostPoseActionRecords.Remove(actionRecord);
    }
  }
  void performCollisionIgnoreActions()
  {
    List<VrTutorialGen.CollisionRecord> collisionRecordsPerformed = new List<VrTutorialGen.CollisionRecord>();
    foreach (var collisionRecord in collisionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(collisionRecord.PerformedAt))
        break;
      collisionRecordsPerformed.Add(collisionRecord);

      if (ghostEvents.Contains((VrTutorialGen.Event)collisionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)collisionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var collisionRecord in collisionRecords)
    {
      /* if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(collisionRecord.PerformedAt - 1f)) //-Time.fixedUnscaledDeltaTime Time.deltaTime to ensure this is happening about 1 frame before any realease action that already might access this data
         break;*/
      if (collisionRecord.shouldBeIgnoredThisIteration == false || collisionRecord.ActionStarts == false)
        continue;
      string selfName = collisionRecord.Self_name;//VrTutorialGen.Event.GetAlias(collisionRecord.Self_name);
      string otherName = VrTutorialGen.Event.GetAlias(collisionRecord.Other_name);
      Debug.Log("[COLL] trying to ignore collision between " + selfName + " and " + otherName + " @" + (collisionRecord.PerformedAt));
      try
      {
        GameObject other = GameObject.Find(otherName);
        GameObject self = GameObject.Find(selfName);
        Collider otherCollider = other.GetComponentInChildren<Collider>();
        Collider selfCollider = self.GetComponentInChildren<Collider>();
        Physics.IgnoreCollision(selfCollider, otherCollider, true);
        Physics.IgnoreCollision(otherCollider, selfCollider, true);

      }
      catch (Exception ex)
      {
        Debug.LogError(ex);
      }
      if (!collisionOccurencesBlocked.Contains(collisionRecord))
        collisionOccurencesBlocked.Add(collisionRecord);
    }
    foreach (var actionRecord in collisionRecordsPerformed)
    {
      collisionRecords.Remove(actionRecord);
    }
  }


  void performTriggerIgnoreActions()
  {
    List<VrTutorialGen.TriggerRecord> triggerRecordsPerformed = new List<VrTutorialGen.TriggerRecord>();
    foreach (var triggerRecord in triggerRecords)
    {
      /* if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(collisionRecord.PerformedAt - 1f)) //-Time.fixedUnscaledDeltaTime Time.deltaTime to ensure this is happening about 1 frame before any realease action that already might access this data
         break;*/
      triggerRecordsPerformed.Add(triggerRecord);
      if (triggerRecord.shouldBeIgnoredThisIteration == false)
        continue;
      string selfName = triggerRecord.Self_name;//VrTutorialGen.Event.GetAlias(collisionRecord.Self_name);
      string otherName = VrTutorialGen.Event.GetAlias(triggerRecord.Other_name);
      Debug.Log("[TRIGG] trying to ignore collision between " + selfName + " and " + otherName + " @" + (triggerRecord.PerformedAt));
      try
      {
        GameObject other = GameObject.Find(otherName);
        GameObject self = GameObject.Find(selfName);
        Collider otherCollider = other.GetComponentInChildren<Collider>();
        Collider selfCollider = self.GetComponentInChildren<Collider>();
        Physics.IgnoreCollision(selfCollider, otherCollider, true);
        Physics.IgnoreCollision(otherCollider, selfCollider, true);

      }
      catch (Exception ex)
      {
        Debug.LogError(ex);
      }
      if (!triggerOccurencesBlocked.Contains(triggerRecord))
        triggerOccurencesBlocked.Add(triggerRecord);

      if (ghostEvents.Contains((VrTutorialGen.Event)triggerRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)triggerRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in triggerRecordsPerformed)
    {
      triggerRecords.Remove(actionRecord);
    }
  }


  void performVector1Actions()
  {

    List<VrTutorialGen.Vector1ActionRecord> actionRecordsPerformed = new List<VrTutorialGen.Vector1ActionRecord>();
    foreach (var ActionRecord in ghostVector1ActionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(ActionRecord.PerformedAt))
        break;
      //Debug.Log("[VEC1] trying to perform action " + ActionRecord.GetName() + " " + ActionRecord.ActionStarts + " @" + (ActionRecord.PerformedAt) + "("+Time.realtimeSinceStartup+")     axis: " + ActionRecord.Action_Axis + "     delta: "+ ActionRecord.Action_Delta);
      var actionSingle = SteamVR_Input.GetSingleActionFromPath(ActionRecord.GetName());
      SteamVR_Input_Sources source = ActionRecord.Input_Source;
      actionSingle.setGhostData(ActionRecord.Action_Axis, ActionRecord.Action_Delta);
      actionRecordsPerformed.Add(ActionRecord);

      if (ghostEvents.Contains((VrTutorialGen.Event)ActionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)ActionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in actionRecordsPerformed)
    {
      ghostVector1ActionRecords.Remove(actionRecord);
    }
  }
  void performVector2Actions()
  {
    List<VrTutorialGen.Vector2ActionRecord> actionRecordsPerformed = new List<VrTutorialGen.Vector2ActionRecord>();
    foreach (var ActionRecord in ghostVector2ActionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(ActionRecord.PerformedAt))
        break;
      // Debug.Log("[VEC2] trying to perform action " + ActionRecord.GetName() + " " + ActionRecord.ActionStarts + " @" + (ActionRecord.PerformedAt) + "     axis: " + ActionRecord.Action_Axis + "     delta: " + ActionRecord.Action_Delta);
      var actionVec2 = SteamVR_Input.GetVector2ActionFromPath(ActionRecord.GetName());
      SteamVR_Input_Sources source = ActionRecord.Input_Source;
      actionVec2.setGhostData(ActionRecord.Action_Axis, ActionRecord.Action_Delta);
      actionRecordsPerformed.Add(ActionRecord);
      if (ghostEvents.Contains((VrTutorialGen.Event)ActionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)ActionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in actionRecordsPerformed)
    {
      ghostVector2ActionRecords.Remove(actionRecord);
    }
  }
  void performVector3Actions()
  {
    List<VrTutorialGen.Vector3ActionRecord> actionRecordsPerformed = new List<VrTutorialGen.Vector3ActionRecord>();
    foreach (var ActionRecord in ghostVector3ActionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(ActionRecord.PerformedAt))
        break;
      // Debug.Log("[VEC3] trying to perform action " + ActionRecord.GetName() + " " + ActionRecord.ActionStarts + " @" + (ActionRecord.PerformedAt) + "     axis: " + ActionRecord.Action_Axis + "     delta: " + ActionRecord.Action_Delta);
      var actionVec3 = SteamVR_Input.GetVector2ActionFromPath(ActionRecord.GetName());
      SteamVR_Input_Sources source = ActionRecord.Input_Source;
      actionVec3.setGhostData(ActionRecord.Action_Axis, ActionRecord.Action_Delta);
      actionRecordsPerformed.Add(ActionRecord);
      if (ghostEvents.Contains((VrTutorialGen.Event)ActionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)ActionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in actionRecordsPerformed)
    {
      ghostVector3ActionRecords.Remove(actionRecord);
    }
  }
  void performGhostGameActions()
  {
    List<VrTutorialGen.GameEndRecord> actionRecordsPerformed = new List<VrTutorialGen.GameEndRecord>();
    foreach (var ActionRecord in ghostGameActionRecords)
    {
      if (TutorialEventManager.Instance.iterationDependendTimeIsInFuture(ActionRecord.PerformedAt))
        break;
      actionRecordsPerformed.Add(ActionRecord);
      if (ActionRecord.ActionStarts == true && ghostGameActionRecords.Count == 1) //Game End
        TutorialEventManager.GhostGameEndReached();
      if (ghostEvents.Contains((VrTutorialGen.Event)ActionRecord) && TutorialEventManager.geIstVideoRecording())
      {
        float VideoTime = TutorialEventManager.Instance.getVideoDependedTime();
        ghostEvents[ghostEvents.IndexOf((VrTutorialGen.Event)ActionRecord)].VideoTime = VideoTime;
      }
    }
    foreach (var actionRecord in actionRecordsPerformed)
    {
      ghostGameActionRecords.Remove(actionRecord);
    }
  }
  private void PerformCurrentActions()
  {
    performAnimationSyncActions();
    performPoseActions();
    performCollisionIgnoreActions();
    performTriggerIgnoreActions();
    performBooleanActions();
    performVector1Actions();
    performVector2Actions();
    performVector3Actions();
    performGhostGameActions();
    checkFinished();
  }

  /// <summary>
  /// resets the Ghost Pose Action Data and Ignored Collisions set in this iteration
  /// Needed during relevancy checking to ensure that none of the ghost data is present in the next iteration. (it persists when a new scene is loaded)
  /// </summary>
  public static void ResetModifiedGhostData()
  {
    ResetModifiedGhostPoseActionData();
  }
  private static void ResetModifiedGhostPoseActionData()
  {
    //Set PoseAction ghost data back to 0,0,0 - for next iteration - this has 0 effect for the current iteration.
    foreach (var posepair in poseActionPathsModified)
    {
      var oldValue = SteamVR_Input.GetPoseActionFromPath(posepair.Key).getGhostPoseActionData(posepair.Value);
      InputPoseActionData_t empty = new InputPoseActionData_t();
      SteamVR_Input.GetPoseActionFromPath(posepair.Key).setGhostPoseActionData(posepair.Value, empty);
    }
    poseActionPathsModified.Clear();
  }

  private static void ResetModifiedGhostCollisionData()
  {

    foreach (var collisionRecord in collisionOccurencesBlocked)
    {
      try
      {
        string selfName = collisionRecord.Self_name;//VrTutorialGen.Event.GetAlias(collisionRecord.Self_name);
        string otherName = VrTutorialGen.Event.GetAlias(collisionRecord.Other_name);

        GameObject other = GameObject.Find(otherName);
        GameObject self = GameObject.Find(selfName);
        Collider otherCollider = other.GetComponentInChildren<Collider>();
        Collider selfCollider = self.GetComponentInChildren<Collider>();
        Physics.IgnoreCollision(selfCollider, otherCollider, false);
        Physics.IgnoreCollision(otherCollider, selfCollider, false);
      }
      catch (Exception ex)
      {
        Debug.LogError(ex);
      }
    }
    collisionOccurencesBlocked.Clear();
    foreach (var triggerRecord in triggerOccurencesBlocked)
    {

      try
      {
        string selfName = triggerRecord.Self_name;//VrTutorialGen.Event.GetAlias(collisionRecord.Self_name);
        string otherName = triggerRecord.Other_name;

        GameObject other = GameObject.Find(otherName);
        GameObject self = GameObject.Find(selfName);
        Collider otherCollider = other.GetComponentInChildren<Collider>();
        Collider selfCollider = self.GetComponentInChildren<Collider>();
        Physics.IgnoreCollision(selfCollider, otherCollider, false);
        Physics.IgnoreCollision(otherCollider, selfCollider, false);
      }
      catch (Exception ex)
      {
        Debug.LogError(ex);
      }
    }
    triggerOccurencesBlocked.Clear();
  }

  public void checkFinished()
  {
    if (ghostVector1ActionRecords.Count == 0 &&
        ghostVector2ActionRecords.Count == 0 &&
        ghostVector3ActionRecords.Count == 0 &&
        ghostBooleanActionRecords.Count == 0 &&
        ghostPoseActionRecords.Count == 0 &&
        ghostGameActionRecords.Count == 0)
    {
      ResetModifiedGhostData();
      hasFinished = true;
    }
  }
  public bool hasFinished = false;

  public void SetGhostEvents(List<VrTutorialGen.Event> ghostEvents)
  {
    ResetModifiedGhostCollisionData();
    this.ghostEvents = ghostEvents;
    ghostBooleanActionRecords.Clear();
    ghostVector1ActionRecords.Clear();
    ghostVector2ActionRecords.Clear();
    ghostVector3ActionRecords.Clear();
    ghostPoseActionRecords.Clear();
    triggerRecords.Clear();
    collisionRecords.Clear();
    collisionOccurencesBlocked.Clear();
    foreach (var ghostEvent in ghostEvents)
    {
      try
      {
        switch (ghostEvent.EventType_)
        {
          case VrTutorialGen.Event.EventType.BOOLEAN_ACTION:
            ghostBooleanActionRecords.Add((VrTutorialGen.BooleanActionRecord)ghostEvent);
            break;
          case VrTutorialGen.Event.EventType.VECTOR1_ACTION:
            ghostVector1ActionRecords.Add((VrTutorialGen.Vector1ActionRecord)ghostEvent);
            break;
          case VrTutorialGen.Event.EventType.VECTOR2_ACTION:
            ghostVector2ActionRecords.Add((VrTutorialGen.Vector2ActionRecord)ghostEvent);
            break;
          case VrTutorialGen.Event.EventType.VECTOR3_ACTION:
            ghostVector3ActionRecords.Add((VrTutorialGen.Vector3ActionRecord)ghostEvent);
            break;
          case VrTutorialGen.Event.EventType.POSE_ACTION:
            ghostPoseActionRecords.Add((VrTutorialGen.PoseActionRecord)ghostEvent);
            break;
          case VrTutorialGen.Event.EventType.ANIMATION_SYNC:
            ghostAnimationSyncRecords.Add((VrTutorialGen.AnimationSyncRecord)ghostEvent);
            break;
          case VrTutorialGen.Event.EventType.END:
            ghostGameActionRecords.Add((VrTutorialGen.GameEndRecord)ghostEvent);
            if (ghostEvent.ActionStarts)
            {
              end = ghostEvent.PerformedAt;
              first = System.Math.Min(first, ghostEvent.PerformedAt);
            }
            else
              first = ghostEvent.PerformedAt;
            break;

          case VrTutorialGen.Event.EventType.COLLISION:
            collisionRecords.Add((VrTutorialGen.CollisionRecord)ghostEvent);
            break;

          case VrTutorialGen.Event.EventType.TRIGGER:
            triggerRecords.Add((VrTutorialGen.TriggerRecord)ghostEvent);
            break;
          default:
            Debug.Log("Unimplemented event type: "+ghostEvent.EventType_);
            break;

        }
      }

      catch (System.Exception ex)
      {
        Debug.LogError(ex);
      }
    }
    Debug.Log("Number of events {" + ghostEvents.Count + "} BooleanActions {" + ghostBooleanActionRecords.Count + "}, CollisionRecords {" + collisionRecords.Count + "}, TriggerRecords {" + triggerRecords.Count + "}, Vec1Actions {" + ghostVector1ActionRecords.Count + "}, Vec2Actions {" + ghostVector2ActionRecords.Count + "}, Vec3Actions {" + ghostVector3ActionRecords.Count + "}, PoseActions {" + ghostPoseActionRecords.Count + "}, AnimationSync {" + ghostAnimationSyncRecords.Count + "} @" + Time.unscaledTime);


  }

  static AnimationClip clip;
  public AnimationClip GetCurrentGhostAnimation()
  {
    return clip;
  }
  public void SetGhostFile(string ghostClipFileName)
  {
    string trimmedname = ghostClipFileName.Replace(".anim", "").Replace("Assets/Resources/", "").Replace("/","\\");
    if( clip == null)
      clip = Resources.Load<AnimationClip>(trimmedname);
    if(clip == null)
      Debug.LogError("clip: "+trimmedname+ "  not found");
    // Debug.Log("loading ghostanimaton clip: " + ghostClipFileName);

    StartCoroutine(StartAnimation());
  }
  IEnumerator StartAnimation()
  {
    AnimatorOverrideController aoc = new AnimatorOverrideController(ghostAnimator.runtimeAnimatorController);
    AnimationClipOverrides clipOverrides = new AnimationClipOverrides(1);
    aoc.name = "GhostAnimationOverride";
    clip.name = "GhostAnimation";
    clipOverrides["OldGhostAnimation 1"] = clip;
    aoc["OldGhostAnimation 1"] = clip;
    aoc.GetOverrides(clipOverrides);
    ghostAnimator.runtimeAnimatorController = aoc;


    StartTime = Time.unscaledTime + sw.ElapsedMilliseconds/1000f;
    sw.Reset();
    float timeToWait = first + TutorialEventManager.iterationStartTime - StartTime;
    float iterationStartTimeShifted = TutorialEventManager.iterationStartTime - timeToWait * 0.9f;
    float adjustedTimeToWait = first + iterationStartTimeShifted - StartTime; //adjust time to wait such that we still have a tiny delay to start synchornously with events
    Debug.Log("[INFO] "+ TutorialEventManager.iteration+ " Original iterationStartTime:"+ TutorialEventManager.iterationStartTime + "  start time: " + StartTime + "first: " + first + 
      "\nHence shift iteration Start time " + (iterationStartTimeShifted) + " and wait for "+ adjustedTimeToWait + "s");

    TutorialEventManager.iterationStartTime = Mathf.Max(iterationStartTimeShifted, 0); //Reduce time to wait by 90% to quicken replay (by simulating an earlier start)
    if (adjustedTimeToWait > 0)
       yield return new WaitForSeconds(adjustedTimeToWait);
    float animationDuration = clip.length;
    float eventsDuration = (end+ TutorialEventManager.iterationStartTime - Time.unscaledTime);
    float speed = animationDuration / eventsDuration;
   // Debug.Log("length: " + clip.length + " duration: " + (eventsDuration) +"end(@"+end+")"+ " speed: "+speed);
    ghostAnimator.speed = 1/speed * Time.timeScale;
    //Time.timeScale = 1f;
    ghostAnimator.enabled = false;
   // ghostAnimator.playbackTime = 0;
  }
}
