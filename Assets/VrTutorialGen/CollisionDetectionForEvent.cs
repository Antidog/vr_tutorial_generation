using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetectionForEvent : MonoBehaviour
{
  public GhostTutorialScript parent;
  public string objectNameToCheck;
  private void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.name == objectNameToCheck)
    {
      parent.PlayerPerformedAction = true;
      Destroy(this);
    }
  }
}