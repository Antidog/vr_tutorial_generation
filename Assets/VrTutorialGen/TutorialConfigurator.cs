﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class TutorialConfigurator : MonoBehaviour
{
  public enum ExecutionMode
  {
    step1RecordNewGhost,
    step2FindRelevantEvents,
    step3aGenerateTutorialGhost,
    step3bGenerateTutorialGhost,
    step4PlayerPlayThrough,
    step4PlayerPlayThroughWithVideo,
    manualDebugConfig,
    reGenerateVideo,
    nothing
    //    fullTutorialGeneration,
  }
  public ExecutionMode executionMode;
  public TutorialEventManager tutorialEventManager;
  public VideoTutorialScript videoTutorialScript;
  public GhostTutorialScript ghostTutorialScript;

  [Header("GhostGameDisplayMode")]
  [Tooltip("recommended= true .Triggers and Collisons with the same object names are collapsed into the first occurence of them. It (most likely) isn't interesting for the user to have 20x A collides with B in the tutorial.")]
  public bool ShowOnlySingleCollisionAndSingleTrigger = true;

  public float OffsetTimeForGhostAnimationsToText = 1f;
  // Start is called before the first frame update
  public static TutorialConfigurator Instance;
  void Awake()
  {
    if (Instance == null)
    {
      Instance = this;
      DontDestroyOnLoad(Instance);
    }
    else
    {
      Destroy(gameObject);
      return;
    }
    handleStart();
  }
  void handleStart()
  { 
    switch (Instance.executionMode)
    {
      case ExecutionMode.manualDebugConfig:
        break;
      case ExecutionMode.step4PlayerPlayThrough:
        {
          Instance.tutorialEventManager.loadNewScenesMode = false;
          Instance.tutorialEventManager.IsGhostGame = false;
          Instance.tutorialEventManager.doNotRecord = true;
          Instance.tutorialEventManager.recordVideoOnFirstIteration = false;
          Instance.tutorialEventManager.ghostGameId = getMostRecentTutorialOrGameId(ID_TYPE.GAME_ID);
          Instance.ghostTutorialScript.idFromTutorial = getMostRecentTutorialOrGameId(ID_TYPE.TUTORIAL_ID);
          Instance.ghostTutorialScript.deduplicateCollisonsAndTriggers = ShowOnlySingleCollisionAndSingleTrigger;
          Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.PlayGhostAnimation;
          Instance.ghostTutorialScript.eventsOffset = OffsetTimeForGhostAnimationsToText;
          Instance.ghostTutorialScript.gameObject.SetActive(true);
          Instance.ghostTutorialScript.enabled = true;
          Instance.videoTutorialScript.gameObject.SetActive(false);
        }
        break;
      case ExecutionMode.step4PlayerPlayThroughWithVideo:
        {
          Instance.tutorialEventManager.loadNewScenesMode = false;
          Instance.tutorialEventManager.IsGhostGame = false;
          Instance.tutorialEventManager.doNotRecord = true;
          Instance.tutorialEventManager.recordVideoOnFirstIteration = false;
          Instance.tutorialEventManager.ghostGameId = getMostRecentTutorialOrGameId(ID_TYPE.GAME_ID);
          Instance.ghostTutorialScript.idFromTutorial = getMostRecentTutorialOrGameId(ID_TYPE.TUTORIAL_ID);
          Instance.ghostTutorialScript.deduplicateCollisonsAndTriggers = ShowOnlySingleCollisionAndSingleTrigger;
          Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.PlayGhostAnimation;
          Instance.ghostTutorialScript.eventsOffset = OffsetTimeForGhostAnimationsToText;
          Instance.ghostTutorialScript.gameObject.SetActive(true);
          Instance.ghostTutorialScript.enabled = true;
          Instance.videoTutorialScript.idFromTutorial = getMostRecentTutorialOrGameId(ID_TYPE.TUTORIAL_ID);
          Instance.videoTutorialScript.gameObject.SetActive(true);
          Instance.videoTutorialScript.enabled = true;
        }
        break;
      case ExecutionMode.step1RecordNewGhost:
        {
          Instance.tutorialEventManager.IsGhostGame = false;
          Instance.tutorialEventManager.doNotRecord = false;
          Instance.tutorialEventManager.recordVideoOnFirstIteration = false;
          Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.Nothing;
          Instance.ghostTutorialScript.gameObject.SetActive(false);
          Instance.videoTutorialScript.gameObject.SetActive(false);
          Instance.tutorialEventManager.loadNewScenesMode = false;
        }
        break;
      case ExecutionMode.step2FindRelevantEvents:
        {
          Instance.tutorialEventManager.IsGhostGame = true;
          Instance.tutorialEventManager.doNotRecord = true;
          Instance.tutorialEventManager.recordVideoOnFirstIteration = true;
          Instance.tutorialEventManager.usePreviousProgress = false;
          Instance.tutorialEventManager.ghostGameId = -1;
          Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.Nothing;
          Instance.ghostTutorialScript.gameObject.SetActive(false);
          Instance.videoTutorialScript.gameObject.SetActive(false);
          Instance.tutorialEventManager.loadNewScenesMode = true;
        }
        break;
      case ExecutionMode.step3aGenerateTutorialGhost:
        {
          Instance.tutorialEventManager.IsGhostGame = true;
          Instance.tutorialEventManager.doNotRecord = false;
          Instance.tutorialEventManager.recordVideoOnFirstIteration = false;
          Instance.tutorialEventManager.usePreviousProgress = false;
          Instance.tutorialEventManager.ghostGameId = getMostRecentTutorialOrGameId(ID_TYPE.GAME_ID);
          Instance.tutorialEventManager.previousIterationIdToContinueFrom = -1;
          Instance.tutorialEventManager.previousEssentialEventsIdToContinueFrom = -1;
          Instance.ghostTutorialScript.idFromTutorial = getMostRecentTutorialOrGameId(ID_TYPE.TUTORIAL_ID);
          Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.RecordGhostAnimation;
          Instance.ghostTutorialScript.gameObject.SetActive(true);
          Instance.ghostTutorialScript.enabled = true;
          Instance.videoTutorialScript.gameObject.SetActive(false);
          Instance.tutorialEventManager.loadNewScenesMode = false;
        }
        break;
      case ExecutionMode.step3bGenerateTutorialGhost:
        {
          Instance.tutorialEventManager.IsGhostGame = true;
          Instance.tutorialEventManager.doNotRecord = false;
          Instance.tutorialEventManager.recordVideoOnFirstIteration = false;
          Instance.tutorialEventManager.usePreviousProgress = false;
          Instance.tutorialEventManager.ghostGameId = getMostRecentTutorialOrGameId(ID_TYPE.GAME_ID);
          Instance.tutorialEventManager.previousIterationIdToContinueFrom = -1;
          Instance.tutorialEventManager.previousEssentialEventsIdToContinueFrom = -1;
          Instance.ghostTutorialScript.idFromTutorial = getMostRecentTutorialOrGameId(ID_TYPE.TUTORIAL_ID);
          Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.RecordGhostAnimationStage2;
          Instance.ghostTutorialScript.gameObject.SetActive(true);
          Instance.ghostTutorialScript.enabled = true;
          Instance.videoTutorialScript.gameObject.SetActive(false);
          Instance.tutorialEventManager.loadNewScenesMode = false;
        }
        break;
      case ExecutionMode.reGenerateVideo:
        Instance.tutorialEventManager.IsGhostGame = true;
        Instance.tutorialEventManager.doNotRecord = true;
        Instance.tutorialEventManager.recordVideoOnFirstIteration = true;
        Instance.tutorialEventManager.usePreviousProgress = false;
        Instance.tutorialEventManager.ghostGameId = getMostRecentTutorialOrGameId(ID_TYPE.GAME_ID);
        Instance.ghostTutorialScript.mode = GhostTutorialScript.GHOST_SCRIPT_MODE.Nothing;
        Instance.ghostTutorialScript.gameObject.SetActive(false);
        Instance.videoTutorialScript.gameObject.SetActive(false);
        Instance.tutorialEventManager.loadNewScenesMode = false;
        break;
      case ExecutionMode.nothing:
        Instance.tutorialEventManager.IsGhostGame = false;
        Instance.tutorialEventManager.doNotRecord = true;
        Instance.tutorialEventManager.recordVideoOnFirstIteration = false;
        Instance.tutorialEventManager.usePreviousProgress = false;
        Instance.tutorialEventManager.enabled = false;
        Instance.ghostTutorialScript.gameObject.SetActive(false);
        Instance.videoTutorialScript.gameObject.SetActive(false);
        Instance.tutorialEventManager.loadNewScenesMode = false;
        break;
        /*  case ExecutionMode.fullTutorialGeneration:
            Debug.LogError("full Tutorial Generation in one go not yet implemented");
            break;*/
    }
    Instance.tutorialEventManager.enabled = true;
    Instance.tutorialEventManager.gameObject.SetActive(true);
  }
  enum ID_TYPE { GAME_ID, TUTORIAL_ID}
  static int getMostRecentTutorialOrGameId(ID_TYPE type)
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    int gameId = -1;
    int tutorialId = -1;
    cmd.CommandText = "Select id, gameId from tutorialevents order by id desc limit 1";
    var reader = cmd.ExecuteReader();
    if (reader.Read())
    {
      tutorialId = reader.GetInt32(0);
      gameId = reader.GetInt32(1);
    }
    con.Close();
    con.Dispose();
    con = null;
    if (type == ID_TYPE.GAME_ID)
      return gameId;
    else if (type == ID_TYPE.TUTORIAL_ID)
      return tutorialId;
    return -1;
  }
}
