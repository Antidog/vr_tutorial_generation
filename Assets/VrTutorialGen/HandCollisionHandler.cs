using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

//this class is a reduced/adapted version of SteamVRs HandPhysics.cs which handles only enabling/disabling of the colliders
public class HandCollisionHandler : MonoBehaviour
{

  [Tooltip("Hand collider prefab to instantiate")]
  public HandCollider handColliderPrefab;

  [HideInInspector]
  public Hand hand;

  [HideInInspector]
  public HandCollider handCollider;

  const float collisionReenableClearanceRadius = 0.1f;

  private bool initialized = false;

  private bool collisionsEnabled = true;
  Collider[] clearanceBuffer = new Collider[1];

  private void Start()
  {
    hand = GetComponent<Hand>();
    //spawn hand collider and link it to us
    if (hand.IsGhost)
    {
      handCollider = handColliderPrefab.GetComponent<HandCollider>();
      handCollider.enabled = true;
    }
  }

  void Update()
  {
    UpdatePositions();
  }

  private void UpdatePositions()
  {
    // disable collisions when holding something
    if (hand.currentAttachedObject != null)
    {
      collisionsEnabled = false;
    }
    else
    {
      // wait for area to become clear before reenabling collisions
      if (!collisionsEnabled)
      {
        clearanceBuffer[0] = null;
        Physics.OverlapSphereNonAlloc(hand.objectAttachmentPoint.position, collisionReenableClearanceRadius, clearanceBuffer);
        // if we don't find anything in the vicinity, reenable collisions!
        if (clearanceBuffer[0] == null)
        {
          collisionsEnabled = true;
        }
      }
    }

    handCollider.SetCollisionDetectionEnabled(collisionsEnabled);
  }
}