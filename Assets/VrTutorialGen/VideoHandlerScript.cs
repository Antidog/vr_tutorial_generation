﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoHandlerScript : MonoBehaviour
{
  public double startTime = 0;
  public double endTime = 0;
  public float DEFAULT_DURATION = 2;
  public double time = 0;
  public VideoPlayer videoPlayer;
  private void Start()
  {
    videoPlayer.Prepare();
  }
  // Update is called once per frame
  void Update()
  {
    if(videoPlayer.isPrepared)
    {
      if (startTime == endTime)
        return;
      time = videoPlayer.time;
      /*if (startTime < 0)
        startTime = 0;*/
      if (videoPlayer.time >= endTime || videoPlayer.time < startTime)
      {
        videoPlayer.time = startTime;
        videoPlayer.Play();
      }
      if (!videoPlayer.isPlaying)
        videoPlayer.Play();

    }
  }
}
