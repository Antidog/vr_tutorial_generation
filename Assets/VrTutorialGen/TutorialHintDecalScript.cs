using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class TutorialHintDecalScript : MonoBehaviour
{
  public DecalProjector projectorZ;
  public DecalProjector projectorX;
  private GameObject sourceObject;
  private Vector3 destinationPos;
  private bool initalized = false;
  private bool destinationSet = false;
  public AnimationPauser pauser;
  void Update()
  {
    if (pauser && pauser.finalPosition)
      setDestination(pauser.finalPosition.position);
    if (initalized && destinationSet)
      Reposition();
  }
  public void setDestination(Vector3 destinationPos)
  {
    this.destinationPos = new Vector3(destinationPos.x, this.transform.position.y, destinationPos.z);
    destinationSet = true;
  }
  public void init(GameObject sourceObject)
  {
    this.sourceObject = sourceObject;
    initalized = true;
  }
  public void DoReset()
  {
    projectorX.enabled = false;
    projectorZ.enabled = false;
    pauser = null;
    initalized = false;
    destinationSet = false;
  }

  private void Reposition()
  {
    Vector3 sourcePos = new Vector3(sourceObject.transform.position.x, transform.position.y, sourceObject.transform.position.z);
    float lengthX = Mathf.Abs(destinationPos.x - sourcePos.x);
    float lengthZ = Mathf.Abs(destinationPos.z - sourcePos.z);
    projectorZ.size = new Vector3(projectorZ.size.x, lengthZ, projectorZ.size.z);
    projectorX.size = new Vector3(projectorX.size.x, lengthX, projectorX.size.x);
    projectorZ.uvScale = new Vector2(projectorZ.uvScale.x, Mathf.Abs(destinationPos.z - sourcePos.z));
    projectorX.uvScale = new Vector2(projectorX.uvScale.x, Mathf.Abs(destinationPos.x - sourcePos.x));
    projectorZ.pivot = new Vector3(projectorZ.size.x / 2, projectorZ.size.y / 2, projectorZ.size.z / 2);
    projectorX.pivot = new Vector3(projectorX.size.x / 2, projectorX.size.y / 2, projectorX.size.z / 2);
    if (sourcePos.z > destinationPos.z)
    {
      projectorZ.transform.position = new Vector3(sourcePos.x, sourcePos.y, destinationPos.z);
    }
    else
    {
      projectorZ.transform.position = new Vector3(sourcePos.x, sourcePos.y, destinationPos.z - lengthZ);
    }

    if (sourcePos.x > destinationPos.x)
    {
      projectorX.transform.position = new Vector3(sourcePos.x - lengthX, sourcePos.y, destinationPos.z);
    }
    else
    {
      projectorX.transform.position = new Vector3(sourcePos.x, sourcePos.y, destinationPos.z);
    }
    projectorX.enabled = true;
    projectorZ.enabled = true;
  }
}