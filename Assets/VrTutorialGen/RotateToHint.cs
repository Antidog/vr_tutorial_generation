using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToHint : MonoBehaviour
{
  public Vector3 lookat = new Vector3();
  public Camera vrCamera;
  void Update()
  {
    Vector3 newPos = new Vector3(vrCamera.transform.position.x, 0.05f, vrCamera.transform.position.z);
    this.transform.position = newPos;
    lookat = AnimationPauser.currentHintPositon;
    lookat.y = transform.position.y; 
    transform.rotation = Quaternion.LookRotation(newPos- lookat);

  }
}
