﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.VrTutorialGen
{
  [Serializable]
  public class PlayerPlaythroughStats
  {
    public PlayerPlaythroughStats(bool gameWasWon, float gameDuration, long startTimeInUnixFormat)
    {
      this.gameDuration = gameDuration;
      this.gameWasWon = gameWasWon;
      this.startTimeInUnixFormat = startTimeInUnixFormat;
    }
    [SerializeField]
    public bool gameWasWon = false;
    [SerializeField]
    public float gameDuration;
    [SerializeField]
    public long startTimeInUnixFormat;
  }
}
