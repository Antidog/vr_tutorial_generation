﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ListTextSelected : MonoBehaviour
{
  ListScript listScript;
  ListItemScript listItemScript; //inParent
  private void OnCollisionEnter(Collision collision)
  {
    //only collisions with hands/fingers matter
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>() && !waitForExit)
    {
      waitForExit = true;
      listScript.SelectListItem(listItemScript);
    }
  }
  private void OnCollisionExit(Collision collision)
  {
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>())
    {
      waitForExit = false;
    }
  }

  bool waitForExit = false;
  double time = 0;  
  // Update is called once per frame
  void Update()
  {
    
    if (waitForExit)
    {
      //For detecting how long it is pressed
      time += Time.deltaTime;
    }
    else
    {
      time = 0;
    }
  }
  private void Start()
  {
    listItemScript = GetComponentInParent<ListItemScript>();
    listScript = GetComponentInParent<ListScript>();
  }

}
