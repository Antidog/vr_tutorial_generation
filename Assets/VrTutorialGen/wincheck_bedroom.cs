using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wincheck_bedroom : MonoBehaviour
{
  public Animator doorAnimator;
  private static readonly int IsPlaying = Animator.StringToHash("_isPlaying");
  bool cutRobeAnimationStarted = false;
  public AGTIV.Scripts.PunchingGlove.PunchHand punchHand;
  bool failedBecauseTriggerdBeforePunch = false;
  void Update()
  {
    if(doorAnimator.GetBool(IsPlaying) && !cutRobeAnimationStarted && !failedBecauseTriggerdBeforePunch)
    {
      if (punchHand.currentBoundingBox.z >= 0.1)
      {
        cutRobeAnimationStarted = true;
        TutorialEventManager.Win();
      }
      else {
        Debug.LogWarning("Win blocked this iteration - punchglove not used before hitting the button");
        failedBecauseTriggerdBeforePunch = true;

      }
      //the room should not be completed without using the punching glove - this is a measure to avoid that the ghost just presses the button on the right with the teddy manualy
      //If the door opens before punching something went wrong
    }
  }
}
