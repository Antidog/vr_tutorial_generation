﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoHandler : MonoBehaviour
{
  public TextMeshPro TextObj;

  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }
  public void RefreshText(int actionCount)
  {

    TextObj.text = "You win. \nRecorded " + actionCount + " actions.";
  }
  public void RefreshText(string text)
  {
    TextObj.text = text;
  }
}
