using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DropOffDetectionForEvent : MonoBehaviour
{
  public GameObject gameObjectThatShouldBeDetached;
  public GhostTutorialScript parent;
  public AnimationPauser pauser;
  public Vector3? finalPositon = null;
  public float maximumDistanceBetweenFinalPositonAndObject = 0.5f;
  public List<Hand> hands = null;
  private void Start()
  {
    if (hands == null)
    {
      hands = new List<Hand>();
      foreach (var hand in FindObjectsOfType<Hand>())
      {
        hands.Add(hand);
      }
    }
  }
  void Update()
  {

    if (parent.PlayerPerformedAction)
    {
      Destroy(this);
    }
    else if (finalPositon != null || pauser && pauser.finalPosition != null)
    {
      if(finalPositon == null)
        finalPositon = pauser.finalPosition.position;
      float currentMagnitude = (gameObjectThatShouldBeDetached.transform.position - finalPositon).Value.magnitude;
      if (currentMagnitude <= maximumDistanceBetweenFinalPositonAndObject && !heldInAHand())
      {
        parent.PlayerPerformedAction = true;
        Destroy(this);
      }
    }
  }
  bool heldInAHand()
  {
    foreach(var hand in hands)
    {
      if (hand.currentAttachedObject == gameObjectThatShouldBeDetached)
        return true;
    }
    return false;
  }
}
