﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class SaveList : MonoBehaviour
{
  public ListScript list;
  private void OnCollisionEnter(Collision collision)
  {
    //only collisions with hands/fingers matter
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>() && !waitForExit)
    {
      waitForExit = true;
      list.SaveList();
    }
  }
  private void OnCollisionExit(Collision collision)
  {
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>())
    {
      waitForExit = false;
    }
  }
  bool waitForExit = false;
}
