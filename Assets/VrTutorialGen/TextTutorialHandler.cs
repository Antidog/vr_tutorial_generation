using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextTutorialHandler : MonoBehaviour
{
  public TextMeshPro headLine;
  public TextMeshPro numberLine;
  public TextMeshPro subLine;
  public MeshRenderer background;
  public MeshRenderer line;
  private void Start()
  {
    subLine.alpha = 0;
    headLine.alpha = 0;
    background.enabled = false;
    line.enabled = false;
    numberLine.alpha = 0;
  }
  public void enableTextTutorial(string textHeadline, string textSubline, string textNumber)
  {
    headLine.text = textHeadline;
    headLine.alpha = 255;
    subLine.text = textSubline;
    subLine.alpha = 255;
    numberLine.text = textNumber;
    numberLine.alpha = 255;
    background.enabled = true;
    line.enabled = true;
    this.gameObject.SetActive(true);
  }

  public void diableTextTutorial()
  {
    transform.SetParent(null);
    headLine.text = "";
    subLine.text = "";
    numberLine.text = "";
    this.gameObject.SetActive(false);
    background.enabled = false;
    line.enabled = false;
    Destroy(gameObject);
  }
}
//useless comment to tell git this file changed
