﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ListUpDownScript : MonoBehaviour
{
  public bool increment = true;
  public ListScript listScript;
  private void OnCollisionEnter(Collision collision)
  {
    //only collisions with hands/fingers matter
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>() && !waitForExit)
    {
      waitForExit = true;
      if (increment)
        listScript.offset++;
      else
        listScript.offset--;
    }
  }
  private void OnCollisionExit(Collision collision)
  {
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>())
    {
      waitForExit = false;
    }
  }
  bool waitForExit = false;
  
}
