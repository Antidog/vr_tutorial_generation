﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlattformScript : MonoBehaviour
{
  public GameObject objectToCollideWith;
  public bool isCollidingWithCorrectObject= false;
  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.rigidbody.gameObject == objectToCollideWith)
      isCollidingWithCorrectObject = true;
  }
  private void OnCollisionExit(Collision collision)
  {

    if (collision.rigidbody.gameObject == objectToCollideWith)
      isCollidingWithCorrectObject = false;
  }
}
