﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.Recorder;
using UnityEditor.Recorder.Encoder;
using UnityEditor.Recorder.Input;
#endif
using UnityEngine;
using UnityEngine.Assertions;

#if UNITY_EDITOR
public class VideoRecordingManager : MonoBehaviour
{
  private MovieRecorderSettings settings;
  private RecorderController controller = null;
  RecorderControllerSettings controllerSettings;

  bool prepared = false;
  public bool doNotRecord = false;
  public float recordingdelayInSeconds = -1;
  float startTime = -0f;
  private void Update()
  {
    if (recordingdelayInSeconds < 0f && controller != null && controller.IsRecording())
      recordingdelayInSeconds = Time.realtimeSinceStartup-TutorialEventManager.iterationStartTime;
  }
  public void PrepareRecording(string outputPath)
  {
    if (doNotRecord)
      return;
    settings = ScriptableObject.CreateInstance<MovieRecorderSettings>();
    MovieRecorderSettings videoRecorder = ScriptableObject.CreateInstance<MovieRecorderSettings>();
    videoRecorder.name = "MainCamera"; //"PlayerView"
    settings.EncoderSettings = new CoreEncoderSettings
    {
      EncodingQuality = CoreEncoderSettings.VideoEncodingQuality.High,
      Codec = CoreEncoderSettings.OutputCodec.MP4
    };
    var camSettings = new CameraInputSettings();
    camSettings.CameraTag = "MainCamera";
    camSettings.OutputHeight = 1280;
    camSettings.OutputHeight = 720;
    videoRecorder.ImageInputSettings = camSettings;
    new GameViewInputSettings
    {
      OutputWidth = 1280,
      OutputHeight = 720
    };
    settings.OutputFile = outputPath;
    videoRecorder.AudioInputSettings.PreserveAudio = false;
    controllerSettings =  ScriptableObject.CreateInstance<RecorderControllerSettings>();
    controllerSettings.AddRecorderSettings(settings);
    controllerSettings.SetRecordModeToManual();
    controllerSettings.FrameRate = 90;
    controllerSettings.CapFrameRate = true;
    //settings.FrameRatePlayback = FrameRatePlayback.Variable;
    videoRecorder.Enabled = true;
    prepared = true;
    RecorderOptions.VerboseMode = false;
    //RecorderOptions.VerboseMode = true;
  }

  public void StartRecording()
  {
    if (doNotRecord)
      return;
    if (!prepared)
      Debug.LogError("StartRecording called Before PrepareRecording");

    RecorderOptions.VerboseMode = false;
    controller = new RecorderController(controllerSettings);
    controller.PrepareRecording();
    startTime = Time.time;
    controller.StartRecording();
  }
  public void StopRecording()
  {
    if (doNotRecord)
      return;
    if (controller == null)
    {
      Debug.LogError("StopRecording() called before StartRecording()");
      return;
    }
    controller.StopRecording();
  }
  public float VideoTime()
  {
    return Time.time-startTime;
  }
}

#endif
#if !UNITY_EDITOR
public class VideoRecordingManager : MonoBehaviour
{
  
  bool prepared = false;
  public bool doNotRecord = false;
  public float recordingdelayInSeconds = -1;
  float startTime = -0f;
  private void Update()
  {
  }
  public void PrepareRecording(string outputPath)
  {
  }

  public void StartRecording()
  {
  
  }
  public void StopRecording()
  {
 
  }
  public float VideoTime()
  {
    return Time.time - startTime;
  }
}
#endif