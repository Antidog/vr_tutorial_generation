using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetectionForEvent : MonoBehaviour
{
  public GhostTutorialScript parent;
  public string objectNameToCheck;
  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.name == objectNameToCheck)
    {
      parent.PlayerPerformedAction = true;
      Destroy(this);
    }
  }
}
