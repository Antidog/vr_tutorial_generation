﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class GameEndRecord : Event
  {
    public GameEndRecord(bool win, bool ActionStarts = true)
    {
      this.ActionStarts = true;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.END;
      GameWasWon = win;
    }
    public readonly bool GameWasWon = false;
    public override string GetAutomatedText()
    {
      if (!this.ActionStarts)
        return "Game Started";
      if (GameWasWon == true)
        return "Game is won";
      else
        return "Game is lost";
    }
  }
}
