﻿//using slightly modified code from: https://stackoverflow.com/a/2772060 because some class names where refactored during development and old recorded data should still be usable to avoid recording games again. Probalbly not needed in the final version at all.
//TODO: Delte this class?
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Assets.VrTutorialGen.events
{
  public sealed class SerializationBinderHelper : SerializationBinder
  {
      public override Type BindToType(string assemblyName, string typeName)
      {
        Type typeToDeserialize = null;

        if (typeName == "VrTutorialGen.ActionRecord")
          typeName = "VrTutorialGen.BooleanActionRecord";
        typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
                                            typeName, assemblyName));
        return typeToDeserialize;
      }
  }
}
