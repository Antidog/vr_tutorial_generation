﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class AnimationSyncRecord : Event
  {
    public AnimationSyncRecord(float animationTime)
    {
      this.ActionStarts = true;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.ANIMATION_SYNC;
      AnimationTime = animationTime;
    }
    public readonly float AnimationTime = 0;
    public override string GetAutomatedText()
    {
        return "GhostAnimator should be at " +AnimationTime+" playback time at " + PerformedAt +" gameTime";
    }
  }
}