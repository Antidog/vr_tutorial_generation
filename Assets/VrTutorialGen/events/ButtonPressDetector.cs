﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ButtonPressDetector : MonoBehaviour
{
  public Hand correspondingHand;
  public SteamVR_Input_Sources controller;
#if UNITY_EDITOR
  void Start()
  {
//    SteamVR_Input.actionsPose[0].AddOnChangeListener()
    foreach (var action in SteamVR_Input.actionsBoolean)
    {
      action.AddOnChangeListener(BooleanChangeHandler, controller);
    }
    foreach (var action in SteamVR_Input.actionsVector3)
    {
      action.AddOnChangeListener(Vector3ChangeHandler, controller);
    }
    foreach (var action in SteamVR_Input.actionsVector2)
    {
      action.AddOnChangeListener(Vector2ChangeHandler, controller);
    }
    foreach (var action in SteamVR_Input.actionsSingle)
    {
      action.AddOnChangeListener(Vector1ChangeHandler, controller);
    }
    foreach (var action in SteamVR_Input.actionsPose)
    {
      action.onTempPoseActionDataChanged += Action_onTempPoseActionDataChanged;
    }
    //TODO: other Action types?
  }
  string mostRecentObject = "";
  private void Update()
  {
    if (correspondingHand == null)
    {
      foreach (var hand in FindObjectsOfType<Hand>())
      {
        if (hand.handType == controller)
        {
          correspondingHand = hand;

        }

      }
    }
    if (correspondingHand && correspondingHand.currentAttachedObject)
      mostRecentObject = correspondingHand.currentAttachedObject.name;
    else if (correspondingHand && correspondingHand.hoveringInteractable)
      mostRecentObject = correspondingHand.hoveringInteractable.name;
  }
  private void Action_onTempPoseActionDataChanged(SteamVR_Action_Pose fromAction, SteamVR_Input_Sources fromSource, bool active, InputPoseActionData_t ghostPoseActionData)
  {
    TutorialEventManager.Instance.AddPoseRecordAction(new VrTutorialGen.PoseActionRecord(fromAction, fromSource, active, ghostPoseActionData));
  }

  private void BooleanChangeHandler(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
  {
    String attachedObject = "";
    if (correspondingHand && correspondingHand.currentAttachedObject)
      attachedObject = correspondingHand.currentAttachedObject.name;
    else if (correspondingHand && correspondingHand.hoveringInteractable)
      attachedObject = correspondingHand.hoveringInteractable.name;
    else if (correspondingHand && !newState)
      attachedObject = mostRecentObject;
    TutorialEventManager.Instance.AddBooleanRecordAction(new VrTutorialGen.BooleanActionRecord(fromAction, fromSource, newState, attachedObject));
  }


  private void Vector3ChangeHandler(SteamVR_Action_Vector3 fromAction, SteamVR_Input_Sources fromSource, Vector3 axis, Vector3 delta)
  {
    TutorialEventManager.Instance.AddVector3RecordAction(new VrTutorialGen.Vector3ActionRecord(fromAction, fromSource, axis, delta));
  }
  private void Vector2ChangeHandler(SteamVR_Action_Vector2 fromAction, SteamVR_Input_Sources fromSource, Vector2 axis, Vector2 delta)
  {
    TutorialEventManager.Instance.AddVector2RecordAction(new VrTutorialGen.Vector2ActionRecord(fromAction, fromSource, axis, delta));
  }
  private void Vector1ChangeHandler(SteamVR_Action_Single fromAction, SteamVR_Input_Sources fromSource, float axis, float delta)
  {
    TutorialEventManager.Instance.AddVector1RecordAction(new VrTutorialGen.Vector1ActionRecord(fromAction, fromSource, axis, delta));
  }
#endif
}
