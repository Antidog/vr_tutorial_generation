﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR;
using Valve.VR.InteractionSystem;

public class CollisionDetectionHands : MonoBehaviour
{
  Hand hand;
  // Start is called before the first frame update
  void Start()
  {
    hand = gameObject.GetComponent<Hand>();
  }
  void Update()
  {
  }
  private void OnCollisionEnter(Collision collision)
  {
    if (!hand.ObjectIsAttached(collision.gameObject))
      TutorialEventManager.Instance.AddRecordAction(new VrTutorialGen.CollisionRecord(this.gameObject, collision, true));
  }
  private void OnCollisionExit(Collision collision)
  {
    if (!hand.ObjectIsAttached(collision.gameObject))
      TutorialEventManager.Instance.AddRecordAction(new VrTutorialGen.CollisionRecord(this.gameObject, collision, false));
  }

  private void OnTriggerEnter(Collider other)
  {
    if (!hand.ObjectIsAttached(other.gameObject))
      TutorialEventManager.Instance.AddRecordAction(new VrTutorialGen.TriggerRecord(this.gameObject, other, true));
  }

  private void OnTriggerExit(Collider other)
  {
    if (!hand.ObjectIsAttached(other.gameObject))
      TutorialEventManager.Instance.AddRecordAction(new VrTutorialGen.TriggerRecord(this.gameObject, other, false));
  }
}

