﻿using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
  List<CollisionOccurance> recentCollisions = new List<CollisionOccurance>();


  protected void OnCollisionEnter(Collision collision)
  {
    VrTutorialGen.CollisionRecord enterRecord = new VrTutorialGen.CollisionRecord(this.gameObject, collision, true);
    if (CollisionOccurance.InList(recentCollisions, new CollisionOccurance(enterRecord)) == null)
    {
      if(!TutorialEventManager.Instance){return;}
      recentCollisions.Add(new CollisionOccurance(enterRecord));
      TutorialEventManager.Instance.AddRecordAction(enterRecord);
    }
  }
  protected void OnCollisionExit(Collision collision)
  {
    VrTutorialGen.CollisionRecord leaveRecord = new VrTutorialGen.CollisionRecord(this.gameObject, collision, false);
    VrTutorialGen.CollisionRecord enterRecord = new VrTutorialGen.CollisionRecord(this.gameObject, collision, true);
    CollisionOccurance inList = CollisionOccurance.InList(recentCollisions, new CollisionOccurance(enterRecord));
    if (inList != null)
    {
      if(!TutorialEventManager.Instance){return;}
      recentCollisions.Remove(inList);
      TutorialEventManager.Instance.AddRecordAction(leaveRecord);
    }
  }

  protected void OnTriggerEnter(Collider other)
  {
    TutorialEventManager.Instance.AddRecordAction(new VrTutorialGen.TriggerRecord(this.gameObject, other, true));
  }

  protected void OnTriggerExit(Collider other)
  {
    TutorialEventManager.Instance.AddRecordAction(new VrTutorialGen.TriggerRecord(this.gameObject, other, false));
  }

  class CollisionOccurance
  {
    //helper class for list compare
    public VrTutorialGen.CollisionRecord record;
    public CollisionOccurance(VrTutorialGen.CollisionRecord record)
    {
      this.record = record;
    }
    public bool Equals(CollisionOccurance compare)
    {
      bool sameEnteringStatus = compare.record.ActionStarts == this.record.ActionStarts;
      bool sameObjects = compare.record.Col.gameObject == this.record.Col.gameObject;
      bool sameCollidingObjects = compare.record.Self.gameObject == this.record.Self.gameObject;
      if (sameObjects && sameEnteringStatus && sameCollidingObjects)
        return true;
      return false;
    }

    //regular Contains & remove from list does not work with our special case, therefore overwrite of equals + this function
    public static CollisionOccurance InList(List<CollisionOccurance> list, CollisionOccurance compare)
    {
      foreach(var element in list )
        if(compare.Equals(element))
            return element;
      return null;
    }
  }
}

