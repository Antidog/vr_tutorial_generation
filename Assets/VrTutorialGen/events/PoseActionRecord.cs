﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

namespace VrTutorialGen
{
  [System.Serializable]
  public class PoseActionRecord : Event
  {
    public PoseActionRecord(SteamVR_Action_Pose action, SteamVR_Input_Sources fromSource, bool status, InputPoseActionData_t ghostPoseActionData)
    {
      Releavant = false;
      Input_Source = fromSource;
      Action_Pose = action;
      ActionStarts = status;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.POSE_ACTION;
      GhostPoseActionData = SerializeablePoseStructs.PseudoConvertInputPoseActionData(ghostPoseActionData);
    }


    public readonly SerializeablePoseStructs.InputPoseActionData_g GhostPoseActionData = new SerializeablePoseStructs.InputPoseActionData_g();
    public readonly SteamVR_Action_Pose Action_Pose;
    public readonly SteamVR_Input_Sources Input_Source = SteamVR_Input_Sources.Waist;

    public override string GetAutomatedTutorialText()
    {
      return "Throw the object";
    }
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(Action_Pose.GetPath());
      else
        return GetAlias(Action_Pose.GetPath()) + " ends";
    }
    public string GetName()
    {
      return Action_Pose.GetPath();
    }
  }
  //contains the same structs as openvr_api.cs but marked serializable 
  public class SerializeablePoseStructs
  {
    public static InputPoseActionData_g PseudoConvertInputPoseActionData(InputPoseActionData_t toconvert)
    {
      InputPoseActionData_g value = new InputPoseActionData_g();
      value.pose.bDeviceIsConnected = toconvert.pose.bDeviceIsConnected;
      value.pose.bPoseIsValid = toconvert.pose.bPoseIsValid;
      value.pose.eTrackingResult = toconvert.pose.eTrackingResult;
      value.pose.mDeviceToAbsoluteTracking.m0 = toconvert.pose.mDeviceToAbsoluteTracking.m0;
      value.pose.mDeviceToAbsoluteTracking.m1 = toconvert.pose.mDeviceToAbsoluteTracking.m1;
      value.pose.mDeviceToAbsoluteTracking.m2 = toconvert.pose.mDeviceToAbsoluteTracking.m2;
      value.pose.mDeviceToAbsoluteTracking.m3 = toconvert.pose.mDeviceToAbsoluteTracking.m3;
      value.pose.mDeviceToAbsoluteTracking.m4 = toconvert.pose.mDeviceToAbsoluteTracking.m4;
      value.pose.mDeviceToAbsoluteTracking.m5 = toconvert.pose.mDeviceToAbsoluteTracking.m5;
      value.pose.mDeviceToAbsoluteTracking.m6 = toconvert.pose.mDeviceToAbsoluteTracking.m6;
      value.pose.mDeviceToAbsoluteTracking.m7 = toconvert.pose.mDeviceToAbsoluteTracking.m7;
      value.pose.mDeviceToAbsoluteTracking.m8 = toconvert.pose.mDeviceToAbsoluteTracking.m8;
      value.pose.mDeviceToAbsoluteTracking.m9 = toconvert.pose.mDeviceToAbsoluteTracking.m9;
      value.pose.mDeviceToAbsoluteTracking.m10 = toconvert.pose.mDeviceToAbsoluteTracking.m10;
      value.pose.vAngularVelocity.v0 = toconvert.pose.vAngularVelocity.v0;
      value.pose.vAngularVelocity.v1 = toconvert.pose.vAngularVelocity.v1;
      value.pose.vAngularVelocity.v2 = toconvert.pose.vAngularVelocity.v2;
      value.pose.vVelocity.v0 = toconvert.pose.vVelocity.v0;
      value.pose.vVelocity.v1 = toconvert.pose.vVelocity.v1;
      value.pose.vVelocity.v2 = toconvert.pose.vVelocity.v2;
      value.bActive = toconvert.bActive;
      value.activeOrigin = toconvert.activeOrigin;
      return value;
    }
    public static InputPoseActionData_t PseudoConvertInputPoseActionData(InputPoseActionData_g toconvert)
    {
      InputPoseActionData_t value = new InputPoseActionData_t();
      value.pose.bDeviceIsConnected = toconvert.pose.bDeviceIsConnected;
      value.pose.bPoseIsValid = toconvert.pose.bPoseIsValid;
      value.pose.eTrackingResult = toconvert.pose.eTrackingResult;
      value.pose.mDeviceToAbsoluteTracking.m0 = toconvert.pose.mDeviceToAbsoluteTracking.m0;
      value.pose.mDeviceToAbsoluteTracking.m1 = toconvert.pose.mDeviceToAbsoluteTracking.m1;
      value.pose.mDeviceToAbsoluteTracking.m2 = toconvert.pose.mDeviceToAbsoluteTracking.m2;
      value.pose.mDeviceToAbsoluteTracking.m3 = toconvert.pose.mDeviceToAbsoluteTracking.m3;
      value.pose.mDeviceToAbsoluteTracking.m4 = toconvert.pose.mDeviceToAbsoluteTracking.m4;
      value.pose.mDeviceToAbsoluteTracking.m5 = toconvert.pose.mDeviceToAbsoluteTracking.m5;
      value.pose.mDeviceToAbsoluteTracking.m6 = toconvert.pose.mDeviceToAbsoluteTracking.m6;
      value.pose.mDeviceToAbsoluteTracking.m7 = toconvert.pose.mDeviceToAbsoluteTracking.m7;
      value.pose.mDeviceToAbsoluteTracking.m8 = toconvert.pose.mDeviceToAbsoluteTracking.m8;
      value.pose.mDeviceToAbsoluteTracking.m9 = toconvert.pose.mDeviceToAbsoluteTracking.m9;
      value.pose.mDeviceToAbsoluteTracking.m10 = toconvert.pose.mDeviceToAbsoluteTracking.m10;
      value.pose.vAngularVelocity.v0 = toconvert.pose.vAngularVelocity.v0;
      value.pose.vAngularVelocity.v1 = toconvert.pose.vAngularVelocity.v1;
      value.pose.vAngularVelocity.v2 = toconvert.pose.vAngularVelocity.v2;
      value.pose.vVelocity.v0 = toconvert.pose.vVelocity.v0;
      value.pose.vVelocity.v1 = toconvert.pose.vVelocity.v1;
      value.pose.vVelocity.v2 = toconvert.pose.vVelocity.v2;
      value.bActive = toconvert.bActive;
      value.activeOrigin = toconvert.activeOrigin;
      return value;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct InputPoseActionData_g //basically the same as InputPoseActionData_t but serializeable
    {
      public bool bActive;
      public ulong activeOrigin;
      public TrackedDevicePose_t pose;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct TrackedDevicePose_t
    {
      public HmdMatrix34_t mDeviceToAbsoluteTracking;
      public HmdVector3_t vVelocity;
      public HmdVector3_t vAngularVelocity;
      public ETrackingResult eTrackingResult;
      public bool bPoseIsValid;
      public bool bDeviceIsConnected;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct HmdVector3_t
    {
      public float v0; //float[3]
      public float v1;
      public float v2;
    }
    [System.Serializable]
    public struct HmdMatrix34_t
    {
      public float m0; //float[3][4]
      public float m1;
      public float m2;
      public float m3;
      public float m4;
      public float m5;
      public float m6;
      public float m7;
      public float m8;
      public float m9;
      public float m10;
      public float m11;
#if UNITY_5_3_OR_NEWER

      public Vector3 GetPosition()
      {
        return new Vector3(m3, m7, -m11);
      }

      public bool IsRotationValid()
      {
        return ((m2 != 0 || m6 != 0 || m10 != 0) && (m1 != 0 || m5 != 0 || m9 != 0));
      }

      public Quaternion GetRotation()
      {
        if (IsRotationValid())
        {
          float w = Mathf.Sqrt(Mathf.Max(0, 1 + m0 + m5 + m10)) / 2;
          float x = Mathf.Sqrt(Mathf.Max(0, 1 + m0 - m5 - m10)) / 2;
          float y = Mathf.Sqrt(Mathf.Max(0, 1 - m0 + m5 - m10)) / 2;
          float z = Mathf.Sqrt(Mathf.Max(0, 1 - m0 - m5 + m10)) / 2;

          _copysign(ref x, -m9 - -m6);
          _copysign(ref y, -m2 - -m8);
          _copysign(ref z, m4 - m1);

          return new Quaternion(x, y, z, w);
        }
        return Quaternion.identity;
      }

      private static void _copysign(ref float sizeval, float signval)
      {
        if (signval > 0 != sizeval > 0)
          sizeval = -sizeval;
      }
#endif

    }
  }
}