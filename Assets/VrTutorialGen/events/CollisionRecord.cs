﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace VrTutorialGen
{
  [System.Serializable]
  public class CollisionRecord : Event
  {
    [field: System.NonSerializedAttribute()]
    public GameObject Self { get; protected set; }
    [field: System.NonSerializedAttribute()]
    public Collision Col { get; protected set; }
    public string Other_name;
    public string Self_name;
    public CollisionRecord(GameObject self, Collision collision, bool enter)
    {
      Self = self;
      Col = collision;
      Other_name = collision.gameObject.name;
      Self_name = self.name;
      ActionStarts = enter;
      EventType_ = EventType.COLLISION;
      PerformedAt = Time.unscaledTime;
    }
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(Self_name) + " Collides with " + GetAlias(Other_name);
      else
        return GetAlias(Self_name) + " Collides with " + GetAlias(Other_name) + " ends";
    }

    public override string GetAutomatedTutorialText()
    {
      if (ActionStarts)
        return GetAlias(GetAlias(Self_name) + " has to collide with " + GetAlias(Other_name));
      else
        return GetAlias(GetAlias(Self_name) + " hast to stop colliding with " + GetAlias(Other_name));
    }
  }
}