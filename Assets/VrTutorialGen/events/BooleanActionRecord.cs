﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class BooleanActionRecord : Event
  {
    public string attachedObject = "";
    public BooleanActionRecord(SteamVR_Action_Boolean action, SteamVR_Input_Sources fromSource, bool status, string attachedObject)
    {
      this.attachedObject = attachedObject;
      Input_Source = fromSource;
      Action_Boolean = action;
      ActionStarts = status;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.BOOLEAN_ACTION;
    }
    public readonly SteamVR_Action_Boolean Action_Boolean;
    public readonly SteamVR_Input_Sources Input_Source = SteamVR_Input_Sources.Waist;
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(Action_Boolean.GetPath());
      else
        return GetAlias(Action_Boolean.GetPath()) + " ends";
    }
    public override string GetAutomatedTutorialText()
    {
      string booleanName = GetAlias(Action_Boolean.GetPath());
      if(ActionStarts)
        return "Press "+ booleanName + "";
      else
        return "Release " + booleanName + "";

    }
    public override string GetHeadlineText()
    {
      if (ActionStarts)
        return "Take the "+GetAlias(attachedObject);
      else
        return "Place the " + GetAlias(attachedObject);
    }
    public override string GetSublineText()
    {

      string booleanName = GetAlias(Action_Boolean.GetPath());
      if (ActionStarts)
        return "Press " + booleanName;
      else
        return "Release " + booleanName;
    }
    public string GetName()
    {
      return Action_Boolean.GetPath();
    }
  }
}