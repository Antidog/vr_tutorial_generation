﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class Vector2ActionRecord : Event
  {
    public readonly Vector2 Action_Axis;
    public readonly Vector2 Action_Delta;
    public Vector2ActionRecord(SteamVR_Action_Vector2 action, SteamVR_Input_Sources fromSource, Vector2 axis, Vector2 delta)
    {
      Input_Source = fromSource;
      Action_Vector2 = action;
      ActionStarts = true;
      Action_Axis = axis;
      Action_Delta = delta;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.VECTOR2_ACTION;
    }
    public readonly SteamVR_Action_Vector2 Action_Vector2;
    public readonly SteamVR_Input_Sources Input_Source = SteamVR_Input_Sources.Waist;
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(Action_Vector2.GetShortName());
      else
        return GetAlias(Action_Vector2.GetShortName())+ " ends";
    }
    public string GetName()
    {
      return Action_Vector2.GetPath();
    }
  }
}