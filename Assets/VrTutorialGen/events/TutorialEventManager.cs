﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Valve.VR;
using Mono.Data.Sqlite;
using System.Data;
using System;
using Valve.VR.InteractionSystem;
using Assets.VrTutorialGen.events;
using UnityEngine.SceneManagement;

public class TutorialEventManager : MonoBehaviour
{
  public static TutorialEventManager Instance { get; private set; }
  public InfoHandler infoHandler;
  public TabletManger tabletManger;
  [HideInInspector]
  public DatabaseHandler databaseHandler;
  public GameObject Player;
  public GhostScript Ghost;
  public List<VrTutorialGen.Event> ActionsPerformed { get; private set; }
  [Header("Settings for normal games")]
  public float STARTUP_TIME_THRESHOLD = 2.3333f; //Ignores all events in the first N seconds
  public bool InitialTimeThresholdPassed = false;
  [Header("Settings for ghost games")]
  public Boolean IsGhostGame = false;
  public int ghostGameId = -1;
  public Boolean recordVideoOnFirstIteration = false;

  public Boolean FindRelevantEvents = false;
  public List<VrTutorialGen.Event.EventType> eventTypesToCheckRelevancy = new List<VrTutorialGen.Event.EventType>() { VrTutorialGen.Event.EventType.BOOLEAN_ACTION, VrTutorialGen.Event.EventType.POSE_ACTION }; //TODO: find out which are really relevant

  [Space(10)]
  public bool usePreviousProgress = false;
  public int previousIterationIdToContinueFrom = -1;
  public int previousEssentialEventsIdToContinueFrom = -1;
  public static bool firstIterationIsOver = false;

  private static DateTime started = DateTime.Now;
  private Dictionary<int, List<VrTutorialGen.Event>> PreviousGames = new Dictionary<int, List<VrTutorialGen.Event>>();
  private string SaveFilepath;
  public enum FileType
  {
    NONE, ANIMATION, VIDEO
  }
  private void OnEnable()
  {
    SceneManager.sceneLoaded += OnSceneLoaded;
  }
  void OnDisable()
  {
    SceneManager.sceneLoaded -= OnSceneLoaded;
  }
  public static string getGhostFileNames(FileType withEnding, bool withPath = false, String clipFileName="")
  {
    String result = "Ghost_" + started.Year + "_" + started.Month + "_" + started.Day + "_" + started.Hour + "_" + started.Minute + "_" + started.Second + "_";
    if (clipFileName != "")
      result = clipFileName.Substring(clipFileName.LastIndexOf("/")+1, clipFileName.LastIndexOf(".anim")- clipFileName.LastIndexOf("/")-1);
    String resultWithStandardPath = "Assets/VrTutorialGen/objects/ghost/" + result;
    String resultWithAnimationPath = "Assets/Resources/ghost/" + result;
    String resultWithStreamingAssetsPath = Application.streamingAssetsPath +"/VrTutorialGen/"+ result;
    switch (withEnding)
    {
      case FileType.ANIMATION:
        if (withPath)
          return resultWithAnimationPath + ".anim";
        return result + ".anim";
        break;
      case FileType.NONE:
        if (withPath)
          return resultWithStandardPath;
        return result;
        break;
      case FileType.VIDEO:
        resultWithStreamingAssetsPath.Replace("/", "\\");
                if (withPath)
                   return resultWithStreamingAssetsPath;
                return result + ".mp4";
        break;
    }
    return result;
  }
  // Start is called before the first frame update
  public static int iteration = 0;
  public static float iterationStartTime = 0;
  /// <summary>
  /// return true if the given time is in the future - relative to the iteration start time.
  /// </summary>
  /// <param name="timeToCheck">the time to check if it is in the future</param>
  /// <returns></returns>
  public bool iterationDependendTimeIsInFuture(float timeToCheck)
  {
    if(IsVideoRecording) 
      return timeToCheck  > Time.time;
    return timeToCheck + iterationStartTime > Time.unscaledTime;
  }
  public float iterationDependendTime()
  {
    return Time.unscaledTime- iterationStartTime;
  }
  public float getVideoDependedTime()
  {
    return Instance.GetComponent<VideoRecordingManager>().VideoTime();
  }
  public bool doNotRecord = false;
  private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
  {
    if (Instance == null)
    {
      Instance = this;
      DontDestroyOnLoad(Instance);
    }
    else if (this != Instance)
    {
      Instance.Player = Player;
      Instance.Ghost = Ghost;
      Destroy(gameObject);
    }
    runNewGameCycleNextUpdate = true;
  }
  void Awake()
  {
    if (Instance == null)
    {
      Instance = this;
      DontDestroyOnLoad(Instance);
    }
    else if(this != Instance)
    {
      Instance.Player = Player;
      Instance.Ghost = Ghost;
      Destroy(gameObject);
    }
    runNewGameCycleNextUpdate = true;
  }
  bool runNewGameCycleNextUpdate = false;
  private void Update()
  {
    if(runNewGameCycleNextUpdate)
    {
      newGameCycle();
      runNewGameCycleNextUpdate = false;
    }
  }
  void newGameCycle()
  {
    TutorialEventManager.iterationStartTime = Time.unscaledTime;
    Instance.GameWasWon = false;
    Instance.GameHasEnded = false;
    Instance.Ghost = Ghost;
    Instance.Player = Player;

    Ghost.gameObject.SetActive(IsGhostGame);
    Player.SetActive(!IsGhostGame);
    databaseHandler = DatabaseHandler.getInstance();
    SaveFilepath = Application.persistentDataPath + "/save.dat";
    ActionsPerformed = new List<VrTutorialGen.Event>();
    if (!IsGhostGame && iteration == 0) //normal playthrough
    {
      if(!doNotRecord)
        StartCoroutine(startGhostAnimationRecording());
    }
    else if (iteration == 0) 
    {
      LoadGhostDataFromDB();
      if (recordVideoOnFirstIteration)
      {
        PrepareVideoFilesBeforeRecording(getGhostFileNames(FileType.NONE, true, ghostClipFileName));
        StartVideoRecording(getGhostFileNames(FileType.VIDEO, true, ghostClipFileName));
      }
      LoadNextIteration();
    }
    else if(FindRelevantEvents)
      RunWithManipulatedEvents();
    iteration++;
  }
  
  void RunWithManipulatedEvents()
  {
    logNumberOfRelevantEvents();
    if (!IsGhostGame)
    {
      Debug.LogError("Potentially unplanned programm flow, RunWithManipulatedEvents called without GhostGame=1");
    }
    if (iteration == 1)
    {
      manipulatedGhostGameEvents = new List<VrTutorialGen.Event>(originalGhostGameEvents);
      for (int i = 0; i < manipulatedGhostGameEvents.Count; i++)
      {
        manipulatedGhostGameEvents[i].Releavant = false;
      }
      //iteration++;
      // Ghost.ResetModifiedGhostPoseActionData();
      Debug.Log("[loadScene][manipulatedGhostGameEvents] count: " + manipulatedGhostGameEvents.Count);
      //SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }
    databaseHandler.SaveEssentialEvents(manipulatedGhostGameEvents, Instance.ghostGameId, iteration);
    List<VrTutorialGen.Event> relevantEvents = new List<VrTutorialGen.Event>();
    float duration = 1;
    if (AllEventsRelevant(previousManipulatedGhostGameEvents))
    {
      foreach (var ev in previousManipulatedGhostGameEvents)
      {
        if (ev.Releavant && eventTypesToCheckRelevancy.Contains(ev.EventType_))
        {
          relevantEvents.Add(ev);
          Debug.Log("[Result] " + ev.GetAutomatedText() + "  Is Relevant");
        }
        if(ev.EventType_ == VrTutorialGen.Event.EventType.END)
          duration = Mathf.Max(duration, previousManipulatedGhostGameEvents[previousManipulatedGhostGameEvents.Count - 1].PerformedAt);
      }
      PrintRelevantTutorialTexts(previousManipulatedGhostGameEvents);
      Instance.infoHandler.enabled = true;
      Instance.infoHandler.RefreshText(getRelevantTutorialTexts(previousManipulatedGhostGameEvents));
      //databaseHandler.SaveEssentialEvents(previousManipulatedGhostGameEvents, ghostId, iteration);
      databaseHandler.SaveTutorialEvents(relevantEvents, ghostGameId, getGhostFileNames(FileType.NONE, true, ghostClipFileName), duration);
      Time.timeScale = 0;
      Instance.tabletManger.ShowTabletManager(previousManipulatedGhostGameEvents, getGhostFileNames(FileType.NONE, true, ghostClipFileName), Ghost.GetCurrentGhostAnimation(), true);
      //StopCoroutine("TimeOutIteration");
    }
    if(iteration > 0)
    {
      Ghost.SetGhostEvents(manipulatedGhostGameEvents);
      Ghost.SetGhostFile(ghostClipFileName);
    }
  }
  void logNumberOfRelevantEvents()
  {
    List<VrTutorialGen.Event> relevantEvents = new List<VrTutorialGen.Event>();
    List<VrTutorialGen.Event> unclassifiedEvents = new List<VrTutorialGen.Event>();
    int unclassified_col = 0;
    int unclassified_bool = 0;
    int unclassified_pose = 0;
    int unclassified_trigg = 0;
    foreach (var ev in previousManipulatedGhostGameEvents)
    {
      if (ev.Releavant && eventTypesToCheckRelevancy.Contains(ev.EventType_))
      {
        relevantEvents.Add(ev);
      }
      else if (eventTypesToCheckRelevancy.Contains(ev.EventType_))
      {
        unclassifiedEvents.Add(ev);
        if (ev.EventType_ == VrTutorialGen.Event.EventType.BOOLEAN_ACTION)
          unclassified_bool++;
        else if (ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
          unclassified_col++;
        else if (ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
          unclassified_trigg++;
        else if (ev.EventType_ == VrTutorialGen.Event.EventType.POSE_ACTION)
          unclassified_pose++;
      }
    }
    Debug.Log("[Progress]: " + relevantEvents.Count + " / " + (unclassifiedEvents.Count + relevantEvents.Count) + " (bool: " + unclassified_bool + " pose: " + unclassified_pose + " collison: " + unclassified_col + " trigger: " + unclassified_trigg + ")");
  }
  private static void PrintRelevantTutorialTexts(List<VrTutorialGen.Event> relevantEvents)
  {
    String tutorialText = getRelevantTutorialTexts(relevantEvents);
    Debug.Log("[Result]:\n" + tutorialText);
  }
  private static string getRelevantTutorialTexts(List<VrTutorialGen.Event> relevantEvents)
  {
    String tutorialText = "";
    foreach (var ev in relevantEvents)
    {
      if (ev.Releavant && Instance.eventTypesToCheckRelevancy.Contains(ev.EventType_))
      {
        tutorialText += ev.GetAutomatedTutorialText() + " \n";
      }
    }
    return tutorialText;
  }
  private static string removeFirstNonRelevantEventFromManipulated()
  {
    List<VrTutorialGen.Event> eventsCopy = new List<VrTutorialGen.Event>(manipulatedGhostGameEvents);
    VrTutorialGen.CollisionRecord collisionEvent = null;
    foreach (var ev in eventsCopy)
    {
      if (ev.shouldBeIgnoredThisIteration && (ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION || ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER))
      {
        if(ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
          collisionEvent = (VrTutorialGen.CollisionRecord)ev;
        //Collision records get removed one iteration later, hence we remove it here but not break the loop (with return) as we still need to find the next event to remove
        manipulatedGhostGameEvents.Remove(ev);
        continue;
      }
      if(collisionEvent != null && ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
      {
        //Set similar collisions all at once as the outcome is always the same no matter when the collision occurs (currently collision ignorance is for a whole iteration)
        VrTutorialGen.CollisionRecord convertedEv = (VrTutorialGen.CollisionRecord)ev;
        if(collisionEvent.Self_name == convertedEv.Self_name && collisionEvent.Other_name == convertedEv.Other_name)
        {
          ev.shouldBeIgnoredThisIteration = true;
        }
      }
    }
      foreach (var ev in eventsCopy)
    {
      if (ev.shouldBeIgnoredThisIteration && (ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION || ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER))
      {
        //Collision records get removed one iteration later, hence we remove it here but not break the loop (with return) as we still need to find the next event to remove
        manipulatedGhostGameEvents.Remove(ev);
        continue;
      }
      if (!ev.Releavant && Instance.eventTypesToCheckRelevancy.Contains(ev.EventType_))
      {
        if (ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION || ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
        {
          ev.shouldBeIgnoredThisIteration = true;
        }
        else
        {
          manipulatedGhostGameEvents.Remove(ev);
        }
        return ev.GetAutomatedText();
      }
    }
    return "No unrelevant events left. " + eventsCopy.Count;
  }
  private static bool AllEventsRelevant(List<VrTutorialGen.Event> events)
  {
    foreach (var ev in events)
    {
      if (!ev.Releavant && Instance.eventTypesToCheckRelevancy.Contains(ev.EventType_))
      {
        if (!ev.ActionStarts && ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
          continue;
        if (!ev.ActionStarts && ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
          continue;

        return false;
      }
    }
    return true;
  }
  private static string setFirstNonRelevantEventRelevant(List<VrTutorialGen.Event> events)
  {
    foreach (var ev in events)
    {
      if (!ev.Releavant && Instance.eventTypesToCheckRelevancy.Contains(ev.EventType_))
      {
        if (!ev.ActionStarts && ev.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
        {
          string selfName = ((VrTutorialGen.CollisionRecord)ev).Self_name;
          string otherName = VrTutorialGen.Event.GetAlias(((VrTutorialGen.CollisionRecord)ev).Other_name);
          ev.shouldBeIgnoredThisIteration = false;
          ev.Releavant = true;
          foreach (var ev2 in events)
          {
            if (ev2.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
            {
              string selfName2 = ((VrTutorialGen.CollisionRecord)ev2).Self_name;
              string otherName2 = VrTutorialGen.Event.GetAlias(((VrTutorialGen.CollisionRecord)ev2).Other_name);
              if (selfName2 == selfName && otherName2 == otherName)
              {

                Debug.Log("[COLL] collision-ends-event between" + selfName + "and" + otherName + " gets automatically set to irrelevant (no need to test this as it has no effect with current implementation)");
                ev2.shouldBeIgnoredThisIteration = false;
                ev2.Releavant = true;
              }
            }
          }
          return ev.GetAutomatedText();
        }
        if (!ev.ActionStarts && ev.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
        {
          string selfName = ((VrTutorialGen.TriggerRecord)ev).Self_name;
          string otherName = VrTutorialGen.Event.GetAlias(((VrTutorialGen.TriggerRecord)ev).Other_name);
          Debug.Log("[TRIG] collision-ends-event between" + selfName + "and" + otherName + " gets automatically set to irrelevant (no need to test this as it has no effect with current implementation)");
          ev.shouldBeIgnoredThisIteration = false;
          ev.Releavant = true;
          foreach (var ev2 in events)
          {
            if (ev2.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
            {
              string selfName2 = ((VrTutorialGen.TriggerRecord)ev2).Self_name;
              string otherName2 = VrTutorialGen.Event.GetAlias(((VrTutorialGen.CollisionRecord)ev2).Other_name);
              if (selfName2 == selfName && otherName2 == otherName)
              {

                Debug.Log("[COLL] collision-ends-event between" + selfName + "and" + otherName + " gets automatically set to irrelevant (no need to test this as it has no effect with current implementation)");
                ev2.shouldBeIgnoredThisIteration = false;
                ev2.Releavant = true;
              }
            }
            return ev.GetAutomatedText();
          }
        }
        ev.shouldBeIgnoredThisIteration = false;
        ev.Releavant = true;
        return ev.GetAutomatedText();
      }
    }
    return "No events left??? " + events.Count;
  }
  public static void GhostGameEndReached()
  {
    if(!Instance.GameHasEnded)
    {
      Instance.StartCoroutine(TimeOutIteration(iteration));
    }
  }
  private static IEnumerator TimeOutIteration(int iterationWhenCalled)
  {
    yield return new WaitForSeconds(2f);
    if (IsAnimationRecording)
    {
      StopAnimationRecording();
    }
    if (IsVideoRecording)
    {
      StopVideoRecording();
    }
    if (iteration == iterationWhenCalled && !Instance.loadLoadNextIterationAlreadyCalled)
    {
      string text = setFirstNonRelevantEventRelevant(previousManipulatedGhostGameEvents);
      Debug.LogError("[loadScene][Iteration]["+ iteration + "] critical event seems to be removed " + Instance.lastText + " =set " + text + " relevant");
      manipulatedGhostGameEvents = new List<VrTutorialGen.Event>(previousManipulatedGhostGameEvents);
      Instance.lastText = removeFirstNonRelevantEventFromManipulated();
      LoadNextIteration();
    }
  }

  private IEnumerator startGhostAnimationRecording()
  {
    yield return new WaitForSeconds((float)(STARTUP_TIME_THRESHOLD));
    InitialTimeThresholdPassed = true;
    StartAnimationRecording();
    ActionsPerformed.Add(new VrTutorialGen.GameEndRecord(true, false));
  }

  public void AddAnimationSyncRecordAction(VrTutorialGen.AnimationSyncRecord record)
  {
    if (Instance.GameHasEnded)
      return;
    //Debug.Log("[ANIM] " + record.PerformedAt + ": " + record.AnimationTime + "animation time,  "+ " time: " + Time.unscaledTime);
    ActionsPerformed.Add(record);
  }

  public void AddPoseRecordAction(VrTutorialGen.PoseActionRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
   // Debug.Log("[POSE] " +record.PerformedAt + ": " + record.Input_Source + "  " + record.Action_Pose.GetShortName() + " " + record.ActionStarts + " time: " + Time.unscaledTime);
    ActionsPerformed.Add(record);
  }
  public void AddBooleanRecordAction(VrTutorialGen.BooleanActionRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
    //  Debug.Log("[BOOL] " + record.PerformedAt + ": " + record.Input_Source + "  " + record.Action_Boolean.GetShortName() + " " + record.ActionStarts + " time: "+Time.unscaledTime);
    ActionsPerformed.Add(record);
  }
  public void AddVector1RecordAction(VrTutorialGen.Vector1ActionRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
    //  Debug.Log("[VEC1] " + record.PerformedAt + ": " + record.Input_Source + "  " + record.Action_Vector1.GetShortName() + " Axis:" + record.Action_Axis + " Delta:" + record.Action_Vector1 + " Delta:" + record.Action_Delta);
    ActionsPerformed.Add(record);
  }
  public void AddVector2RecordAction(VrTutorialGen.Vector2ActionRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
    // Debug.Log("[VEC2] " + record.PerformedAt + ": " + record.Input_Source + "  " + record.Action_Vector2.GetShortName() + " Axis:" + record.Action_Axis + " Delta:" + record.Action_Vector2 + " Delta:" + record.Action_Delta);
    ActionsPerformed.Add(record);
  }
  public void AddVector3RecordAction(VrTutorialGen.Vector3ActionRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
    //   Debug.Log("[VEC3] " + record.PerformedAt + ": " + record.Input_Source + "  " + record.Action_Vector3.GetShortName() + " Axis:" + record.Action_Axis + " Delta:" + record.Action_Vector3 + " Delta:" + record.Action_Delta);
    ActionsPerformed.Add(record);
  }
  public void AddRecordAction(VrTutorialGen.TriggerRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
    //  Debug.Log("[TRIG] " + record.PerformedAt + ": " + record.Self.name + " triggered " + record.Other.gameObject.name + " " + record.ActionStarts);
    ActionsPerformed.Add(record);
  }
  public void AddRecordAction(VrTutorialGen.CollisionRecord record)
  {
    if (!InitialTimeThresholdPassed || Instance.GameHasEnded)
      return;
    //  Debug.Log("[COLL] " +record.PerformedAt + ": " + record.Self.name + " collided with " + record.Col.gameObject.name + " " + record.ActionStarts);
    ActionsPerformed.Add(record);
  }
  bool GameHasEnded = false;
  public bool GameWasWon { get; private set; } = false;
  public static float firstIterationDuration = 0;
  public static void Win()
  {
    Debug.Log("[WIN] " + Time.unscaledTime);
    if (!Instance.IsGhostGame)
    {
      if (!Instance.GameHasEnded)
        Instance.ActionsPerformed.Add(new VrTutorialGen.GameEndRecord(true));
      Instance.GameHasEnded = true;
      Instance.GameWasWon = true;
      if (IsAnimationRecording)
      {
        AnimationClip ghostAnimation = StopAnimationRecording();
       // Instance.ActionsPerformed = EverGreenEvent.TickEvergreenEvents(Instance.ActionsPerformed);
        Debug.Log("actioncount:" + Instance.ActionsPerformed.Count);
        Instance.tabletManger.ShowTabletManager(Instance.ActionsPerformed, "file://" + getGhostFileNames(FileType.NONE, true), ghostAnimation);
      }
    }
    else
    {
      if (IsAnimationRecording)
      {
        StopAnimationRecording();
      }
      if (IsVideoRecording)
      {
        StopVideoRecording();
      }
      if (!TutorialEventManager.firstIterationIsOver)
      {
        TutorialEventManager.firstIterationIsOver = true;
        TutorialEventManager.firstIterationDuration = Time.unscaledTime;

      }
    }
    Instance.infoHandler.enabled = true;
    Instance.infoHandler.RefreshText(Instance.ActionsPerformed.Count);
    if (Instance.FindRelevantEvents && Instance.IsGhostGame && !Instance.loadLoadNextIterationAlreadyCalled)
    {
      previousManipulatedGhostGameEvents = new List<VrTutorialGen.Event>(manipulatedGhostGameEvents);
      Instance.lastText = removeFirstNonRelevantEventFromManipulated();
      Debug.Log("[loadScene][Iteration] useless event got detected, trying to remove next  {" + Instance.lastText + "}");
      LoadNextIteration();
    }
  }
  string lastText = "";
  public bool loadNewScenesMode = false;
  private bool loadLoadNextIterationAlreadyCalled = false;
  private static void LoadNextIteration()
  {
    if (Instance.loadNewScenesMode)
    {
      SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }
    else
      Instance.loadLoadNextIterationAlreadyCalled = true;

  }
  public void Lose()
  {
    Debug.Log("[LOSE] " + Time.unscaledTime + " actioncount: " + ActionsPerformed.Count);
    if (!GameHasEnded)
      ActionsPerformed.Add(new VrTutorialGen.GameEndRecord(false));
    GameHasEnded = true;
    Instance.GameWasWon = false;
  }
  static List<VrTutorialGen.Event> originalGhostGameEvents;
  static List<VrTutorialGen.Event> manipulatedGhostGameEvents;
  static List<VrTutorialGen.Event> previousManipulatedGhostGameEvents;
  public static String ghostClipFileName = "";
  private void LoadGhostDataFromDB()
  {
    //EverGreenEvent.PopulateEvergreenEvents();
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "SELECT id, binary, ghostClipFileName FROM games  where win=1 order by id asc"; 
    IDataReader reader = cmd.ExecuteReader();
    originalGhostGameEvents = new List<VrTutorialGen.Event>();
    int ghostIdUsed = -1;
    while (reader.Read())
    {
      int id = -1;
      try
      {
        id = reader.GetInt32(0);
        byte[] bin = (byte[])reader.GetValue(1);
        MemoryStream s = new MemoryStream(bin);
        BinaryFormatter binary = new BinaryFormatter();
        binary.Binder = new SerializationBinderHelper();
        List<VrTutorialGen.Event> previousGame = (List<VrTutorialGen.Event>)binary.Deserialize(s);
        PreviousGames.Add(id, previousGame);
        if (!reader.IsDBNull(2)) //look for the game with the specified ghostId
        {
          if (ghostGameId < 0 || id == ghostGameId)
          {
            ghostClipFileName = reader.GetValue(2).ToString();
            originalGhostGameEvents = previousGame;
            ghostIdUsed = id; //in case ghostId = -1 we need to know the real id of the latest game to later store information accordingly
          }
        }
      }
      catch (Exception ex)
      {
        Debug.LogWarning("Error when reading old ghost data id "+ id + ":" + ex);
      }
    }
    ghostGameId = ghostIdUsed; 
    reader.Close();
    reader = null;
    cmd.Dispose();
    cmd = null;
    con.Close();
    con = null;


    if (IsGhostGame)
    {
      if (usePreviousProgress)
        LoadEssentialEventDataFromDB(ghostGameId);
      manipulatedGhostGameEvents = new List<VrTutorialGen.Event>(originalGhostGameEvents);
      Ghost.SetGhostEvents(originalGhostGameEvents);
      Ghost.SetGhostFile(ghostClipFileName);
      previousManipulatedGhostGameEvents = new List<VrTutorialGen.Event>(originalGhostGameEvents); 
    }
  }

  private void LoadEssentialEventDataFromDB(int gameId)
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    if (previousEssentialEventsIdToContinueFrom != -1) //Id was set
    {
      cmd.CommandText = "SELECT id, events, iteration FROM essentialEvents where gameId=@gameId and id=@id;";
      SqliteParameter idParam = new SqliteParameter("@id", previousEssentialEventsIdToContinueFrom);
      cmd.Parameters.Add(idParam);
    }
     else if (previousIterationIdToContinueFrom != -1) //iteration was set
    {
      cmd.CommandText = "SELECT id, events, iteration FROM essentialEvents where gameId=@gameId and iteration=@iteration;";
      SqliteParameter iterationParam = new SqliteParameter("@iteration", previousIterationIdToContinueFrom);
      cmd.Parameters.Add(iterationParam);
    }
    else //nothing was set
    {
      cmd.CommandText = "SELECT id, events, iteration FROM essentialEvents where gameId=@gameId;";
    }
    SqliteParameter gameIdParam = new SqliteParameter("@gameId", gameId);
    cmd.Parameters.Add(gameIdParam);
    IDataReader reader = cmd.ExecuteReader();

    if (reader.RecordsAffected > 0)
      originalGhostGameEvents = new List<VrTutorialGen.Event>();
    while (reader.Read())
    {
      int id = reader.GetInt32(0);
      byte[] bin = (byte[])reader.GetValue(1);
      int iter = reader.GetInt32(2);
      MemoryStream s = new MemoryStream(bin);
      BinaryFormatter binary = new BinaryFormatter();
      binary.Binder = new SerializationBinderHelper();
      List<VrTutorialGen.Event> previousGame = (List<VrTutorialGen.Event>)binary.Deserialize(s);
      if (!reader.IsDBNull(2)) //look for the game with the specified ghostId
      {
        if (previousIterationIdToContinueFrom < 0 || iter == previousIterationIdToContinueFrom)
        {
          originalGhostGameEvents = previousGame;
        }
      }
    }
    reader.Close();
    reader = null;
    cmd.Dispose();
    cmd = null;
    con.Close();
    con = null;
  }
  private static bool IsAnimationRecording = false;
  public static bool geIstVideoRecording()
  {
    return IsVideoRecording;
  }
  private static bool IsVideoRecording = false;
  public static void PrepareVideoFilesBeforeRecording(string file = null)
  {
    if (Instance.GetComponent<VideoRecordingManager>().doNotRecord)
      return;
    if (file == null)
      file = getGhostFileNames(FileType.NONE, true);
    if (File.Exists(file + ".mp4"))
    {
      Debug.LogWarning(file + ".mp4 already exists. Its getting deleted now.");
      File.Delete(file + ".mp4");
    }
  }
  public static void StartVideoRecording(string file = null)
  {
    if (Instance.GetComponent<VideoRecordingManager>().doNotRecord)
      return;
    if (file == null)
      file = getGhostFileNames(FileType.VIDEO, true);
    Instance.GetComponent<VideoRecordingManager>().PrepareRecording(file);
    Instance.GetComponent<VideoRecordingManager>().StartRecording();
    Debug.Log("Started video recording to file: " + file);
    IsVideoRecording = true;
  }
  public static void StopVideoRecording()
  {
    Instance.GetComponent<VideoRecordingManager>().StopRecording();
    Debug.Log("Stopped video recording" + Instance.getVideoDependedTime());
    IsVideoRecording = false;
    Instance.GetComponent<VideoRecordingManager>().doNotRecord = true;
  }
  public static void StartAnimationRecording(string file = null)
  {
    if (Instance.doNotRecord)
      return;
    Instance.Player.GetComponent<AnimationRecorder>().enabled = true;
    Instance.Player.GetComponent<AnimationRecorder>().StartAnimationRecording();
    Debug.Log("Started animation recording.");
    IsAnimationRecording = true;
  }

  public static AnimationClip StopAnimationRecording()
  {
    AnimationRecorder animationRecorder = Instance.Player.GetComponent<AnimationRecorder>();
    animationRecorder.enabled = false;
    animationRecorder.StoreClip();
    Debug.Log("Stopped animation recording");
    IsAnimationRecording = false;
    return animationRecorder.clip;
  }
}

