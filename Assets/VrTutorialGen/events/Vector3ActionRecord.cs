﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class Vector3ActionRecord : Event
  {
    public readonly Vector3 Action_Axis;
    public readonly Vector3 Action_Delta;
    public Vector3ActionRecord(SteamVR_Action_Vector3 action, SteamVR_Input_Sources fromSource, Vector3 axis, Vector3 delta)
    {
      Input_Source = fromSource;
      Action_Vector3 = action;
      ActionStarts = true;
      Action_Axis = axis;
      Action_Delta = delta;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.VECTOR2_ACTION;
    }
    public readonly SteamVR_Action_Vector3 Action_Vector3;
    public readonly SteamVR_Input_Sources Input_Source = SteamVR_Input_Sources.Waist;
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(Action_Vector3.GetPath());
      else
        return GetAlias(Action_Vector3.GetPath()) + " ends";
    }
    public string GetName()
    {
      return Action_Vector3.GetPath();
    }
  }
}