﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using VrTutorialGen;
public class EverGreenEvent
{
  private static List<EverGreenEvent> everGreenEvents = null;

  //Returns all events that allways happen
  public static void PopulateEvergreenEvents()
  {
    everGreenEvents = new List<EverGreenEvent>();
    IDbConnection con = (IDbConnection)new SqliteConnection(DatabaseHandler.getInstance().getConstring());
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "SELECT *, count(id) as occurences from (SELECT * from events where gameId in (" +
                      "Select gameId from events where eventType = 3) group by gameId, eventType, objectId, object2Id, starting) " +
                      "group by eventType, objectId, object2Id, starting order by occurences DESC; ";

    using (var reader = cmd.ExecuteReader())
    {
      int maxOccurences = 0;
      while (reader.Read())
      {
        int occurences = reader.GetInt32(reader.GetOrdinal("occurences"));
        maxOccurences = System.Math.Max(occurences, maxOccurences);
        //if (occurences != maxOccurences)
          //break;


        int starting = reader.GetInt32(reader.GetOrdinal("starting"));
        int eventType = reader.GetInt32(reader.GetOrdinal("eventType"));

        int? object1 = null;
        if (!reader.IsDBNull(reader.GetOrdinal("objectId")))
          object1 = reader.GetInt32(reader.GetOrdinal("objectId"));
        int? object2 = null;
        if (!reader.IsDBNull(reader.GetOrdinal("object2Id")))
          object2 = reader.GetInt32(reader.GetOrdinal("object2Id"));


        everGreenEvents.Add(new EverGreenEvent(object1, object2, starting, eventType));
      }
    }
    //  Debug.Log("GetObjectId " + name + ":" + id);
    cmd.Dispose();
    con.Close();
    con = null;
  }

  int? obj1_ = null;
  int? obj2_ = null;
  int starting_;
  int eventType_;
  EverGreenEvent(int? obj1, int? obj2, int starting, int eventType)
  {
    if(eventType == (int)VrTutorialGen.Event.EventType.COLLISION && (obj1 == null || obj2 == null))
    {
      Debug.LogError("Evergreen event of type collision has obj1 or obj2 null");
    }
    obj1_ = obj1;
    obj2_ = obj2;
    starting_ = starting;
    eventType_ = eventType;
  }

  public static bool IsEventEvergreen(VrTutorialGen.Event action)
  {
    return IsEventEvergreen(new EverGreenEvent(action));
  }
  public static bool IsEventEvergreen(EverGreenEvent action)
  {
    if(everGreenEvents == null)
      EverGreenEvent.PopulateEvergreenEvents();
    return everGreenEvents.Contains(action);
  }

  VrTutorialGen.Event ev = new VrTutorialGen.Event();
  public override bool Equals(object obj)
  {
    return Equals(obj as EverGreenEvent);
  }
  public bool Equals(EverGreenEvent other)
  {
    if (other == null) return false;

    if ((this.obj1_.Equals(other.obj1_))
      && (this.obj2_.Equals(other.obj2_))
      && (this.starting_.Equals(other.starting_))
      && (this.eventType_.Equals(other.eventType_))
      && (this.ev.GetAutomatedText().Equals(other.ev.GetAutomatedText())))
      {
      return true;
    }
    return false;
  }

  public override int GetHashCode()
  {
    return new { A = obj1_, B = obj2_, C = starting_, D = eventType_ }.GetHashCode();
  }
  EverGreenEvent(VrTutorialGen.Event ev)
  {
    int? obj1 = null;
    int? obj2 = null;
    int eventType = (int)ev.EventType_;
    switch (ev.EventType_)
    {
      case VrTutorialGen.Event.EventType.BOOLEAN_ACTION:
        BooleanActionRecord actionRecord = (BooleanActionRecord)ev;
        obj1 = DatabaseHandler.GetObjectId(actionRecord.GetName());
        break;
      case VrTutorialGen.Event.EventType.COLLISION:
        CollisionRecord collisionRecord = (CollisionRecord)ev;
        obj1 = DatabaseHandler.GetObjectId(collisionRecord.Self_name);
        obj2 = DatabaseHandler.GetObjectId(collisionRecord.Other_name);
        break;
      case VrTutorialGen.Event.EventType.TRIGGER:
        TriggerRecord triggerRecord = (TriggerRecord)ev;
        obj1 = DatabaseHandler.GetObjectId(triggerRecord.Self_name);
        obj2 = DatabaseHandler.GetObjectId(triggerRecord.Other_name);
        break;
      case VrTutorialGen.Event.EventType.END:
        break;
      default:
        break;
    }
    int starting = ev.ActionStarts == false ? 0 : 1;
    obj1_ = obj1;
    obj2_ = obj2;
    starting_ = starting;
    eventType_ = eventType;
  }

  public static List<VrTutorialGen.Event> TickEvergreenEvents(List<VrTutorialGen.Event> input)
  {
    List<VrTutorialGen.Event> result = new List<VrTutorialGen.Event>();
    foreach (VrTutorialGen.Event ev in input)
    {
      ev.Releavant = EverGreenEvent.IsEventEvergreen(ev);
      result.Add(ev);
    }
    return result;
  }
}
