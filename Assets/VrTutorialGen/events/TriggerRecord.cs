﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace VrTutorialGen
{
  [System.Serializable]
  public class TriggerRecord : Event
  {
    [field: System.NonSerializedAttribute()]
    public GameObject Self { get; protected set; }
    [field: System.NonSerializedAttribute()]
    public Collider Other { get; protected set; }
    public string Other_name;
    public string Self_name;
    public TriggerRecord(GameObject self, Collider other, bool enter)
    {
      Self = self;
      Other = other;
      Other_name = other.gameObject.name;
      Self_name = self.name;
      ActionStarts = enter;
      EventType_ = EventType.TRIGGER;
      PerformedAt = Time.unscaledTime;
    }
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(GetAlias(Self_name) + " triggers " + GetAlias(Other_name));
      else
        return GetAlias(GetAlias(Self_name) + " trigger " + GetAlias(Other_name) + " ends");
    }

    public override string GetAutomatedTutorialText()
    {
      if (ActionStarts)
        return GetAlias(GetAlias(Self_name) + " has to enter trigger area " + GetAlias(Other_name));
      else
        return GetAlias(GetAlias(Self_name) + " hast to leave trigger area " + GetAlias(Other_name));
    }
  }
}