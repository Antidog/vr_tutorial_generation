﻿using Valve.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class Vector1ActionRecord : Event
  {
    public readonly float Action_Axis;
    public readonly float Action_Delta;
    public Vector1ActionRecord(SteamVR_Action_Single action, SteamVR_Input_Sources fromSource, float axis, float delta)
    {
      Input_Source = fromSource;
      Action_Vector1 = action;
      ActionStarts = true;
      Action_Axis = axis;
      Action_Delta = delta;
      PerformedAt = Time.unscaledTime;
      EventType_ = EventType.VECTOR1_ACTION;
    }
    public readonly SteamVR_Action_Single Action_Vector1;
    public readonly SteamVR_Input_Sources Input_Source = SteamVR_Input_Sources.Waist;
    public override string GetAutomatedText()
    {
      if (ActionStarts)
        return GetAlias(Action_Vector1.GetPath());
      else
        return GetAlias(Action_Vector1.GetPath()) + " ends";
    }
    public string GetName()
    {
      return Action_Vector1.GetPath();
    }
  }
}