﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace VrTutorialGen
{
  [System.Serializable]
  public class Event
  {
    public bool Releavant = false;
    public float PerformedAt { get; protected set; }
    public float VideoTime = -1;
    public bool ActionStarts { get; protected set; }
    public bool shouldBeIgnoredThisIteration = false;
    public enum EventType
    {
      COLLISION,
      TRIGGER,
      BOOLEAN_ACTION,
      END,
      VECTOR1_ACTION, 
      VECTOR2_ACTION,
      VECTOR3_ACTION,
      POSE_ACTION ,
      ANIMATION_SYNC //not yet implemented
    }
    public EventType EventType_ { get; protected set; }
    public virtual string GetAutomatedText() { return "Text not available"; }
    public virtual string GetAutomatedTutorialText() { return "Text not available"; }
    public virtual string GetHeadlineText() { return "Text not available"; }
    public virtual string GetSublineText() { return "Text not available"; }

    public static string GetAlias(string orig)
    {
      if (aliasNames.ContainsKey(orig))
        return aliasNames[orig];
      else return orig;
    }
    public static Dictionary<string, string> aliasNames = new Dictionary<string, string> {
            { "/actions/default/in/GrabPinch", "GrabPinch" },
            { "/actions/default/in/GrabGrip", "GrabGrip" },
            { "/actions/default/in/InteractUI", "InteractUI" },
            { "VrTutorialGen_HandColliderLeft(Clone)", "LeftHand" },
            { "VrTutorialGen_HandColliderRight(Clone)", "RightHand" },
            { "RHand", "RightHand" },
            { "Button_Moveable has to collide with LeftHand", "The button has to be pressed." },
            { "Button_Moveable has to collide with RightHand", "The button has to be pressed." },
            { "Button_Moveable (1)", "Button" },
            { "PostBox (1)", "PostBox" },
            { "PostBox (2)", "PostBox" },
            { "Book.004 (6)", "Pink Book" },
            { "Book.002 (2)", "Gray Book" },
            { "Sphere.011 (1)", "Teddy" },
            { "", "Object" },
            { "LHand", "LeftHand" }
        };
    //TODO: make dictionary static to save mem
  }
}