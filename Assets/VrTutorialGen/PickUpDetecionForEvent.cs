using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class PickUpDetecionForEvent : MonoBehaviour
{
  public Hand hand;
  public GameObject gameObjectThatShouldBeAttached;
  public GhostTutorialScript parent;
  
  void Update()
  {
    if (parent.PlayerPerformedAction)
    {
      Destroy(this);
    }
    else if (hand && hand.currentAttachedObject != null)
    {
      if (hand.currentAttachedObject == gameObjectThatShouldBeAttached)
      { 
        parent.PlayerPerformedAction = true;
        Destroy(this);
      }
    }
  }
}
