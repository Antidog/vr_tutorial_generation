﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Video;

public class ListScript : MonoBehaviour
{
  const float SECONDS_BEFORE_EVENT_SHOWN = 0.5f;
  public List<ListItemScript> attachedItems;
  public List<VrTutorialGen.Event> eventsToDisplay;
  public List<VrTutorialGen.Event> eventsNotToDisplay;
  [HideInInspector]
  public AnimationClip ghostAnimation;
  public VideoPlayer videoPlayer;
  public VideoHandlerScript videoHandlerScript;
  public int offset = 0;
  private ListItemScript listItemSelected;
  public TextMeshPro DescritpionCurrent;
  public void SelectListItem(ListItemScript selected)
  {
    if (listItemSelected != null)
    { //set old item to non selected
      foreach (var item in attachedItems)
      {
        if (listItemSelected == item)
        {
          item.SetSelectedColor(false);
        }
      }
    }
    listItemSelected = selected;
    float startTime = selected.GetStartTimeOfEvent();
    videoHandlerScript.startTime = startTime - SECONDS_BEFORE_EVENT_SHOWN;
    videoHandlerScript.endTime = startTime - SECONDS_BEFORE_EVENT_SHOWN + videoHandlerScript.DEFAULT_DURATION;
    videoPlayer.Play();
  }
  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    //if (offset > attachedItems.Count)
    //  offset = attachedItems.Count;
    if (offset < 0)
      offset = 0;
    int index = offset;
    foreach (var item in attachedItems)
    {
      if (index <= eventsToDisplay.Count-1)
      {
        item.ExchangeEvent(ref eventsToDisplay, index);
      }
      else
      {
        item.text.text = "";
        if(item.checkbox != null)
          item.checkbox.CheckboxChecked = false;
      }
      index++;
    }
    foreach (var item in attachedItems)
    {
      if (listItemSelected == item)
      {
        item.SetSelectedColor(true);
        DescritpionCurrent.text = item.tutorialText;
      }
      else
      {
        item.SetSelectedColor(false);
      }
    }
  }
  bool AlreadySaved = false;
  public void SaveList()
  {

    TutorialEventManager tutorialEventManager = GetComponentInParent<TutorialEventManager>();

  List<VrTutorialGen.Event> allEvents = new List<VrTutorialGen.Event>();
    foreach (var ev in eventsNotToDisplay)
      allEvents.Add(ev);
    foreach (var ev in eventsToDisplay)
      allEvents.Add(ev);
    tutorialEventManager.databaseHandler.AsyncSaveGameInfo(tutorialEventManager.GameWasWon, allEvents, AlreadySaved, ghostAnimation);
    AlreadySaved = true;
    //throw new System.Exception("Not Implemented");
  }
}
