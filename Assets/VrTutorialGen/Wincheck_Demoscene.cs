﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wincheck_Demoscene : MonoBehaviour
{
  public PlattformScript greenPlattform;
  public PlattformScript redPlattform;

  public bool won = false;
  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    if(!won && redPlattform.isCollidingWithCorrectObject && greenPlattform.isCollidingWithCorrectObject)
    {
      TutorialEventManager.Win();
      won = true;
    }
  }
}
