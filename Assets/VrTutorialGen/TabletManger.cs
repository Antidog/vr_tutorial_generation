﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TabletManger : MonoBehaviour
{
  public VideoPlayer videoPlayer;
  public ListScript listScript;
  public void ShowTabletManager(List<VrTutorialGen.Event> actionsPerformed, string videoUrl, AnimationClip ghostAnimation, bool ignoreVideo = false)
  {
    List<VrTutorialGen.Event> actionsPerformedWithoutAnimationSync = new List<VrTutorialGen.Event>();
    List<VrTutorialGen.Event> actionsPerformedOnlyAnimationSync = new List<VrTutorialGen.Event>();
    foreach (var ev in actionsPerformed)
    {
      if (ev.EventType_ != VrTutorialGen.Event.EventType.ANIMATION_SYNC)
        actionsPerformedWithoutAnimationSync.Add(ev);
      else
        actionsPerformedOnlyAnimationSync.Add(ev);

    }
    listScript.eventsToDisplay = actionsPerformedWithoutAnimationSync;
    listScript.eventsNotToDisplay = actionsPerformedOnlyAnimationSync;
    listScript.ghostAnimation = ghostAnimation;
    if (!ignoreVideo)
      videoPlayer.url = videoUrl;
    else
      videoPlayer.Stop();
    this.gameObject.SetActive(true);
    listScript.SaveList();
  }
}
