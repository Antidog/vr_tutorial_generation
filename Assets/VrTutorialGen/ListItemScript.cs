﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ListItemScript : MonoBehaviour
{
  public Material selected;
  private Material notSelected;
  public TextMeshPro text;
  public string tutorialText;
  public Renderer Cube;
  public CheckboxToggleScript checkbox;
  private VrTutorialGen.Event AttachedEvent;
  public int ListIndex { private set; get; } = -1; 
  private void Start()
  {
    if(AttachedEvent != null)
      checkbox.CheckboxChecked = AttachedEvent.Releavant;
    notSelected = Cube.material;
  }
  private void Update()
  {
    if (AttachedEvent != null)
      checkbox.CheckboxChecked = AttachedEvent.Releavant;
    else
      checkbox.CheckboxChecked = false;
  }
  public VrTutorialGen.Event ExchangeEvent(ref List<VrTutorialGen.Event> eventList, int newIndex)
  {
    
    VrTutorialGen.Event oldEvent = AttachedEvent;
    if(oldEvent != null)
    {
      oldEvent.Releavant = checkbox.CheckboxChecked;
      eventList[ListIndex] = oldEvent;
    }
    //oldEvent.SetText(text.text); TODO: custom editible text
    text.text = eventList[newIndex].GetAutomatedText();
    tutorialText = eventList[newIndex].GetAutomatedTutorialText();
    checkbox.CheckboxChecked = eventList[newIndex].Releavant;
    AttachedEvent = eventList[newIndex];
    ListIndex = newIndex;
    return oldEvent;
  }
  public float GetStartTimeOfEvent()
  {
    return AttachedEvent.PerformedAt;
  }
  public void SetSelectedColor(bool isSelected)
  {
    if (isSelected)
      Cube.material = selected;
    else
      Cube.material = notSelected;

  }
}
