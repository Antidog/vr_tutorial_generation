﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CheckboxToggleScript : MonoBehaviour
{
  public TMPro.TextMeshPro checkboxText;

  ListScript listScript;
  ListItemScript listItemScript; //inParent
  private void OnCollisionEnter(Collision collision)
  {
    //only collisions with hands/fingers matter
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>() && !waitForExit)
    {
      waitForExit = true;
      listScript.SelectListItem(listItemScript);
      CheckboxChecked = !CheckboxChecked;
    }
  }
  private void OnCollisionExit(Collision collision)
  {
    if (collision.rigidbody.gameObject.GetComponentInParent<HandCollider>())
    {
      waitForExit = false;
    }
  }
  public bool CheckboxChecked = false;
  // Start is called before the first frame update
  void Start()
  {
    listItemScript = GetComponentInParent<ListItemScript>();
    listScript = GetComponentInParent<ListScript>();
  }
  bool waitForExit = false;
  // Update is called once per frame
  void Update()
  {
    
    if(CheckboxChecked)
    {
      checkboxText.text = "X";
    }
    else
    {
      checkboxText.text = "";
    }
  }
}
