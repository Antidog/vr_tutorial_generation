﻿#if UNITY_EDITOR
using System;
using System.Collections;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

public class AnimationRecorder : MonoBehaviour
{
  public AnimationClip clip;
  private void OnDestroy()
  {
    StoreClip();
  }
  private GameObjectRecorder recorder;
  public bool doNotRecord = false;
  public bool alreadySavedToDisk = false;
  public void StartAnimationRecording()
  {
    try
    {
      if (doNotRecord)
        return;
      clip = new AnimationClip();
      clip.name = TutorialEventManager.getGhostFileNames(TutorialEventManager.FileType.NONE);
      clip.wrapMode = WrapMode.Loop;
      path = TutorialEventManager.getGhostFileNames(TutorialEventManager.FileType.ANIMATION, true);
      recorder = new GameObjectRecorder(gameObject);
      recorder.BindComponentsOfType<Transform>(gameObject, true);
     
    }
    catch (Exception ex)
    {
      Debug.LogError(ex);
    }
    Debug.Log("Animation Recording stared @" + Time.unscaledTime);
  }
  float duration_ = 0;
  String path;
  public void ExtendDurationByXSeconds(float X)
  {
    duration_ += X;
  }
  IEnumerator startRecordingForXSeconds(object[] parms)
  {
    string name =(string) parms[0];
    float duration =(float) parms[1];
    yield return new WaitForSeconds((float)(0.01));
    clip = new AnimationClip();
    clip.name = name;
    clip.wrapMode = WrapMode.Loop;
    path = "Assets/Resources/" + name + ".anim";
    recorder = new GameObjectRecorder(gameObject);
    recorder.BindComponentsOfType<WorldCoordinateTransformMatcher>(gameObject, true);
    foreach(Transform child in gameObject.transform)
    {
      recorder.BindComponentsOfType<Transform>(child.gameObject, true);
      recorder.BindComponentsOfType<WorldCoordinateTransformMatcher>(child.gameObject, true);
    }
    this.duration_ = duration;
  }
  public void StartAnimationRecordingForXSeconds(string name, float duration)
  {
    try
    {
      if (doNotRecord)
        return;

      object[] parms = new object[2] { name, duration};
      StartCoroutine("startRecordingForXSeconds", parms);
    }
    catch (Exception ex)
    {
      Debug.LogError(ex);
    }
    Debug.Log("Animation Recording stared for "+name+" @" + Time.unscaledTime);
  }
  private IEnumerator StopAfterXSeconds()
  {
    while(duration_ >= 0)
    { 
      yield return new WaitForSeconds((float)(0.1));
      duration_ -= 0.1f / Time.timeScale;
    }
  //  StoreClip();
  }
  public float animationSyncTime = 1000;
  private float timeSinceLastSync = 0;
  private float animationTimeSofaar = 0;
  void FixedUpdate()
  {
    if (recorder == null)
    {
      return;
    }
    if (doNotRecord)
      return;
    if (clip == null)
      return;
    recorder.TakeSnapshot(Time.fixedDeltaTime);
    timeSinceLastSync += Time.fixedDeltaTime;
    animationTimeSofaar += Time.fixedDeltaTime;
    if (timeSinceLastSync > animationSyncTime / 1000)
    {
      TutorialEventManager.Instance.AddAnimationSyncRecordAction(new VrTutorialGen.AnimationSyncRecord(animationTimeSofaar));
      timeSinceLastSync = 0;
    }
    if (TutorialEventManager.firstIterationIsOver)
    {
      this.enabled = false;
      Destroy(this);
    }
  }
  public Boolean instantSave = false;
  private void Update()
  {
    if (instantSave)
    {
      StoreClip();
      instantSave = false;
    }
  }
  void OnDisable()
  {
    if (doNotRecord)
      return;
    if (clip == null)
      return;
    StoreClip();
  }

  public void StoreClip()
  {
    if (recorder && recorder.isRecording && !alreadySavedToDisk && !TutorialEventManager.Instance.doNotRecord)
    {
      AssetDatabase.DeleteAsset(path);
      AssetDatabase.CreateAsset(clip, path);
      recorder.SaveToClip(clip);
      AssetDatabase.SaveAssets();
      Debug.Log("Animation clip stored. @" + Time.unscaledTime + " duration: " + clip.length + " " + clip.name);
      alreadySavedToDisk = true;
    }
  }
}

#endif
#if !UNITY_EDITOR
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
public class AnimationRecorder : MonoBehaviour
{
  public AnimationClip clip;
  private void OnDestroy()
  {
    StoreClip();
  }
  public bool doNotRecord = false;
  public bool alreadySavedToDisk = false;
  public void StartAnimationRecording()
  {
  }
  float duration_ = 0;
  String path;
  public void ExtendDurationByXSeconds(float X)
  {
    duration_ += X;
  }

  public void StartAnimationRecordingForXSeconds(string name, float duration)
  {
    try
    {
      if (doNotRecord)
        return;

      object[] parms = new object[2] { name, duration };
      StartCoroutine("startRecordingForXSeconds", parms);
    }
    catch (Exception ex)
    {
      Debug.LogError(ex);
    }
    Debug.Log("Animation Recording stared for " + name + " @" + Time.unscaledTime);
  }
  private IEnumerator StopAfterXSeconds()
  {
    while (duration_ >= 0)
    {
      yield return new WaitForSeconds((float)(0.1));
      duration_ -= 0.1f / Time.timeScale;
    }
  }
  public float animationSyncTime = 1000;
  private float timeSinceLastSync = 0;
  private float animationTimeSofaar = 0;
  void FixedUpdate()
  {

  }
  public Boolean instantSave = false;
  private void Update()
  {
  }
  void OnDisable()
  {
  }

  public void StoreClip()
  {

  }
}
#endif