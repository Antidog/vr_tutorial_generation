﻿using Assets.VrTutorialGen;
using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine;
using VrTutorialGen;

public class DatabaseHandler
{
  public static DatabaseHandler getInstance()
  {
    CONNECTIONSTRING = "URI=file:" + Application.streamingAssetsPath + "/VrTutorialGen/tutDb.s3db";
    if (instance_ == null)
      instance_ = new DatabaseHandler();
    return instance_;
  }
  Thread thread = null;
  private static DatabaseHandler instance_ = null;
  public void AsyncSaveGameInfo(bool win, List<VrTutorialGen.Event> actionsPerformed, bool update, AnimationClip ghostAnimation)
  {
    StorageContext context = new StorageContext(win, new List<VrTutorialGen.Event>(actionsPerformed), update, ghostAnimation);
    while (thread != null && thread.IsAlive)
    {
      Debug.Log("Error, previous thread is still saving database actions");
      Thread.Sleep(10);
    }
    thread = new Thread(new ThreadStart(context.StoreGameData));
    thread.IsBackground = true;
    thread.Start();
    
  }
  private static string CONNECTIONSTRING = null;
  public string getConstring()
  {
    return CONNECTIONSTRING;
  }
  public void SaveEssentialEvents(List<VrTutorialGen.Event> essentialEvents, int gameId, int iteration)
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
    con.Open();
    IDbCommand cmd = con.CreateCommand();

    cmd.CommandText = "Insert into essentialEvents (gameId,events,iteration) VALUES (@gameid,@events,@iteration);";
    SqliteParameter gameIdParam = new SqliteParameter("@gameId", System.Data.DbType.Int32);
    gameIdParam.Value = gameId;
    SqliteParameter iterationParam = new SqliteParameter("@iteration", System.Data.DbType.Int32);
    iterationParam.Value = iteration;
    MemoryStream s = new MemoryStream();
    BinaryFormatter binaryEvents = new BinaryFormatter();
    binaryEvents.Serialize(s, essentialEvents);
    SqliteParameter esseentialEventsParam = new SqliteParameter("@events", System.Data.DbType.Binary);
    esseentialEventsParam.Value = s.ToArray();

    cmd.Parameters.Add(gameIdParam);
    cmd.Parameters.Add(esseentialEventsParam);
    cmd.Parameters.Add(iterationParam);
    cmd.ExecuteNonQuery();
    con.Close();
  }
  public void SaveTutorialEvents(List<VrTutorialGen.Event> essentialEvents, int gameId, string videoName, float duration)
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
    con.Open();
    IDbCommand cmd = con.CreateCommand();

    cmd.CommandText = "Insert into tutorialEvents (gameId,events,videoName,duration) VALUES (@gameid,@events,@videoName,@duration);";
    SqliteParameter gameIdParam = new SqliteParameter("@gameId", System.Data.DbType.Int32);
    gameIdParam.Value = gameId;
    MemoryStream s = new MemoryStream();
    BinaryFormatter binaryEvents = new BinaryFormatter();
    binaryEvents.Serialize(s, essentialEvents);
    SqliteParameter eventsParam = new SqliteParameter("@events", System.Data.DbType.Binary);
    eventsParam.Value = s.ToArray();
    SqliteParameter videoNameParam = new SqliteParameter("@videoName", System.Data.DbType.String);
    videoNameParam.Value = videoName.Split('/')[videoName.Split('/').Length-1];
    SqliteParameter durationParam = new SqliteParameter("@duration", System.Data.DbType.Single);
    durationParam.Value = duration;
    cmd.Parameters.Add(durationParam);
    cmd.Parameters.Add(gameIdParam);
    cmd.Parameters.Add(eventsParam);
    cmd.Parameters.Add(videoNameParam);
    cmd.ExecuteNonQuery();
    con.Close();
    Debug.Log("Tutorial event id is: " + getLastTutorialEventsId() +" use it in VideoTutorial/GhostTutorial as Id of Tutorial");
  }

  public void SavePlayerStatScript(PlayerPlaythroughStats toSave)
  {
    IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "Insert into PlayerGameStats (datetime,statscript) VALUES (@datetime,@statscript);";
    SqliteParameter datetimeParam = new SqliteParameter("@datetime", System.Data.DbType.UInt64);
    MemoryStream s = new MemoryStream();
    BinaryFormatter binaryEvents = new BinaryFormatter();
    binaryEvents.Serialize(s, toSave);
    SqliteParameter statscriptParam = new SqliteParameter("@statscript", System.Data.DbType.Binary);
    statscriptParam.Value = s.ToArray();
    datetimeParam.Value = toSave.startTimeInUnixFormat;
    cmd.Parameters.Add(datetimeParam);
    cmd.Parameters.Add(statscriptParam);
    cmd.ExecuteNonQuery();
    con.Close();
    Debug.Log("PlayerStatscript saved " + toSave.gameDuration);
  }

  public int getLastTutorialEventsId()
  {
    int id = -1;
    IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
    con.Open();
    IDbCommand cmd = con.CreateCommand();

    cmd.CommandText = "Select * from tutorialEvents order by id desc limit 1;";
    var reader = cmd.ExecuteReader();
    while (reader.Read())
      id = reader.GetInt32(reader.GetOrdinal("id"));
    con.Close();
    return id;
  }

  private static Dictionary<String, int> objectIdCache = new Dictionary<string, int>();

  public static int GetObjectId(string name)
  {
    if (objectIdCache.ContainsKey(name))
    {
     // Debug.Log("GetObjectId " + name + " cache hit: " + objectIdCache[name]);
      return objectIdCache[name];
    }
    //Debug.Log("GetObjectId " + name + " cache miss");
    int id = 0;
    IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
    con.Open();
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "Select id,name from Objects where name=@name;";
    SqliteParameter nameParam = new SqliteParameter("@name", System.Data.DbType.String);
    nameParam.Value = name;
    cmd.Parameters.Add(nameParam);
    using (var reader = cmd.ExecuteReader())
    {
      if (reader.Read())
      {
        id = reader.GetInt32(0);
      }
    }
    cmd.Dispose();
    if (id == 0)
    {

      //object does not exist yet
      cmd = con.CreateCommand();
      cmd.CommandText = "Insert into objects (name,prefabTypes) VALUES (@name,0)";
      cmd.Parameters.Add(nameParam);
      cmd.ExecuteNonQuery();
      id = getInsertedId(con);
      cmd.Dispose();
      cmd = null;
    }
    con.Close();
    con = null;
    objectIdCache[name] = id;
    return id;
  }

  public static int GetPrefabId(GameObject gameObj)
  {
    //TODO: a good way to implement prefabs would be to have a seperate prefabname component which could be attached to objects
    throw new System.Exception("Not yet Implemented");
    //return 0;
  }

  public static int getInsertedId(IDbConnection con)
  {
    IDbCommand cmd = con.CreateCommand();
    cmd.CommandText = "Select last_insert_rowid()";
    using (var reader = cmd.ExecuteReader())
    {
      if (reader.Read())
      {
        cmd.Dispose();
        cmd = null;
        return reader.GetInt32(0);
      }
    }
    cmd.Dispose();
    cmd = null;
    return 0;
  }

  class StorageContext
  {
    bool update_ = false;
    bool win_;
    List<VrTutorialGen.Event> actionsPerformed_;
    AnimationClip ghostAnimation_;
    public StorageContext(bool win, List<VrTutorialGen.Event> actionsPerformed, bool update, AnimationClip ghostAnimation)
    {
      this.update_ = update;
      this.win_ = win;
      this.actionsPerformed_ = actionsPerformed;
      this.ghostAnimation_ = ghostAnimation;
    }
    public void StoreGameData()
    {
      IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
      con.Open();
      IDbCommand cmd = con.CreateCommand();
      int gameId = -1;
      if (update_)
      {
        cmd.CommandText = "Select id from games order by id desc limit 1";

        var reader = cmd.ExecuteReader();
        if(reader.Read())
        {
          gameId = reader.GetInt32(0);
        }
      }
      else
      {
//        cmd.CommandText = "Insert into games (binary,win,action_count,ghostData) VALUES (@binary,@win,@action_count,@ghostData)";
        cmd.CommandText = "Insert into games (binary,win,action_count,ghostClipFileName) VALUES (@binary,@win,@action_count,@ghostClipFileName)";

        MemoryStream s = new MemoryStream();
        BinaryFormatter binary = new BinaryFormatter();
        binary.Serialize(s, actionsPerformed_);
        SqliteParameter binaryParam = new SqliteParameter("@binary", System.Data.DbType.Binary);
        binaryParam.Value = s.ToArray();
        cmd.Parameters.Add(binaryParam);

        SqliteParameter wonParam = new SqliteParameter("@win", System.Data.DbType.Boolean);
        wonParam.Value = win_;
        cmd.Parameters.Add(wonParam);

        SqliteParameter actionCountParam = new SqliteParameter("@action_count", System.Data.DbType.Int32);
        actionCountParam.Value = actionsPerformed_.Count;
        cmd.Parameters.Add(actionCountParam);

        SqliteParameter ghostDataParam = new SqliteParameter("@ghostClipFileName", System.Data.DbType.String);
        ghostDataParam.Value = TutorialEventManager.getGhostFileNames(TutorialEventManager.FileType.ANIMATION, true);
        cmd.Parameters.Add(ghostDataParam);

        cmd.ExecuteNonQuery();
        gameId = getInsertedId(con);
      }
      //HanldeActions(gameId);
      cmd.Dispose();
      cmd = null;
      con.Close();
      con = null;
      Debug.Log("Game saved to Database.");
    }


    public void HanldeActions(int gameId)
    {
      IDbConnection con = (IDbConnection)new SqliteConnection(CONNECTIONSTRING);
      con.Open();
      SqliteParameter gameidParam = new SqliteParameter("@gameid", System.Data.DbType.Int32);
      gameidParam.Value = gameId;
      //In case of update we need to delete old entries (multiple times save button pressed for example)

      IDbCommand cmd_delete = con.CreateCommand();
      cmd_delete.CommandText = "Delete from events where gameid = @gameid;";
      cmd_delete.Parameters.Add(gameidParam);
      cmd_delete.ExecuteNonQuery();
      cmd_delete.Dispose();
      cmd_delete = null;
      //Usual insert procedure

      foreach (var action in actionsPerformed_)
      {
        IDbCommand cmd = con.CreateCommand();
        cmd.CommandText = "Insert into events (gameid,eventType,objectId,object2Id,time,starting,markedRelevantByUser) VALUES (@gameid,@eventType,@objectId,@object2Id,@time,@starting,@markedRelevantByUser)";
        SqliteParameter eventTypeParam = new SqliteParameter("@eventType", System.Data.DbType.Int32);
        eventTypeParam.Value = action.EventType_;
        SqliteParameter objectIdParam = new SqliteParameter("@objectId", System.Data.DbType.Int32);
        objectIdParam.Value = null;
        SqliteParameter object2IdParam = new SqliteParameter("@object2Id", System.Data.DbType.Int32);
        object2IdParam.Value = null;
        SqliteParameter timeParam = new SqliteParameter("@time", System.Data.DbType.Double);
        timeParam.Value = action.PerformedAt;
        SqliteParameter startingParam = new SqliteParameter("@starting", System.Data.DbType.Boolean);
        startingParam.Value = action.ActionStarts;
        SqliteParameter markedRelevantByUser = new SqliteParameter("@markedRelevantByUser", System.Data.DbType.Boolean);
        markedRelevantByUser.Value = action.Releavant;

        if (action.EventType_ == VrTutorialGen.Event.EventType.BOOLEAN_ACTION)
        {
          var convertedAction = (BooleanActionRecord)action;
          //objectIdParam.Value = GetObjectId(convertedAction.Action_Boolean.GetPath());
          objectIdParam.Value = GetObjectId(convertedAction.Action_Boolean.GetPath());
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.VECTOR1_ACTION)
        {
          var convertedAction = (Vector1ActionRecord)action;
          //objectIdParam.Value = GetObjectId(convertedAction.Action_Vector1.GetPath());
          //    Debug.LogError("Vector2 NOT YET IMPLEMENTED");
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.VECTOR2_ACTION)
        {
          var convertedAction = (Vector2ActionRecord)action;
          //objectIdParam.Value = GetObjectId(convertedAction.Action_Vector2.GetPath());
          //    Debug.LogError("Vector2 NOT YET IMPLEMENTED");
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.VECTOR3_ACTION)
        {
          var convertedAction = (Vector3ActionRecord)action;
          //objectIdParam.Value = GetObjectId(convertedAction.Action_Vector3.GetPath());
      //    Debug.LogWarning("Vector3 Actions Only partially implemented (not usefull for now)");
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.END)
        {
          var convertedAction = (GameEndRecord)action;
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.POSE_ACTION)
        {
          var convertedAction = (PoseActionRecord)action;
          cmd.Parameters.Add(objectIdParam);
          cmd.Parameters.Add(object2IdParam);
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.COLLISION)
        {
          var convertedAction = (CollisionRecord)action;
          objectIdParam.Value = GetObjectId(convertedAction.Self_name);
          object2IdParam.Value = GetObjectId(convertedAction.Other_name);
        }
        if (action.EventType_ == VrTutorialGen.Event.EventType.TRIGGER)
        {
          var convertedAction = (TriggerRecord)action;
          objectIdParam.Value = GetObjectId(convertedAction.Self_name);
          object2IdParam.Value = GetObjectId(convertedAction.Other_name);
        }
        cmd.Parameters.Add(gameidParam);
        cmd.Parameters.Add(eventTypeParam);
        cmd.Parameters.Add(objectIdParam);
        cmd.Parameters.Add(object2IdParam);
        cmd.Parameters.Add(timeParam);
        cmd.Parameters.Add(startingParam);
        cmd.Parameters.Add(markedRelevantByUser);
        if (action.EventType_ != VrTutorialGen.Event.EventType.ANIMATION_SYNC)
          cmd.ExecuteNonQuery();
        cmd.Dispose();
        cmd = null;
      }

      con.Close();
      con = null;
    }
  }
}