using Assets.VrTutorialGen;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerStatScript : MonoBehaviour
{
 
  TutorialConfigurator configurator;
  public long startTimeInUnix;
  public float gameDuration;
  public bool gameWasWon = false;
  TutorialConfigurator.ExecutionMode mode;
  // Start is called before the first frame update
  void Start()
  {
    this.configurator = gameObject.GetComponent<TutorialConfigurator>();
    DateTimeOffset dto = DateTime.Now;
    startTimeInUnix = dto.ToUnixTimeSeconds();
    mode = TutorialConfigurator.Instance.executionMode;
  }

  // Update is called once per frame
  void Update()
  {
    if(TutorialConfigurator.Instance && TutorialConfigurator.Instance.tutorialEventManager)
      gameWasWon = TutorialConfigurator.Instance.tutorialEventManager.GameWasWon;
    gameDuration = Time.timeSinceLevelLoad;
  }
  private void OnDestroy()
  {
    if (mode == TutorialConfigurator.ExecutionMode.step4PlayerPlayThrough)
    {
      //DatabaseHandler.getInstance().SavePlayerStatScript(new PlayerPlaythroughStats(gameWasWon, gameDuration, startTimeInUnix));
    }
  }
}
