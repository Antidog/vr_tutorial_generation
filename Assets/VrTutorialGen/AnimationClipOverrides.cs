﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.VrTutorialGen
{
  public class AnimationClipOverrides : List<KeyValuePair<AnimationClip, AnimationClip>>
  {
    // https://docs.unity3d.com/ScriptReference/AnimatorOverrideController.ApplyOverrides.html
    public AnimationClipOverrides(int capacity) : base(capacity) { }

    public AnimationClip this[string name]
    {
      get { return this.Find(x => x.Key.name.Equals(name)).Value; }
      set
      {
        int index = this.FindIndex(x => x.Key.name.Equals(name));
        if (index != -1)
          this[index] = new KeyValuePair<AnimationClip, AnimationClip>(this[index].Key, value);
      }
    }
  }
}
