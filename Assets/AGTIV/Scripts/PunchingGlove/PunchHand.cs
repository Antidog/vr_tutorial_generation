using AGTIV.Scripts.Management;
using UnityEngine;

namespace AGTIV.Scripts.PunchingGlove
{
    public class PunchHand : MonoBehaviour, IResetable
    {
        [SerializeField] private bool isMoving = true;
        [SerializeField] private Vector3 speed = new Vector3(0, 0, -0.1f);
        [SerializeField] private float maxTravelDistance = -1;
        [SerializeField] private GameObject spring;

        public Vector3 currentBoundingBox;


        private float _movementDirection = 1;
        
        private Vector3 _initialLocation;
        private Vector3 _initialSpringBoundExtents;
        private Vector3 _initialSpringScale;
        private MeshRenderer _springMeshRenderer;

        void Start()
        {
            _initialLocation = this.transform.position;
            _springMeshRenderer = spring.GetComponent<MeshRenderer>();
            _initialSpringBoundExtents = _springMeshRenderer.bounds.extents;
            _initialSpringScale = spring.transform.localScale;
        }

        // Update is called once per frame
        void Update()
        {
            currentBoundingBox = _springMeshRenderer.bounds.extents;
            
            if (!isMoving) {return;}

            this.transform.position += speed * (Time.deltaTime * _movementDirection);

            var currentDeformation = Vector3.Distance(_initialLocation, this.transform.position);
            
            var extensionRatio = (currentDeformation - _initialSpringBoundExtents.z)/_initialSpringBoundExtents.z;
            spring.transform.localScale = new Vector3(_initialSpringScale.x, _initialSpringScale.y,
                _initialSpringScale.z * extensionRatio);


            if (currentDeformation >= maxTravelDistance)
            {
                isMoving = false;
            }
        }

        public void Punch()
        {
            isMoving = true;
        }

        public void OnReset()
        {
            isMoving = false;
            this.transform.position = _initialLocation;
            spring.transform.localScale = _initialSpringScale;
        }
    }
}