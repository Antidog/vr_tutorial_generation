using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using AGTIV.Scripts.Management;
using UnityEngine.SceneManagement;

public class HeadsetRemovalDetection : MonoBehaviour
{
  Player player;
  LoadLevel level;
  public Camera alternativeCam;
  bool stateReached = false;
  void Start()
  {
    player = FindObjectOfType<Player>();
    player.headsetOnHead.AddOnChangeListener(HeadsetPutOnHeadCallback, Valve.VR.SteamVR_Input_Sources.Any);
    level = FindObjectOfType<LoadLevel>();
    alternativeCam.gameObject.SetActive(LoadLevel.shouldPutHeadsetOnHead);
  }
  private void HeadsetPutOnHeadCallback(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
  {
    if(LoadLevel.shouldPutHeadsetOnHead == newState && !stateReached)
    {
      stateReached = true;
      player.headsetOnHead.RemoveOnChangeListener(HeadsetPutOnHeadCallback, Valve.VR.SteamVR_Input_Sources.Any); //player persists otherwise multiple calls at once
      level.LoadNextScene();
    }
  }
}
