using UnityEngine;
using UnityEngine.Events;

namespace AGTIV.Scripts.ButtonRed
{
    public class PunchButtonOverlap : MonoBehaviour
    {
        public UnityEvent onButtonPressed;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("tag_button"))
            {
                Debug.Log("Object Overlapped with trigger area");
                onButtonPressed.Invoke();
            }
        }
    }
}
