using System;
using AGTIV.Scripts.Management;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace AGTIV.Scripts.ButtonRed
{
    public class ButtonOverlapArea : MonoBehaviour, IResetable
    {
        [SerializeField] private GameObject interactableButton;
        [SerializeField] private SpringJoint joint;
        private Vector3 _buttonInitialLocation;
        private Quaternion _buttonInitialRotation;
        private float _initialSpringJoint;
        
        // TODO: Make it private
        public bool _isAlreadyActive = false;


        public UnityEvent onButtonPressed;
        public UnityEvent onButtonPressedRemotely;

        public void CallButtonPressed()
        {
            onButtonPressed.Invoke();
        }

        public void CallButtonPressedRemotely()
        {
            onButtonPressedRemotely.Invoke();
        }

        private void Start()
        {
            if (interactableButton)
            {
                _buttonInitialLocation = interactableButton.transform.position;
                _buttonInitialRotation = interactableButton.transform.rotation;
            }

            if (joint)
            {
                _initialSpringJoint = joint.spring;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_isAlreadyActive)
            {
                return;
            }

            if (other.CompareTag("tag_button"))
            {
                Debug.Log("Object Overlapped with trigger area: " + other.name);
                _isAlreadyActive = true;
                if (joint)
                {
                    joint.spring = 10000;
                }
                onButtonPressed.Invoke();
            }
        }

        public void OnReset()
        {
            if (interactableButton.GetComponent<Rigidbody>())
            {
                interactableButton.GetComponent<Rigidbody>().velocity = Vector3.zero;
                interactableButton.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
            if (joint)
            {
                joint.spring = _initialSpringJoint;
            }
            interactableButton.transform.position = _buttonInitialLocation;
            interactableButton.transform.rotation = _buttonInitialRotation;
        }
        
    }
}