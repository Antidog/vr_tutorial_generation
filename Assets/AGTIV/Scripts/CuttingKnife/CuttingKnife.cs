using UnityEngine;

public class CuttingKnife : MonoBehaviour
{
    public bool _isKnifeRotating = false;
    private Vector3 _knifeInitialRotation;
    private float _initialRotationTime;
    private float _accumulativeRotation = 0;

    [SerializeField] private float rotationSpeed = 360;
    [SerializeField] private float speedReductionMultiplier = 1;


    // Update is called once per frame
    void Update()
    {
        if (!_isKnifeRotating)
        {
            return;
        }

        var deltaRotation = new Vector3(0, 1, 0) * (rotationSpeed * Time.deltaTime);
        this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + deltaRotation);
        
        _accumulativeRotation += deltaRotation.y;
        
        if (_accumulativeRotation >= 360)
        {
            _isKnifeRotating = false;
            _accumulativeRotation = 0;
            this.transform.rotation = Quaternion.Euler(_knifeInitialRotation);
        }
    }

    public void BeginKnifeRotation()
    {
        Debug.Log("Begin Knife Rotation");
        
        _isKnifeRotating = true;
        _knifeInitialRotation = this.transform.rotation.eulerAngles;
        _initialRotationTime = Time.time;
    }
}