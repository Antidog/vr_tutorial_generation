using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class VRCallibration : MonoBehaviour
{
    public SteamVR_Action_Boolean reorientation;
    [SerializeField] protected Camera vrCamera;
    [SerializeField] protected GameObject offset;
    [SerializeField] protected GameObject centerIndicator;


    [Header("Teleport")] 
    private Transform playAreaPreviewTransform;
    private Transform[] playAreaPreviewCorners;
    private Transform[] playAreaPreviewSides;
    public bool isShowingPlayArea = false;
    public GameObject playAreaPreviewCorner;
    public GameObject playAreaPreviewSide;

    private void Start()
    {
        if (isShowingPlayArea)
        {
            ShowPlayArea();
        }
    }

    void Update()
    {
        if (reorientation.stateUp)
        {
            var vrCameraForward = vrCamera.transform.forward;
            var vrCameraForwardProjected = new Vector3(vrCameraForward.x, 0, vrCameraForward.z);


            var deltaPosition = vrCamera.transform.position - this.gameObject.transform.position;
            deltaPosition = Vector3.ProjectOnPlane(deltaPosition, this.transform.up);

            this.gameObject.transform.position += deltaPosition;

            this.gameObject.transform.rotation = Quaternion.LookRotation(vrCameraForwardProjected);
            var angle = Vector3.Angle(vrCameraForwardProjected.normalized, offset.transform.forward);
            transform.RotateAround(offset.transform.position, offset.transform.up, angle);

            if (Vector3.Angle(this.transform.forward, vrCameraForwardProjected) > 10)
            {
                print("Should do negative transform");
                transform.RotateAround(offset.transform.position, offset.transform.up, -2 * angle);
            }
            else
            {
                print("rotation is fine");
            }

            centerIndicator.SetActive(true);
            Invoke(nameof(DeactivateCenterIndicator), 5);
        }
    }

    void DeactivateCenterIndicator()
    {
        centerIndicator.SetActive(false);
    }

    void ShowPlayArea()
    {
        ChaperoneInfo chaperone = ChaperoneInfo.instance;

        if (chaperone.initialized && chaperone.roomscale)
        {
            //Set up the render model for the play area bounds
            playAreaPreviewTransform = new GameObject("PlayAreaPreviewTransform").transform;
            playAreaPreviewTransform.parent = transform;
            Util.ResetTransform(playAreaPreviewTransform);

            playAreaPreviewCorner.SetActive(true);
            playAreaPreviewCorners = new Transform[4];
            playAreaPreviewCorners[0] = playAreaPreviewCorner.transform;
            playAreaPreviewCorners[1] = Instantiate(playAreaPreviewCorners[0]);
            playAreaPreviewCorners[2] = Instantiate(playAreaPreviewCorners[0]);
            playAreaPreviewCorners[3] = Instantiate(playAreaPreviewCorners[0]);

            playAreaPreviewCorners[0].transform.parent = playAreaPreviewTransform;
            playAreaPreviewCorners[1].transform.parent = playAreaPreviewTransform;
            playAreaPreviewCorners[2].transform.parent = playAreaPreviewTransform;
            playAreaPreviewCorners[3].transform.parent = playAreaPreviewTransform;

            playAreaPreviewSide.SetActive(true);
            playAreaPreviewSides = new Transform[4];
            playAreaPreviewSides[0] = playAreaPreviewSide.transform;
            playAreaPreviewSides[1] = Instantiate(playAreaPreviewSides[0]);
            playAreaPreviewSides[2] = Instantiate(playAreaPreviewSides[0]);
            playAreaPreviewSides[3] = Instantiate(playAreaPreviewSides[0]);

            playAreaPreviewSides[0].transform.parent = playAreaPreviewTransform;
            playAreaPreviewSides[1].transform.parent = playAreaPreviewTransform;
            playAreaPreviewSides[2].transform.parent = playAreaPreviewTransform;
            playAreaPreviewSides[3].transform.parent = playAreaPreviewTransform;


            float x = chaperone.playAreaSizeX;
            float z = chaperone.playAreaSizeZ;

            playAreaPreviewSides[0].localPosition = new Vector3(0.0f, 0.0f, 0.5f * z - 0.25f);
            playAreaPreviewSides[1].localPosition = new Vector3(0.0f, 0.0f, -0.5f * z + 0.25f);
            playAreaPreviewSides[2].localPosition = new Vector3(0.5f * x - 0.25f, 0.0f, 0.0f);
            playAreaPreviewSides[3].localPosition = new Vector3(-0.5f * x + 0.25f, 0.0f, 0.0f);

            playAreaPreviewSides[0].localScale = new Vector3(x - 0.5f, 1.0f, 1.0f);
            playAreaPreviewSides[1].localScale = new Vector3(x - 0.5f, 1.0f, 1.0f);
            playAreaPreviewSides[2].localScale = new Vector3(z - 0.5f, 1.0f, 1.0f);
            playAreaPreviewSides[3].localScale = new Vector3(z - 0.5f, 1.0f, 1.0f);

            playAreaPreviewSides[0].localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            playAreaPreviewSides[1].localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            playAreaPreviewSides[2].localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            playAreaPreviewSides[3].localRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);

            playAreaPreviewCorners[0].localPosition = new Vector3(0.5f * x - 0.25f, 0.0f, 0.5f * z - 0.25f);
            playAreaPreviewCorners[1].localPosition = new Vector3(0.5f * x - 0.25f, 0.0f, -0.5f * z + 0.25f);
            playAreaPreviewCorners[2].localPosition = new Vector3(-0.5f * x + 0.25f, 0.0f, -0.5f * z + 0.25f);
            playAreaPreviewCorners[3].localPosition = new Vector3(-0.5f * x + 0.25f, 0.0f, 0.5f * z - 0.25f);

            playAreaPreviewCorners[0].localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            playAreaPreviewCorners[1].localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            playAreaPreviewCorners[2].localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
            playAreaPreviewCorners[3].localRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);

            playAreaPreviewTransform.gameObject.SetActive(true);
        }
    }
}