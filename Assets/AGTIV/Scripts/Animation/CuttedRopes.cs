using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace AGTIV.Scripts.Animation
{
    public class CuttedRopes : MonoBehaviour
    {
        [SerializeField] private List<SkinnedMeshRenderer> ropes = new List<SkinnedMeshRenderer>();
        [SerializeField] private float speed = 1;

        [SerializeField] private List<GameObject> staticObjects = new List<GameObject>();
        [SerializeField] private List<GameObject> staticObjectsFinalPosition = new List<GameObject>();

        [SerializeField] private List<Animator> animators = new List<Animator>();

        private bool _isAnimating = false;
        private float _blendShapeWeight = 0;
        private float _ratio = 0;
        private List<GameObject> _tempStaticObjectsInitial = new List<GameObject>();
        private static readonly int IsPlaying = Animator.StringToHash("_isPlaying");

        public UnityEvent onCuttingRope;

        void Start()
        {
            this.GetComponent<MeshRenderer>().enabled = false;
            
            foreach (var staticObject in staticObjects)
            {
                _tempStaticObjectsInitial.Add(staticObject);
            }

            foreach (var staticObject in staticObjectsFinalPosition)
            {
                staticObject.GetComponent<MeshRenderer>().enabled = false;
            }
        }

        void Update()
        {
            if (!_isAnimating)
                return;
            
            _ratio += speed * Time.deltaTime;

            _blendShapeWeight = _ratio * 100;
            foreach (var rope in ropes)
            {
                rope.SetBlendShapeWeight(0, _blendShapeWeight);
            }

            foreach (var animator in animators)
            {
                animator.SetBool(IsPlaying, true);
            }

            // for (int i = 0; i < staticObjects.Count; i++)
            // {
            //     var pos = Vector3.Lerp(_tempStaticObjectsInitial[i].transform.position,
            //         staticObjectsFinalPosition[i].transform.position, _ratio);
            //     var rotation = Quaternion.Lerp(_tempStaticObjectsInitial[i].transform.rotation,
            //         staticObjectsFinalPosition[i].transform.rotation, _ratio);
            //     var scale = Vector3.Lerp(_tempStaticObjectsInitial[i].transform.localScale,
            //         staticObjectsFinalPosition[i].transform.localScale, _ratio);
            //
            //     staticObjects[i].transform.position = pos;
            //     staticObjects[i].transform.rotation = rotation;
            //     staticObjects[i].transform.localScale = scale;
            // }

            if (_ratio >= 1)
            {
                _isAnimating = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("tag_blade"))
            {
                Debug.Log("Blade is Overlapped");
                _isAnimating = true;
                onCuttingRope.Invoke();;
            }
        }
    }
}