using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAlwaysInCameraView : MonoBehaviour
{
    private bool _isInHand = false;
    [SerializeField] private Camera vrCamera;
    [SerializeField] private float minimumDistanceToCamera = 0.05f;
    [SerializeField] private float maximumDistanceToCamera = 1f;
    [SerializeField] private float optimalDistance = 0.5f;
    [SerializeField] private float angleThreshold = 45;
    [SerializeField] private Vector3 offset = new Vector3(0, -0.3f, 0);
    [SerializeField] private GameObject hintObject;
    private Transform _initialTransform;

    private void OnEnable()
    {
        _initialTransform = this.transform;
    }

    void Update()
    {
        if (!_isInHand && ShouldUpdateLocation())
        {
            var cameraPosition = vrCamera.transform.position;
            var cameraForward = vrCamera.transform.forward;

            var cameraProjectedForward = new Vector3(cameraForward.x, 0, cameraForward.z);
            var newObjectPosition = cameraPosition + optimalDistance * cameraProjectedForward + offset;
            this.transform.position = newObjectPosition;

        }

        if (hintObject)
        {
            hintObject.transform.LookAt(vrCamera.transform.position, this.transform.up);
        }
    }

    private bool ShouldUpdateLocation()
    {
        var shouldUpdateLocation = false;

        var cameraTransform = vrCamera.transform;
        var angle = Vector3.Angle(cameraTransform.forward,
            (this.transform.position - cameraTransform.position).normalized);
        if (angle > angleThreshold)
        {
            shouldUpdateLocation = true;
        }

        var distanceToCamera = Vector3.Distance(vrCamera.transform.position, this.transform.position);
        if (distanceToCamera > maximumDistanceToCamera || distanceToCamera < minimumDistanceToCamera)
        {
            shouldUpdateLocation = true;
        }

        return shouldUpdateLocation;
    }

    public void SetIsGrabbed(bool isGrabbed)
    {
        _isInHand = isGrabbed;
    }

    public bool GetIsGrabbed()
    {
        return _isInHand;
    }

    public void ResetTransform()
    {
        this.transform.rotation = _initialTransform.rotation;
        this.transform.position = _initialTransform.position;
    }
}