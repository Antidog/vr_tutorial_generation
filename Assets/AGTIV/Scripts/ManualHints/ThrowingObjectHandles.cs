using System;
using AGTIV.Scripts.Hint;
using UnityEngine;

namespace AGTIV.Scripts.ManualHints
{
    public class ThrowingObjectHandles : MonoBehaviour
    {
        [SerializeField] private Hint.Hint beginHint;
        [SerializeField] private bool shouldCheckForCollision = true;
        [SerializeField] private bool shouldDestroyOnTriggerEnter = true;
        [SerializeField] private float delayToDestroy = 1;
        private void OnCollisionEnter(Collision collision)
        {
            if (!shouldCheckForCollision || !collision.gameObject.CompareTag("tag_deadZone"))
            {
                return;
            }
            
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            HintManager.Instance.ResetToHint(beginHint);
            
            if(this.gameObject.GetComponent<ObjectAlwaysInCameraView>())
            {
                this.gameObject.GetComponent<ObjectAlwaysInCameraView>().ResetTransform();
                this.gameObject.GetComponent<ObjectAlwaysInCameraView>().SetIsGrabbed(false);
            }
            HintManager.Instance.PlayError();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("tag_ThrowArea") )
            {
                return;
            }

            shouldCheckForCollision = false;
            
            HintManager.Instance.PlayAccept();
            if(shouldDestroyOnTriggerEnter)
                HintManager.Instance.DestroyByDelay(this.gameObject, delayToDestroy);
            HintManager.Instance.GoToNextHint();
        }
    }
}
