using UnityEngine;

namespace AGTIV.Scripts.ManualHints
{
    public class UILookAtCamera : MonoBehaviour
    {

        public Camera camera;
        [SerializeField] private bool isLookingAtCamera = true;
        [SerializeField] private bool invertRotation = true;
        [SerializeField] private Vector3 upDirection = new Vector3(0,1,0);

        private Transform _transform;
        void Start()
        {
            if (!camera)
            {
                camera = Camera.main;
            }

            _transform = this.GetComponent<Transform>();
        }

        void Update()
        {
            if (isLookingAtCamera)
            {
                _transform.LookAt(camera.transform, upDirection);
                if (invertRotation)
                {
                    _transform.RotateAround(_transform.position, _transform.up, 180);
                }
            }
        }
    }
}
