using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGTIV.Scripts.HandPosition
{
    enum EHandType
    {
        Right,
        Left
    }

    public class WriteHandPosition : MonoBehaviour
    {
        [SerializeField] private EHandType handType = EHandType.Right;
        private static readonly int RightHandPos = Shader.PropertyToID("_rightHandPos");
        private static readonly int LeftHandPos = Shader.PropertyToID("_leftHandPos");

        void Update()
        {
            var pos = transform.position;
            if (handType == EHandType.Right)
            {
                Shader.SetGlobalVector(RightHandPos, new Vector4(pos.x, pos.y, pos.z, 0));
            }
            else if (handType == EHandType.Left)
            {
                Shader.SetGlobalVector(LeftHandPos, new Vector4(pos.x, pos.y, pos.z, 0));
            }
        }
    }
}