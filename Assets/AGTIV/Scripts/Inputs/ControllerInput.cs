using AGTIV.Scripts.Management;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

namespace AGTIV.Scripts.Inputs
{
    public class ControllerInput : MonoBehaviour
    {
        public SteamVR_Action_Boolean playButton;
        public SteamVR_Action_Boolean touchPlayButton;
        public SteamVR_Action_Boolean resetButton;
        public SteamVR_Action_Boolean touchResetButton;

        public UnityEvent onPlayButtonPressed;
        public UnityEvent onResetButtonPressed;

        [SerializeField] private GameObject hintPlayRight;
        [SerializeField] private GameObject hintPlayLeft;
        [SerializeField] private GameObject hintResetRight;
        [SerializeField] private GameObject hintResetLeft;

        public bool _isPlayButtonActive = true;
        public bool _isResetButtonActive = true;

        bool warned = false;
        void Update()
        {
            if (!touchPlayButton.active)
            {
              if(!warned)
                Debug.LogWarning("Current Controller setup does not have touchPlayButton configured.");
              warned = true;
              return;
            }
            if (!GamePhysics.Instance.isPlayingPhysics && _isPlayButtonActive)
            {
              TogglePlayHint(touchPlayButton.state, touchPlayButton.activeDevice);

                if (playButton.state)
                {
                    onPlayButtonPressed.Invoke();
                    TogglePlayHint(false, SteamVR_Input_Sources.RightHand);
                    TogglePlayHint(false, SteamVR_Input_Sources.LeftHand);
                }
            }
            else
            {
                if (_isResetButtonActive)
                {
                    ToggleResetHint(touchResetButton.state, touchResetButton.activeDevice);

                    if (resetButton.state)
                    {
                        onResetButtonPressed.Invoke();
                        ToggleResetHint(false, SteamVR_Input_Sources.RightHand);
                        ToggleResetHint(false, SteamVR_Input_Sources.LeftHand);
                    }
                }
            }
        }

        void TogglePlayHint(bool isActive, SteamVR_Input_Sources inputSources)
        {
            if (inputSources == SteamVR_Input_Sources.RightHand)
            {
                hintPlayRight.SetActive(isActive);
            }
            else if (inputSources == SteamVR_Input_Sources.LeftHand)
            {
                hintPlayLeft.SetActive(isActive);
            }
        }

        void ToggleResetHint(bool isActive, SteamVR_Input_Sources inputSources)
        {
            if (inputSources == SteamVR_Input_Sources.RightHand)
            {
                hintResetRight.SetActive(isActive);
            }
            else if (inputSources == SteamVR_Input_Sources.LeftHand)
            {
                hintResetLeft.SetActive(isActive);
            }
        }

        public void TogglePlayButtonActivation(bool isActive)
        {
            _isPlayButtonActive = isActive;
        }
        
        public void ToggleResetButtonActivation(bool isActive)
        {
            _isResetButtonActive = isActive;
        }
    }
}