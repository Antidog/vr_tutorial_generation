using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHighlight : MonoBehaviour
{
    private GameObject _buttonB;
    private Material _buttonBMaterial;
    private GameObject _buttonA;
    private Material _buttonAMaterial;
    private GameObject _trigger;
    private Material _triggerMaterial;

    private bool _isAllFound = false;


    [SerializeField] private Material activeMaterial;
    [SerializeField] private bool shouldSearchForButtons = true;

    void Start()
    {
        GetButtons();
    }

    private void Update()
    {
        if (!shouldSearchForButtons)
            return;

        if (!_isAllFound)
        {
            GetButtons();
        }
    }

    private void GetButtons()
    {
        _buttonA = GameObject.Find("button_a");
        if (_buttonA)
            _buttonAMaterial = _buttonA.GetComponent<Renderer>().material;
        else
        {
            _isAllFound = false;
            return;
        }


        _buttonB = GameObject.Find("button_b");
        if (_buttonB)
            _buttonBMaterial = _buttonA.GetComponent<Renderer>().material;
        else
        {
            _isAllFound = false;
            return;
        }

        _trigger = GameObject.Find("trigger");
        if (_trigger)
            _triggerMaterial = _buttonA.GetComponent<Renderer>().material;
        else
        {
            _isAllFound = false;
            return;
        }

        _isAllFound = true;
    }

    public void ToggleButtonAMaterial(bool isActive)
    {
        if (isActive)
        {
            if (!_buttonA)
            {
                _buttonA = GameObject.Find("button_a");
                _buttonAMaterial = _buttonA.GetComponent<Renderer>().material;
            }

            _buttonA.GetComponent<Renderer>().material = activeMaterial;
        }
        else
        {
            if (!_buttonA)
            {
                return;
            }

            _buttonA.GetComponent<Renderer>().material = _buttonAMaterial;
        }
    }

    public void ToggleButtonBMaterial(bool isActive)
    {
        if (isActive)
        {
            if (!_buttonB)
            {
                _buttonB = GameObject.Find("button_a");
                _buttonBMaterial = _buttonB.GetComponent<Renderer>().material;
            }

            _buttonB.GetComponent<Renderer>().material = activeMaterial;
        }
        else
        {
            if (!_buttonB)
            {
                return;
            }

            _buttonB.GetComponent<Renderer>().material = _buttonBMaterial;
        }
    }

    public void ToggleTriggerMaterial(bool isActive)
    {
        if (isActive)
        {
            if (!_trigger)
            {
                _trigger = GameObject.Find("button_a");
                if (_trigger)
                    _triggerMaterial = _trigger.GetComponent<Renderer>().material;
                else
                    return;
            }

            _trigger.GetComponent<Renderer>().material = activeMaterial;
        }
        else
        {
            if (!_trigger)
            {
                return;
            }

            _trigger.GetComponent<Renderer>().material = _triggerMaterial;
        }
    }
}