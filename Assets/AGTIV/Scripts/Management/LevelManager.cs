using System;
using System.Collections;
using System.Collections.Generic;
using GameLabGraz.LimeSurvey;
using GameLabGraz.LimeSurvey.Data;
using GEAR.Gadgets.Coroutine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace AGTIV.Scripts.Management
{
    public class LevelManager : MonoBehaviour
    {
        private LevelManager _instance;

        public LevelManager Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType<LevelManager>();
                }

                return _instance;
            }
        }

        [SerializeField] private bool shouldResetMeasurement = true;
        [SerializeField] private bool shouldUploadMeasurement = true;
        
        [SerializeField] private float maximumGameplayDuration = 900;
        [SerializeField] private StudyMeasurements studyMeasurements;
        
        public float levelBeginTime, levelEndTime;
        public UnityEvent onFinishEvent;

        private void Start()
        {
            levelBeginTime = Time.time;

            if (shouldResetMeasurement)
            {
                if (studyMeasurements)
                {
                    studyMeasurements.levelName = SceneManager.GetActiveScene().name;
                }
            
                ResetMeasurements();
            }
        }

        private void ResetMeasurements()
        {
            studyMeasurements.levelDuration = 0;
            studyMeasurements.isSuccessful = false;
            studyMeasurements.resetCount = 0;
        }

        public void OnFinishLevel(bool isSuccessful)
        {
            if (shouldUploadMeasurement)
            {
                levelEndTime = Time.time;
                studyMeasurements.levelDuration = levelEndTime - levelBeginTime;
                studyMeasurements.isSuccessful = isSuccessful;
            
                StartCoroutine(SubmitMeasurements());
            }
            
            Invoke(nameof(CallOnFinishEvent), 5); 
        }

        private void CallOnFinishEvent()
        {
            DestroyAllRigidBodies();

            onFinishEvent.Invoke();
        }

        private static void DestroyAllRigidBodies()
        {
            var rigidBodies = FindObjectsOfType<Rigidbody>();
            foreach (var rigidBody in rigidBodies)
            {
                Destroy(rigidBody.gameObject);
            }
        }

        private void Update()
        {
            if (Time.time - levelBeginTime >= maximumGameplayDuration)
            {
                TimesUp();
            }
        }

        private void TimesUp()
        {
            OnFinishLevel(false);
        }

        public void AddResetCount()
        {
            studyMeasurements.resetCount++;
        }

        private IEnumerator SubmitMeasurements()
        {
            var ql = new CoroutineWithData(this, LimeSurveyManager.Instance.GetQuestionList());
            yield return ql.Coroutine;
            
            var questionList = (List<Question>)ql.Result;
            
            foreach (var question in questionList)
            {
                if (question.QuestionText.ToLower().Contains("id"))
                {
                    question.Answer = studyMeasurements.userID.ToString();
                }
                else if (question.QuestionText.ToLower().Contains("name"))
                {
                    question.Answer = studyMeasurements.levelName;
                }
                else if (question.QuestionText.ToLower().Contains("duration"))
                {
                    question.Answer = studyMeasurements.levelDuration.ToString();
                }
                else if (question.QuestionText.ToLower().Contains("reset"))
                {
                    question.Answer = studyMeasurements.resetCount.ToString();
                }
                else if (question.QuestionText.ToLower().Contains("successful"))
                {
                    question.Answer = studyMeasurements.isSuccessful.ToString();
                }
            }
            
            var cd = new CoroutineWithData(this, LimeSurveyManager.Instance.UploadQuestionResponses(questionList));
            yield return cd.Coroutine;

            var responseID = (int)cd.Result;
            if (responseID == -1)
                Debug.LogError("LimeSurveyUploader::OnSubmission: Unable to submit responses.");

        }
    }
}