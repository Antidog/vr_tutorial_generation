using UnityEngine;

namespace AGTIV.Scripts.Management
{
    [CreateAssetMenu(fileName = "UsernamePassword", menuName = "Username and Password", order = 0)]
    public class UserPass : ScriptableObject
    {
        public string userName = "NaN";
        public string password = "NaN";
    }
}