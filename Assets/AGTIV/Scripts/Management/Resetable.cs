using AGTIV.Scripts.Management;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resetable : MonoBehaviour, IResetable
{
  Vector3 _initialLocation;
  Quaternion _initialRotation;
  void Start()
  {
    _initialLocation = this.transform.position;
    _initialRotation = this.transform.rotation;
  }
  public void OnReset()
  {
    transform.position = _initialLocation;
    transform.rotation = _initialRotation;
  }
}
