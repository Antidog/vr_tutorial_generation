using UnityEngine;

namespace AGTIV.Scripts.Management
{
    public interface ISaveable
    {
        public void RestoreSave();
        public void Save();

        public void ShouldStayInPlayArea();


    }
}