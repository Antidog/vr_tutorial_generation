using UnityEngine;

namespace AGTIV.Scripts.Management
{
    public interface IResetable
    {
        void OnReset();
    }
}