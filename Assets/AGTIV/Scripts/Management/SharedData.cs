using UnityEngine;

namespace AGTIV.Scripts.Management
{
    [CreateAssetMenu(fileName = "SharedData", menuName = "SharedData", order = 0)]
    public class SharedData : ScriptableObject
    {
      public int userId = 0;
    }
}