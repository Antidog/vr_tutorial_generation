using System.Collections.Generic;
using GameLabGraz.LimeSurvey;
using UnityEngine;

namespace AGTIV.Scripts.Management
{
    public class UpdateUserID : MonoBehaviour
    {
        public List<StudyMeasurements> measurementsList = new List<StudyMeasurements>();
        public SharedData sharedDataAsset;
        public int gameUserID;

        public void HandleUserID(LimeSurveyView limeSurveyView)
        {
            if (limeSurveyView.CurrentQuestion.QuestionText.ToLower().Contains("user id"))
                {
                    UpdateUserIDValue(int.Parse(limeSurveyView.CurrentQuestion.Answer));
                }
        }

        public void UpdateUserIDValue(int userID)
        {
            this.gameUserID = userID;
            sharedDataAsset.userId = userID;
        }

        public void UpdateUserIDForMeasurement()
        {
            foreach (var measurement in measurementsList)
            {
                measurement.userID = sharedDataAsset.userId;
            }
        }
    }
}
