using UnityEngine;

namespace AGTIV.Scripts.Management
{
    [CreateAssetMenu(fileName = "Measurements", menuName = "StudyMeasurements", order = 0)]
    public class StudyMeasurements : ScriptableObject
    {
        public int userID;
        public string levelName;
        public float levelDuration = 0;
        public int resetCount = 0;
        public bool isSuccessful = false;
    }
}