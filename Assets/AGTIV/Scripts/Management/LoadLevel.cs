using UnityEngine;
using UnityEngine.SceneManagement;

namespace AGTIV.Scripts.Management
{
    public class LoadLevel : MonoBehaviour
    {
        [SerializeField] private StudyMeasurements studyMeasurement;
        [SerializeField] public SharedData sharedData;
        private static int levelNumber = 0;
        public static bool shouldPutHeadsetOnHead = false;
        [Header("Scenes")]
        [SerializeField] private string preQuestionareScene = "00_scene_LimeSurvey_PreQuestionnaire";
        [SerializeField] private string automaticTutorialScene = "01a_scene_Bedroom_PrepareForTutorial_auto";
        [SerializeField] private string manualTutorialScene = "01b_scene_Bedroom_PrepareForTutorial_manual";
        [SerializeField] private string challengeScene = "02_scene_Bedroom_Challenge_Gameplay";
        [SerializeField] private string postQuestionnaire = "03_scene_LimeSurvey_PostQuestionnaire";
        [SerializeField] private string userPreferences = "04_scene_LimeSurvey_UserPreferences";
        [SerializeField] private string generalHints = "05_simple_interactionTutorial";
        [SerializeField] private string transitionScene = "06_scene_transition";

    public void LoadNextTutorialLevel()
    {
      LoadNextScene();
    }

    public void LoadPostQuestionnaireLevel()
    {
      LoadNextScene();
    }
    public void LoadUserPreferencesLevel()
    {
      LoadNextScene();
    }

    public void LoadChallengeLevel()
    {
      LoadNextScene();
    }

    public void LoadGeneralHintsLevel()
    {
      Invoke(nameof(LoadNextScene), 2);
    }
    public void LoadRemoveHeadsetScene()
    {
      LoadNextScene();
    }
    public void LoadNextScene()
    {
      var id = sharedData.userId;
      levelNumber++;
      TutorialEventManager toRemove = FindObjectOfType<TutorialEventManager>();
      if (toRemove)
      {
        Destroy(toRemove);
      }
      Debug.Log("Switch to scene " + levelNumber);
      switch (levelNumber)
      {
        case 0:
          shouldPutHeadsetOnHead = false;
          SceneManager.LoadScene(preQuestionareScene);
          //put on headset
          break;
        case 1:
          SceneManager.LoadScene(transitionScene);
          shouldPutHeadsetOnHead = true;
          //put on headset
          break;
        case 2:
          SceneManager.LoadScene(generalHints);
          //button tuorial
          break;
        case 3:
          SceneManager.LoadScene(id > 2000 ? manualTutorialScene : automaticTutorialScene);
          //first tutorial
          break;
        case 4:
          SceneManager.LoadScene(challengeScene);
          //challange level
          break;
        case 5:
          SceneManager.LoadScene(transitionScene);
          shouldPutHeadsetOnHead = false;
          //remove headset
          break;
        case 6:
          SceneManager.LoadScene(postQuestionnaire);
          //post q
          break;
        case 7:
          SceneManager.LoadScene(transitionScene);
          shouldPutHeadsetOnHead = true;
          //put on headset
          break;
        case 8:
          SceneManager.LoadScene(id > 2000 ? automaticTutorialScene : manualTutorialScene);
          //2nd tutorial level
          break;
        case 9:
          SceneManager.LoadScene(transitionScene);
          shouldPutHeadsetOnHead = false;
          //remove headset
          break;
        case 10:
          SceneManager.LoadScene(userPreferences);
          //perference q
          break;
        default:
          Debug.LogError("Unexpected scene workflow - restarting. old #" + levelNumber + " Exiting application");
          levelNumber = 0;
          sharedData.userId = 0;
          Application.Quit();
          break;

      }
    }
  }
}
