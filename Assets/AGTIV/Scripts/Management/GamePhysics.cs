using System.Collections.Generic;
using System.Linq;
using AGTIV.Scripts.ButtonRed;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace AGTIV.Scripts.Management
{
    public class GamePhysics : MonoBehaviour
    {
        private static GamePhysics _instance;

        public static GamePhysics Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType<GamePhysics>();
                }

                return _instance;
            }
        }

        [SerializeField] private List<Rigidbody> fixedRigidBodyAtBeginning = new List<Rigidbody>();
        [SerializeField] private List<GameObject> buttonsThatShouldAlwaysBeInteractable = new List<GameObject>();
        [SerializeField] private GameObject playArea;
        
        [SerializeField] private LayerMask disableCollisionMaskLayer;
        [SerializeField] private LayerMask enableCollisionMaskLayer;

        private List<IResetable> _resetables = new List<IResetable>();
        private List<ISaveable> _saveables = new List<ISaveable>();

        private HandCollider[] _handColliders;
        private Hand[] _hands;

        public bool isPlayingPhysics = false;

        void Start()
        {
            ToggleFixedRigidBodies(true);
            StoreObjectLists();

            _handColliders = FindObjectsOfType<HandCollider>();
            _hands = FindObjectsOfType<Hand>();

            Invoke(nameof(InitializingHandCollisions), 1);
        }

        private void ToggleHandCollision(bool isEnableCollision)
        {
            if (_handColliders.Length == 0)
            {
                _handColliders = FindObjectsOfType<HandCollider>();
            }

            if (_hands.Length == 0)
            {
                _hands = FindObjectsOfType<Hand>();
            }

            print("Hand Colliders Count: " + _handColliders.Length);
            print("Hands Count: " + _handColliders.Length);

            LayerMask handCollisionLayer;
            LayerMask handMaskLayer;
            if (isEnableCollision)
            {
                handCollisionLayer = LayerMask.NameToLayer("AlwaysPhysical");
                handMaskLayer = enableCollisionMaskLayer;
            }
            else
            {
                handCollisionLayer = LayerMask.NameToLayer("SelfCollisionOnly");
                handMaskLayer = disableCollisionMaskLayer;
            }

            foreach (var handCollider in _handColliders)
            {
                foreach (var childCollider in handCollider.GetComponentsInChildren<Collider>())
                {
                    childCollider.gameObject.layer = handCollisionLayer;
                }
            }


            foreach (var hand in _hands)
            {
                hand.hoverLayerMask = handMaskLayer;
            }

            foreach (var hand in _hands)
            {
              GameObject obj = hand.currentAttachedObject;
              if (obj)
              {
                hand.DetachObject(hand.currentAttachedObject);
                Resetable r = obj.GetComponent<Resetable>();
                if (r)
                  r.OnReset();
              }     
            }
            foreach (var button in buttonsThatShouldAlwaysBeInteractable) 
              foreach (var childCollider in button.GetComponentsInChildren<Collider>())
              {
                childCollider.gameObject.layer = handCollisionLayer;
              }
        }

        public void BeginSimulation()
        {
            Saving();
            ToggleFixedRigidBodies(false);
            CheckIfObjectAllowedToBeThere();

            ToggleHandCollision(false);
        }

        private void StoreObjectLists()
        {
            _resetables.Clear();
            var tempResetablesList = FindObjectsOfType<MonoBehaviour>().OfType<IResetable>();
            foreach (var resetable in tempResetablesList)
            {
                _resetables.Add(resetable);
            }

            _saveables.Clear();
            var tempSaveableList = FindObjectsOfType<MonoBehaviour>().OfType<ISaveable>();
            foreach (var saveable in tempSaveableList)
            {
                _saveables.Add(saveable);
            }
        }

        public void ToggleFixedRigidBodies(bool isFixed)
        {
            foreach (var body in fixedRigidBodyAtBeginning)
            {
                body.isKinematic = isFixed;

                /*if(body.GetComponent<Collider>()){body.GetComponent<Collider>().enabled = !isFixed;}
                foreach (var component in body.GetComponentsInChildren<Collider>())
                {
                    component.enabled = !isFixed;
                }*/

                body.constraints = isFixed ? RigidbodyConstraints.FreezeAll : RigidbodyConstraints.FreezePositionX;
            }
        }

        public void ResetAllObjects()
        {
            StoreObjectLists();
            
            foreach (var resetable in _resetables)
            {
                resetable.OnReset();
            }

            foreach (var saveable in _saveables)
            {
                saveable.RestoreSave();
            }

            ToggleFixedRigidBodies(true);

            // TODO: Should happen with a general method for buttons
            foreach (var buttonOverlapArea in FindObjectsOfType<ButtonOverlapArea>())
            {
                buttonOverlapArea._isAlreadyActive = false;
            }

            ToggleHandCollision(true);
        }

        public void Saving()
        {
            StoreObjectLists();
            foreach (var saveable in _saveables)
            {
                saveable.Save();
            }
        }

        public Vector3 GetPlayAreaBounds()
        {
            return playArea.GetComponent<MeshRenderer>().bounds.extents;
        }

        public Vector3 GetPlayAreaPosition()
        {
            return playArea.transform.position;
        }

        public void CheckIfObjectAllowedToBeThere()
        {
            foreach (var saveable in _saveables)
            {
                saveable.ShouldStayInPlayArea();
            }
        }

        public void TogglePlayingPhysics(bool isPlaying)
        {
            isPlayingPhysics = isPlaying;
        }

        private void InitializingHandCollisions()
        {
            ToggleHandCollision(true);
        }
    }
}