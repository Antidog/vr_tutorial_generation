using AGTIV.Scripts.Management;
using UnityEngine;

namespace AGTIV.Scripts.Saveable
{
    public class Saveable : MonoBehaviour, ISaveable
  {
    [SerializeField] private bool shouldStayInPlayableArea = true;
    [SerializeField] private bool alwaysResetToInitialPosition = false;

    private Vector3 _initialLocation;
        private Quaternion _initialRotation;
        private Vector3 _initialScale;
        private Vector3 _savedLocation;
        private Quaternion _savedRotation;
        private Vector3 _savedScale;
        private bool _isAlreadyPlayed = false;

        private GamePhysics _gamePhysics;

        void Start()
        {
            _initialLocation = this.transform.position;
            _initialRotation = this.transform.rotation;
            _initialScale = this.transform.localScale;
            _savedLocation = this.transform.position;
            _savedRotation = this.transform.rotation;
            _savedScale = this.transform.localScale;

            _gamePhysics = GamePhysics.Instance;
        }

        public void RestoreSave()
        {
            if (IsInPlayArea() && !alwaysResetToInitialPosition)
            {
                this.transform.position = _savedLocation;
                this.transform.rotation = _savedRotation;
                this.transform.localScale = _savedScale;
            }
            else
            {
                this.transform.position = _initialLocation;
                this.transform.rotation = _initialRotation;
                this.transform.localScale = _initialScale;
            }

            if (this.GetComponent<Rigidbody>())
            {
                this.GetComponent<Rigidbody>().velocity = Vector3.zero;
                this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
        }

        public bool IsInPlayArea()
        {
            var isInPlayArea = true;

            if (!(_savedLocation.x >
                  _gamePhysics.GetPlayAreaPosition().x - _gamePhysics.GetPlayAreaBounds().x) ||
                !(_savedLocation.x < _gamePhysics.GetPlayAreaPosition().x + _gamePhysics.GetPlayAreaBounds().x))
            {
                isInPlayArea = false;
            }

            if (!(_savedLocation.y >
                  _gamePhysics.GetPlayAreaPosition().y - _gamePhysics.GetPlayAreaBounds().y) ||
                !(_savedLocation.y < _gamePhysics.GetPlayAreaPosition().y + _gamePhysics.GetPlayAreaBounds().y))
            {
                isInPlayArea = false;
            }

            if (!(_savedLocation.z >
                  _gamePhysics.GetPlayAreaPosition().z - _gamePhysics.GetPlayAreaBounds().z) ||
                !(_savedLocation.z < _gamePhysics.GetPlayAreaPosition().z + _gamePhysics.GetPlayAreaBounds().z))
            {
                isInPlayArea = false;
            }

            return isInPlayArea;
        }

        public void Save()
        {
            _savedLocation = this.transform.position;
            _savedRotation = this.transform.rotation;
            _savedScale = this.transform.localScale;
        }

        public void ShouldStayInPlayArea()
        {
            if (shouldStayInPlayableArea)
            {
                if (!IsInPlayArea())
                {
                    this.transform.position = _initialLocation;
                    this.transform.rotation = _initialRotation;
                    this.transform.localScale = _initialScale;
                    
                    if (this.GetComponent<Rigidbody>())
                    {
                        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
                        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                    }
                }
            }
        }
     }
}
