using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AGTIV.Scripts.Balloon
{
    [RequireComponent(typeof(Rigidbody))]
    public class BalloonFly : MonoBehaviour
    {
        [SerializeField] private float flyingForce = 1;
        [SerializeField] private ParticleSystem particleSystem;
        [SerializeField] private AudioSource audioSource;

        private Rigidbody _rigidbody;
        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.useGravity = false;
            _rigidbody.isKinematic = true;

        }

        public void FlyUp()
        {
            _rigidbody.isKinematic = false;
            _rigidbody.AddForce(new Vector3(Random.Range(-1,1),flyingForce, Random.Range(-1,1)));
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("tag_room_boundary"))
            {
                Invoke(nameof(Explode), 0.1f);
            }
        }

        private void Explode()
        {
            particleSystem.Play();
            audioSource.Play();
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
