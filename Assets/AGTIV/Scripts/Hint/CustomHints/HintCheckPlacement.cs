using UnityEngine;

namespace AGTIV.Scripts.Hint.CustomHints
{
    public class HintCheckPlacement : MonoBehaviour, IResetableHint
    {
        [SerializeField] private bool isCheckingLocation = true;
        [SerializeField] private Vector3 desiredLocation;
        [SerializeField] private float distanceThreshold = 0.5f;

        [SerializeField] private bool isCheckingRotation = false;
        [SerializeField] private Vector3 desiredRotation;
        
        [SerializeField] private Hint hint;

        private bool _isHintFinished = false;

        public void OnFinishHint()
        {
            if (!_isHintFinished)
            {
                Invoke(nameof(CheckForFinishHint),1);
            }
        }

        private void CheckForFinishHint()
        {
            if (CheckPositioning())
            {
                _isHintFinished = true;
                hint.OnFinishHint();
            }
        }

        private bool CheckPositioning()
        {
            if (isCheckingLocation)
            {
                var distance = Vector3.Distance(gameObject.transform.position, desiredLocation);
                if (distance > distanceThreshold)
                {
                    return false;
                }
            }

            if (isCheckingRotation)
            {
                if (gameObject.transform.rotation.eulerAngles != desiredRotation)
                {
                    return false;
                }
            }

            return true;
        }

        public void ResetHint()
        {
            isCheckingLocation = true;
            _isHintFinished = false;
        }
    }
}
