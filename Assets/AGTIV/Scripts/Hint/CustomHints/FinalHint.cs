using UnityEngine;
using UnityEngine.Events;

namespace AGTIV.Scripts.Hint.CustomHints
{
    public class FinalHint : MonoBehaviour
    {
        [SerializeField] private float delayToEndHint = 20;

        public UnityEvent endHint;

        private void OnEndHint()
        {
            endHint.Invoke();;
        }

        public void CallEndHint()
        {
            Invoke(nameof(OnEndHint), delayToEndHint);
        }
    }
}
