using UnityEngine;

namespace AGTIV.Scripts.Hint.CustomHints
{
    public class HintFirstPickup : MonoBehaviour, IResetableHint
    {
        public bool isAlreadyPickedUp = false;
        [SerializeField] private Hint hint;

        public void OnFirstPickup()
        {
            if (!isAlreadyPickedUp)
            {
                isAlreadyPickedUp = true;
                hint.OnFinishHint();
            }
        }

        public void ResetHint()
        {
            isAlreadyPickedUp = false;
        }
    }
}
