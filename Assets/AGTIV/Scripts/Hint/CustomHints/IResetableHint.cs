using UnityEngine;

namespace AGTIV.Scripts.Hint.CustomHints
{
    public interface IResetableHint
    {
        public void ResetHint();
    }
}