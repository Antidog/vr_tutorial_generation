using UnityEngine;
using UnityEngine.Events;

namespace AGTIV.Scripts.Hint
{
    public class Hint : MonoBehaviour
    {
        public UnityEvent finishHint;
        public UnityEvent beginHint;

        public void OnBeginHint()
        {
            beginHint.Invoke();
        }

        public void OnFinishHint()
        {
            finishHint.Invoke();
            HintManager.Instance.GoToNextHint();
        }
    }
}
