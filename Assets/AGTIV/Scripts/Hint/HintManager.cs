using System.Collections.Generic;
using System.Linq;
using AGTIV.Scripts.Hint.CustomHints;
using UnityEngine;

namespace AGTIV.Scripts.Hint
{
    public class HintManager : MonoBehaviour
    {
        private static HintManager _instance;
        

        public static HintManager Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<HintManager>();
                return _instance;
            }
        }
    
        public List<Hint> hintsList = new List<Hint>();
        private int _currentHintIndex = -1;
        
        [SerializeField] private AudioSource errorAudioSource;
        [SerializeField] private AudioSource AcceptAudioSource;

        private GameObject _objectToDestroy;

        void Start()
        {
            foreach (var hint in hintsList)
            {
                hint.gameObject.SetActive(false);
            }
            
            GoToNextHint();
        }

        public void GoToNextHint()
        {
            if (_currentHintIndex >=0)
                hintsList[_currentHintIndex].gameObject.SetActive(false);
            
            _currentHintIndex++;
            if(hintsList[_currentHintIndex])
            {
                hintsList[_currentHintIndex].gameObject.SetActive(true);
                hintsList[_currentHintIndex].OnBeginHint();
            }
            
        }

        public void ResetToHint(Hint resetHint)
        {
            foreach (var hint in hintsList)
            {
                if(hint)
                    hint.gameObject.SetActive(false);
            }
            
            _currentHintIndex = hintsList.FindIndex(x => x == resetHint) - 1;
            
            GoToNextHint();
        }

        public void PlayAccept()
        {
            if(AcceptAudioSource){AcceptAudioSource.Play();}
        }
        
        public void PlayError()
        {
            if(errorAudioSource){errorAudioSource.Play();}
        }

        public void DestroyByDelay(GameObject gameObject, float delay = 1.0f)
        {
            _objectToDestroy = gameObject;
            Invoke(nameof(InvokeDestroyByDelay), delay);
        }

        private void InvokeDestroyByDelay()
        {
            Destroy(_objectToDestroy);
        }

        public void CallAllResetables()
        {
            var rObjects = FindObjectsOfType<MonoBehaviour>().OfType<IResetableHint>();
            foreach (var rObject in rObjects)
            {
                rObject.ResetHint();
            }
        }
    }
}
